package com.hooxi.admin.action;

import com.hooxi.admin.biz.v1.action.ActionService;
import com.hooxi.admin.biz.v1.action.model.req.RegActionReq;
import com.hooxi.admin.biz.v1.aws.AwsApplication;
import com.hooxi.admin.constants.code.LangTpCd;
import com.hooxi.admin.db.entity.mission.TbMssnRecomMapping;
import com.hooxi.admin.db.repository.mission.TbMssnRecomMappingRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;


@ExtendWith(SpringExtension.class)
@SpringBootTest
public class ActionServiceTest {

    private final Logger logger = LoggerFactory.getLogger(ActionServiceTest.class);

    @Autowired
    private ActionService actionService;

    @Autowired
    private AwsApplication awsApplication;

    @Autowired
    private TbMssnRecomMappingRepository recomMappingRepository;

    @Test
    @DisplayName("이미지 업로드 테스트")
    void imgUploadTest() {
        //given
        String imgUrl = null;

        //when
        String key = awsApplication.getFilePathToFileUrl(imgUrl);

        //then
        logger.info("key ??? {}", key);
//        assertAll(() -> assert);
    }

    @Test
    @DisplayName("액션 저장 테스트")
    void saveActionTEst() {
        //given
        RegActionReq req = new RegActionReq();
        req.setExp(10);
        req.setLink("test");
        req.setLangTpCd(LangTpCd.KO);
        req.setMssnEtc("방법");
        req.setCateId(586L);
        req.setMssnEffect("효과");
        req.setPoint(100);
        req.setMssnTitle("제목");
        req.setUseYn("N");
        req.setRecomIdList(List.of(8L));
        req.setMssnTitleDtl("소개");

        //when
        actionService.registerAction(req);

        //then


    }

    @Test
    @DisplayName("추천 액션 매핑 테스트")
    void recomMappingTest() {
        //given
        List<Long> actionIdList = new ArrayList<>();
        actionIdList.add(593L);
        actionIdList.add(592L);

        Long recomId = 8L;

        List<Long> mappings = recomMappingRepository.findByRecomId(recomId)
                .stream().map(TbMssnRecomMapping::getMssnId)
                .collect(Collectors.toList());

        //등록하려는 아이디가 db에 있으면 pass
        //등록하려는 아이디가 db에 없으면 insert
        //db 에 저장되있는 아이디가 등록하려는 아이디에 없으면 delete
        //when
        List<Long> insertList = new ArrayList<>();
        List<Long> deleteList = new ArrayList<>();

        for (Long mssnId : actionIdList) {

            if (!mappings.contains(mssnId)) {
                logger.info("######## isContain?? {} insertId : {} ########",  mappings.contains(mssnId), mssnId);
                insertList.add(mssnId);
            }
        }

        for (Long mssnId : mappings) {

            if (!actionIdList.contains(mssnId)) {
                logger.info("########## isContains? {} delete id : {}  ######3 ", actionIdList.contains(mssnId), mssnId);
                deleteList.add(mssnId);
            }
        }

    }

    @Test
    void imgDelete() {
        String imgUrl = "https://hooxi-s3.s3.ap-northeast-2.amazonaws.com/dev/c5106bde-70ea-429f-90f9-b34fbff0a073.jpg";


        awsApplication.deleteByFileUrl(imgUrl);
    }

    @Test
    void dateFormatTest() {
        String endDt = "20220907";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");

        LocalDate localDate = LocalDate.parse(endDt, formatter);
        LocalDateTime localDateTime = localDate.atStartOfDay();

        logger.info("############ localDateTime ?? {} ###########",  localDateTime);
    }

}
