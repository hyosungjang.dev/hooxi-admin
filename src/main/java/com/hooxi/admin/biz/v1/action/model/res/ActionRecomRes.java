package com.hooxi.admin.biz.v1.action.model.res;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ActionRecomRes {
    private Long mssnId;
    private String mssnTitle;
}
