package com.hooxi.admin.biz.v1.user.model;

import com.hooxi.admin.constants.code.MmbrShpStatCd;
import com.hooxi.admin.db.entity.member.TbMmbrshpArticle;
import com.hooxi.admin.db.entity.member.TbMmbrshpSnsLink;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class MmbrshpRes {

    private Long mmbrshpId;

    private String nickname;

    private String afltn;

    private String stateMsg;

    private MmbrShpStatCd mmbrshpStateCd;

    private String mmbrshpStateDesc;

    private String rjctRsn;

    private LocalDateTime frstRgstDttm;

    private LocalDateTime fnlModDttm;

    private LocalDateTime confirmDt;

    private List<TbMmbrshpSnsLink> snsLinks;

    private List<TbMmbrshpArticle> articles;
}
