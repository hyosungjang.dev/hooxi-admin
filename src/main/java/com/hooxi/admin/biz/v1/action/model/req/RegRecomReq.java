package com.hooxi.admin.biz.v1.action.model.req;

import com.hooxi.admin.constants.code.LangTpCd;
import com.hooxi.admin.db.entity.mission.TbMssnRecom;
import com.hooxi.admin.db.entity.mission.TbMssnRecomContents;
import com.hooxi.admin.db.entity.mission.TbMssnRecomMapping;
import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
public class RegRecomReq {
    private String recomTitle;
    private String recomContents;
    private List<Long> actionIdList;
    private String useYn;
    private MultipartFile recomImg;

    public TbMssnRecom toRecomEntity(String imgUrl) {
        return TbMssnRecom.builder()
                .useYn(useYn)
                .regId(3L)
                .regDt(LocalDateTime.now())
                .modId(3L)
                .modDt(LocalDateTime.now())
                .imgUrl(imgUrl)
                .delYn("N")
                .build();
    }

    public TbMssnRecomContents toContentsEntity(Long recomId) {
        return TbMssnRecomContents.builder()
                .recomId(recomId)
                .recomContents(recomContents)
                .recomTitle(recomTitle)
                .langCd(LangTpCd.KO)
                .build();
    }

}
