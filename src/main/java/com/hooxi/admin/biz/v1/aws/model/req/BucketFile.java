package com.hooxi.admin.biz.v1.aws.model.req;

import lombok.Builder;
import lombok.Data;

@Data
public class BucketFile {

    private String fileName;
    private String filePath;
    private String fileUrl;
    private Long fileSize;

    @Builder
    BucketFile(String fileName, String fileUrl, String filePath, Long fileSize) {
        this.fileName = fileName;
        this.filePath = filePath;
        this.fileUrl = fileUrl;
        this.fileSize = fileSize;
    }


}
