package com.hooxi.admin.biz.v1.action.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ActionMissionListReq {

    private int page;
    private int size;

    private String searchSelect;
    private String searchString;

}
