package com.hooxi.admin.biz.v1.action.controller;

import com.hooxi.admin.biz.v1.action.model.ActionMissionListReq;
import com.hooxi.admin.biz.v1.action.model.ActionMissionListRes;
import com.hooxi.admin.biz.v1.action.model.ActionMissionVO;
import com.hooxi.admin.biz.v1.action.service.ActionMissionService;
import com.hooxi.admin.config.type.ListDto;
import com.hooxi.admin.util.Response;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping(path = "/action/mission")
@RequiredArgsConstructor
@CrossOrigin("*")
public class ActionMissionRestController {

    private final Logger logger = LoggerFactory.getLogger(ActionMissionRestController.class);
    private final MessageSourceAccessor messageSourceAccessor;

    @Autowired
    private final ActionMissionService actionMissionService;

    @GetMapping(path = "/list")
    @Operation(
            summary = "미션 리스트 페이지", description = "미션 리스트 페이지",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = ActionMissionListRes.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "2000", description = "서비스 조회에 실패하였습니다"
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<ListDto> getList(
            @Parameter(name = "page", description = "페이지 번호", in = ParameterIn.PATH) @RequestParam(value = "page", required = false, defaultValue = "0") int page,
            @Parameter(name = "size", description = "페이지 사이즈", in = ParameterIn.PATH) @RequestParam(value = "size", required = false, defaultValue = "10") int size
    ) {
        //넘기기
        ActionMissionListReq req = new ActionMissionListReq();
        req.setPage(page);
        req.setSize(size);

        Response res = new Response();

        return actionMissionService.getList(req);
    }

    //상세페이지
    @GetMapping("/view/{id}")
    @Operation(
            summary = "미션 상세페이지", description = "미션 상세페이지",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = ActionMissionVO.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1003", description = "id가 존재하지 않습니다.."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<ActionMissionVO> getOne(
            @Parameter(name = "id", description = "id 값", in = ParameterIn.PATH) @PathVariable(value = "id") Long id
    ) {

        Response res = new Response();

        return actionMissionService.getOne(id);

    }

    //쓰기처리
    @PostMapping("/new")
    @Operation(
            summary = "New 미션 등록", description = "미션 데이터 추가합니다. 등록 시 미션 컨텐츠 필수 같이 등록",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = ActionMissionVO.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1001", description = "continent 코드가 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1101", description = "continent 코드 값이 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1203", description = "Use Type 값 형식이 잘못되었습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1206", description = "Value 형식이 잘못되었습니다."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<Void> saveContent(
            @Parameter(name = "request", description = "Request Dto", in = ParameterIn.PATH) @RequestBody @Valid ActionMissionVO request,
            HttpServletRequest req
    ) {
        Response res = new Response();

        return actionMissionService.saveContent(request, req);
    }

    //수정처리
    @PostMapping("/update/{id}")
    @Operation(
            summary = "기존 미션 수정", description = "미션 데이터 수정합니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = ActionMissionVO.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1003", description = "id가 존재하지 않습니다.."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1001", description = " 코드가 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1101", description = " 코드 값이 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1203", description = " 값 형식이 잘못되었습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1206", description = "Value 형식이 잘못되었습니다."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<Void> updateContent(
            @Parameter(name = "id", description = "id 값", in = ParameterIn.PATH) @PathVariable(value = "id") Long id,
            @Parameter(name = "request", description = "Request Dto", in = ParameterIn.PATH) @RequestBody @Valid ActionMissionVO request,
            HttpServletRequest req
    ) {
        Response res = new Response();

        return actionMissionService.updateContent(id, request, req);
    }

    //삭제처리
    @PostMapping("/delete/{id}")
    @Operation(
            summary = "기존 미션 삭제", description = "미션 데이터 삭제합니다. 삭제는 매우 유의하시기 바랍니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = ActionMissionVO.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1003", description = "id가 존재하지 않습니다.."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<Void> delete(
            @Parameter(name = "id", description = "id 값", in = ParameterIn.PATH) @PathVariable(value = "id") Long id,
            HttpServletRequest req
    ) {
        Response res = new Response();

        return actionMissionService.deleteContent(id, req);
    }
}
