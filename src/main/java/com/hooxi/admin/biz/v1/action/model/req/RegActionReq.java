package com.hooxi.admin.biz.v1.action.model.req;

import com.hooxi.admin.constants.code.LangTpCd;
import com.hooxi.admin.constants.code.TpcTpCd;
import com.hooxi.admin.db.entity.mission.TbMssn;
import com.hooxi.admin.db.entity.mission.TbMssnContents;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.Multipart;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
public class RegActionReq {
    private Long cateId;
    private List<Long> recomIdList;
    private String mssnTitle;
    private String mssnEffect;
    private String mssnEtc;
    private String mssnTitleDtl;
    private int point;
    private int exp;
    @Value("KO")
    private LangTpCd langTpCd;
    private String link;
    private String useYn;

    private MultipartFile actImg;

    public TbMssn toMssnEntity(TpcTpCd tpcTpCd, String imgUrl) {
        // todo 임시 regId, modId 추후 변경
        return TbMssn.builder()
                .cateId(cateId)
                .linkUrl(link)
                .point(point)
                .exp(exp)
                .useYn(useYn)
                .tpcTpCd(tpcTpCd)
                .regId(3L)
                .regDt(LocalDateTime.now())
                .modId(3L)
                .modDt(LocalDateTime.now())
                .imgUrl(imgUrl)
                .build();
    }

    public TbMssnContents toContentsEntity(Long mssnId) {
        return TbMssnContents.builder()
                .mssnId(mssnId)
                .langCd(langTpCd)
                .mssnEffect(mssnEffect)
                .mssnEtc(mssnEtc)
                .mssnTitle(mssnTitle)
                .mssnTitleDtl(mssnTitleDtl)
                .build();
    }

}
