package com.hooxi.admin.biz.v1.aws.model.res;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class FileResponse {

    private Long fileSeq;
    private String fileName;
    private String fileUrl;

}
