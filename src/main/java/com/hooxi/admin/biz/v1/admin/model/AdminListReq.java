package com.hooxi.admin.biz.v1.admin.model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AdminListReq {


    private int page;
    private int size;

    private String searchName;
    private String searchNameVal;

    private String startDt;
    private String endDt;


}
