package com.hooxi.admin.biz.v1.action.model.req;

import com.hooxi.admin.constants.code.ActionPlaceTpCd;
import com.hooxi.admin.constants.code.ActionPurposeTpCd;
import com.hooxi.admin.constants.code.LangTpCd;
import com.hooxi.admin.db.entity.mission.TbMssnCateContents;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.multipart.MultipartFile;

@Getter
@Setter
public class ModCateReq {
    @ApiModelProperty(value = "카테고리 id")
    private Long cateId;
    @ApiModelProperty(value = "언어 코드")
    @Value("KO")
    private LangTpCd langTpCd;
    @ApiModelProperty(value = "카테고리 제목")
    private String cateTitle;
    @ApiModelProperty(value = "소개")
    private String cateContents;
    @ApiModelProperty(value = "장소 코드")
    private ActionPlaceTpCd placeCd;
    @ApiModelProperty(value = "테마 코드")
    private ActionPurposeTpCd prpsCd;

    private MultipartFile cateImg;

    private String useYn;

    public TbMssnCateContents toContentsEntity() {
        return TbMssnCateContents.builder()
                .cateId(cateId)
                .langCd(langTpCd)
                .cateTitle(cateTitle)
                .cateContents(cateContents)
                .build();
    }
}
