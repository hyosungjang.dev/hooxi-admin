package com.hooxi.admin.biz.v1.action.model.req;

import com.hooxi.admin.constants.code.LangTpCd;
import com.hooxi.admin.db.entity.mission.TbMssnContents;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Getter
@Setter
public class ModActionReq {
    private Long mssnId;
    private Long cateId;
    private String mssnTitle;
    private String mssnEtc;
    private String mssnTitleDtl;
    private String mssnEffect;
    private String useYn;
    @Value("KO")
    private LangTpCd langTpCd;
    private Integer point;
    private Integer exp;
    private String link;

    private MultipartFile actImg;

    private List<Long> recomIdList;

    public TbMssnContents toContentsEntify() {
        return TbMssnContents.builder()
                .langCd(LangTpCd.KO)
                .mssnTitleDtl(mssnTitleDtl)
                .mssnTitle(mssnTitle)
                .mssnEtc(mssnEtc)
                .mssnEffect(mssnEffect)
                .mssnId(mssnId)
                .build();
    }
}
