package com.hooxi.admin.biz.v1.action.model.res;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class RecommendListRes {
    @ApiModelProperty(value = "추천 id")
    private Long recomId;
    @ApiModelProperty(value = "추천 타이틀")
    private String recomTitle;
    @ApiModelProperty(value = "추천 컨텐츠")
    private String recomContents;
    @ApiModelProperty(value = "이미지 url")
    private String imgUrl;
    @ApiModelProperty(value = "등록일")
    private LocalDateTime regDt;
    @ApiModelProperty(value = "수정일")
    private LocalDateTime modDt;
    @ApiModelProperty(value = "등록자 회원 id")
    private Long regId;
    @ApiModelProperty(value = "등록자")
    private String nickname;
    @ApiModelProperty(value = "액션 수")
    private Long actionCnt;
    @ApiModelProperty(value = "총 포인트")
    private Integer totalPoint;
}
