package com.hooxi.admin.biz.v1.reply.model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ReplyRes {

    private Long replyId;

    private Long mmbrId;

    private String replyCtnt;

    private String useYn;

    private LocalDateTime regDt;

    private String profileImg;

    private String nickname;

    private String lvlCd;

    private Long likeCnt;

    private Long replyCnt;
}
