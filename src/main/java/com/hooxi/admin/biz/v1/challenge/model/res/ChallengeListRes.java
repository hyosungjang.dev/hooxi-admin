package com.hooxi.admin.biz.v1.challenge.model.res;

import com.hooxi.admin.constants.code.ActionPlaceTpCd;
import com.hooxi.admin.constants.code.ActionPurposeTpCd;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class ChallengeListRes {
    private Long mssnId;
    private Long cateId;
    private Long companyId;
    private Long challengeId;
    private String mssnTitle;
    private ActionPlaceTpCd placeCd;
    private ActionPurposeTpCd prpsCd;
    private String placeCdDesc;
    private String prpsCdDesc;
    private String imgUrl;
    private String companyName;
    private LocalDateTime startDt;
    private LocalDateTime endDt;
    private LocalDateTime regDt;
    private LocalDateTime modDt;
    private Long regId;
    private String nickname;
    private String useYn;

}
