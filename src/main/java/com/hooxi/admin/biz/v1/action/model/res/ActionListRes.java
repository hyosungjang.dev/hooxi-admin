package com.hooxi.admin.biz.v1.action.model.res;

import com.hooxi.admin.constants.code.ActionPlaceTpCd;
import com.hooxi.admin.constants.code.ActionPurposeTpCd;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
public class ActionListRes {
    @ApiModelProperty(value = "미션 id")
    private Long mssnId;
    @ApiModelProperty(value = "카테고리 id")
    private Long cateId;
    @ApiModelProperty(value = "미션 타이틀")
    private String mssnTitle;
    @ApiModelProperty(value = "카테고리 타이틀")
    private String cateTitle;
    @ApiModelProperty(value = "부문 코드")
    private ActionPurposeTpCd prpsCd;
    @ApiModelProperty(value = "장소 코드")
    private ActionPlaceTpCd placeCd;
    @ApiModelProperty(value = "등록일")
    private LocalDateTime regDt;
    @ApiModelProperty(value = "수정일")
    private LocalDateTime modDt;
    @ApiModelProperty(value = "포인트")
    private Integer point;
    @ApiModelProperty(value = "경험치")
    private Integer exp;
    @ApiModelProperty(value = "이미지")
    private String imgUrl;
    @ApiModelProperty(value = "부문")
    private String prpsCdDesc;
    @ApiModelProperty(value = "장소")
    private String placeCdDesc;
    @ApiModelProperty(value = "등록자")
    private String nickname;
    @ApiModelProperty(value = "등록자 회원 id")
    private Long regId;
    @ApiModelProperty(value = "사용 여부")
    private String useYn;

    @ApiModelProperty(value = "추천 액션 추가 여부", notes = "파라미터에 recomId 보내면 해당 추천에 액션이 추가되었는지 판단")
    private String recomAddYn;

}
