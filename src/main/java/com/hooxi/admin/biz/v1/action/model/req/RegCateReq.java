package com.hooxi.admin.biz.v1.action.model.req;

import com.hooxi.admin.constants.code.ActionPlaceTpCd;
import com.hooxi.admin.constants.code.ActionPurposeTpCd;
import com.hooxi.admin.constants.code.LangTpCd;
import com.hooxi.admin.constants.code.TpcTpCd;
import com.hooxi.admin.db.entity.mission.TbMssnCate;
import com.hooxi.admin.db.entity.mission.TbMssnCateContents;
import com.hooxi.admin.db.entity.mission.TbMssnCateMeta;
import com.hooxi.admin.db.entity.mission.TbMssnCatePlace;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDateTime;

@Getter
@Setter
public class RegCateReq {
    @ApiModelProperty(value = "카테고리 제목", required = true)
    private String cateTitle;
    @ApiModelProperty(value = "카테고리 소개")
    private String cateContents;
    @ApiModelProperty(value = "장소 코드", required = true)
    private ActionPlaceTpCd placeCd;
    @ApiModelProperty(value = "테마 코드", required = true)
    private ActionPurposeTpCd prpsCd;
    @ApiModelProperty(value = "언어 코드")
    @Value("KO")
    private LangTpCd langTpCd;

    @ApiModelProperty(value = "이미지")
    private MultipartFile cateImg;

    @ApiModelProperty(value = "사용여부")
    private String useYn;

    public TbMssnCate toCateEntity(TpcTpCd tpcTpCd, String imgUrl) {
        return TbMssnCate.builder()
                .imgUrl(imgUrl)
                .useYn(useYn)
                .tpcTpCd(tpcTpCd)
                .regId(1L)
                .regDt(LocalDateTime.now())
                .modId(1L)
                .modDt(LocalDateTime.now())
                .build();
    }

    public TbMssnCateContents toCateContentsEntity(Long cateId) {
        return TbMssnCateContents.builder()
                .cateContents(cateContents)
                .cateId(cateId)
                .cateTitle(cateTitle)
                .langCd(langTpCd)
                .build();
    }

    public TbMssnCatePlace toCatePlaceEntity(Long cateId) {
        return TbMssnCatePlace.builder()
                .cateId(cateId)
                .placeCd(placeCd)
                .build();
    }

    public TbMssnCateMeta toCateMetaEntity(Long cateId) {
        return TbMssnCateMeta.builder()
                .cateId(cateId)
                .prpsCd(prpsCd)
                .build();
    }
}
