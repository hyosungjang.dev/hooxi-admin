package com.hooxi.admin.biz.v1.aws;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.HttpMethod;
import com.amazonaws.SdkClientException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.Headers;
import com.amazonaws.services.s3.model.*;
import com.hooxi.admin.biz.v1.aws.model.req.BucketFile;
import com.hooxi.admin.biz.v1.aws.model.res.BucketFileResponse;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.*;

@Service
@RequiredArgsConstructor
public class AwsApplication {
    private Logger logger = LoggerFactory.getLogger(AwsApplication.class);
    @Value("${cloud.aws.credentials.bucket}")
    private String AWS_S3_BUCKET;
    @Value("${upload.aws-s3.path}")
    private String AWS_UPLOAD_URL;
    @Value("${upload.aws-s3.dir}")
    private String AWS_UPLOAD_DIR;
    @Value("${upload.aws-s3.pre-sign-expire}")
    private Long AWS_UPLOAD_URL_EXPIRE_TIME;

    private final AmazonS3 amazonS3;

    /**
     * make file path: file path is an object key of s3 bucket
     *  - object key ->  delete a file
     * @param filePath
     * @return
     */
    public String getFileUrl(String filePath) {
        return AWS_UPLOAD_URL + filePath;
    }

    public String getFilePathToFileUrl(String fileUrl) {
        return fileUrl.replace(AWS_UPLOAD_URL,"");
    }

    private String createFileName(String fileName) {
        return AWS_UPLOAD_DIR + UUID.randomUUID().toString().concat(getFileExtension(fileName));
    }

    /**
     * image upload
     * @param multipartFile
     * @return
     */
    public List<BucketFile> uploadImage(List<MultipartFile> multipartFile) {
        List<BucketFile> fileList = new ArrayList<>();
        multipartFile.forEach(file -> {
            BucketFile bucketFile = this.s3UploadImage(file);
            fileList.add(bucketFile);
        });
        return fileList;
    }

    private String getFileExtension(String fileName) {
        try {
            return fileName.substring(fileName.lastIndexOf("."));
        } catch (StringIndexOutOfBoundsException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "잘못된 형식의 파일(" + fileName + ") 입니다.");
        }
    }

    public void deleteByFileUrl(String fileUrl) {
        if (fileUrl != null) {
            String key = this.getFilePathToFileUrl(fileUrl);
            this.deleteByFilePath(key);
        }
    }


    /**
     * file remove
     * @param filePath
     */
    @Transactional
    public void deleteByFilePath(String filePath) {
        try {
            amazonS3.deleteObject(new DeleteObjectRequest(AWS_S3_BUCKET, filePath));
        } catch (AmazonServiceException e) {
            logger.error(e.getMessage());
            throw new AmazonS3Exception(e.getMessage());
        }
    }




    // 만료시간 체크
    private Date getExpiration() {
        // Set the pre-signed URL to expire after milliseconds
        Date expiration = new Date();
        long expTimeMillis = expiration.getTime();
        expTimeMillis += AWS_UPLOAD_URL_EXPIRE_TIME;
        expiration.setTime(expTimeMillis);
        return expiration;
    }

    /**
     * 단일 이미지 업로드
     * @param file
     * @return
     */
    public BucketFile uploadOneImage(MultipartFile file) {
        BucketFile bucketFile = this.s3UploadImage(file);
        return bucketFile;
    }

    /**
     * 이미지 업로드 후 db 저장 객체 만들기
     * @param file
     * @return
     */
    private BucketFile s3UploadImage(MultipartFile file) {
        String filePath = createFileName(file.getOriginalFilename());
        String fileUrl = this.getFileUrl(filePath);
        ObjectMetadata objectMetadata = new ObjectMetadata();
        objectMetadata.setContentLength(file.getSize());
        objectMetadata.setContentType(file.getContentType());
        try(InputStream inputStream = file.getInputStream()) {
            amazonS3.putObject(new PutObjectRequest(AWS_S3_BUCKET, filePath, inputStream, objectMetadata)
                    .withCannedAcl(CannedAccessControlList.PublicRead));
        } catch(IOException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "이미지 업로드에 실패했습니다.");
        }
        return BucketFile.builder()
                .fileName(file.getOriginalFilename())
                .filePath(filePath)
                .fileUrl(fileUrl)
                .fileSize(file.getSize())
                .build();
    }

    public List<BucketFileResponse> makePreSignUrl(List<String> fileNames) {
        try {
            List<BucketFileResponse> result = new LinkedList<>();

            for (String file : fileNames) {
                String ojbKey = this.createFileName(file);
                GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(AWS_S3_BUCKET, ojbKey)
                        .withMethod(HttpMethod.PUT)
                        .withExpiration( this.getExpiration() );

                // 업로드한 파일 접근 - 정책
                generatePresignedUrlRequest.addRequestParameter(Headers.S3_CANNED_ACL, CannedAccessControlList.PublicRead.toString());
                URL url = amazonS3.generatePresignedUrl(generatePresignedUrlRequest);
                // front에 전달할 pre sign url 목록
                result.add(
                        BucketFileResponse.builder()
                                .fileName(file)
                                .preSignUrl(url.toExternalForm())
                                .filePath(ojbKey)
                                .build()
                );
            }
            return result;
        } catch(AmazonServiceException e) {
            // The call was transmitted successfully, but Amazon S3 couldn't process
            // it, so it returned an error response.
            e.printStackTrace();
        } catch(SdkClientException e) {
            // Amazon S3 couldn't be contacted for a response, or the client
            // couldn't parse the response from Amazon S3.
            e.printStackTrace();
        }

        return null;
    }
}
