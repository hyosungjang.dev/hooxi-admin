package com.hooxi.admin.biz.v1.challenge;

import com.hooxi.admin.biz.v1.challenge.model.req.RegChallengeReq;
import com.hooxi.admin.biz.v1.challenge.model.res.ChallengeDtlRes;
import com.hooxi.admin.biz.v1.challenge.model.res.ChallengeListRes;
import com.hooxi.admin.biz.v1.challenge.model.res.RecomChallengeListRes;
import com.hooxi.admin.constants.code.LangTpCd;
import com.hooxi.admin.util.Response;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/challenge")
@CrossOrigin("*")
@RequiredArgsConstructor
@Api(tags = {"챌린지"})
public class ChallengeController {
    private final ChallengeService challengeService;

    @GetMapping(path = "/list")
    @Operation(
            summary = "챌린지 리스트 조회"
    )
    public Response<PageImpl<ChallengeListRes>> getChallengeList(
            Pageable pageable,
            @RequestParam(value = "langTpCd", defaultValue = "KO") LangTpCd langTpCd
    ) {
        return challengeService.getChallengeList(pageable, langTpCd);
    }

    @GetMapping(path = "/recom/list")
    @Operation(summary = "추천 챌린지 조회")
    public Response<PageImpl<RecomChallengeListRes>> getRecomChallengeList(
            Pageable pageable,
            @RequestParam(value = "langTpCd", defaultValue = "KO") LangTpCd langTpCd
    ) {
        return challengeService.getRecomList(pageable, langTpCd);
    }

    @PostMapping(path = "/add", consumes = {"multipart/form-data"})
    @Operation(
            summary = "챌린지 등록",
            responses = {
                    @ApiResponse(responseCode = "0000", description = "성공"),
                    @ApiResponse(responseCode = "A105", description = "카테고리 정보 없음"),
                    @ApiResponse(responseCode = "C001", description = "언어 코드 없음"),
            },
            description = "startDt, endDt 는 yyyyMMdd 형식으로"
    )
    public Response<Void> registerChallenge(@ModelAttribute RegChallengeReq req) {
        return challengeService.registerChallenge(req);
    }

    @GetMapping("/{mssnId}")
    @Operation(
            summary = "챌린지 상세",
            responses = {
                    @ApiResponse(responseCode = "0000", description = "성공"),
                    @ApiResponse(responseCode = "A101", description = "챌린지 정보 없음"),
            }
    )
    public Response<ChallengeDtlRes> getChallengeDtl(@PathVariable(value = "mssnId") Long mssnId) {
        return challengeService.getChallengeDtl(mssnId);
    }
}
