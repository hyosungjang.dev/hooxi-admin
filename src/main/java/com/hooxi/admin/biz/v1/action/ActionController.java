package com.hooxi.admin.biz.v1.action;

import com.hooxi.admin.biz.v1.action.model.req.*;
import com.hooxi.admin.biz.v1.action.model.res.*;
import com.hooxi.admin.constants.code.ActionPlaceTpCd;
import com.hooxi.admin.constants.code.ActionPurposeTpCd;
import com.hooxi.admin.util.Response;
import com.hooxi.admin.constants.code.LangTpCd;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


@RestController
@RequestMapping(path = "/action")
@CrossOrigin("*")
@RequiredArgsConstructor
@Api(tags = {"액션"})
public class ActionController {
    private final Logger log = LoggerFactory.getLogger(ActionController.class);
    private final ActionService actionService;


    @GetMapping(path = "/list")
    @Operation(summary = "액션 리스트 조회")
    public Response<PageImpl<ActionListRes>> getActionList(
            ActionPlaceTpCd placeCd,
            ActionPurposeTpCd prpsCd,
            @RequestParam(value = "langTpCd", defaultValue = "KO") LangTpCd langTpCd,
            Pageable pageable,
            Long recomId
    ) {
        return actionService.getActionList(placeCd, prpsCd, langTpCd, pageable);
    }

    @GetMapping(path ="/cate/list")
    @Operation(summary = "카테고리 리스트 조회")
    public Response<PageImpl<CategoryListRes>> getCategoryList(
            ActionPlaceTpCd placeCd,
            ActionPurposeTpCd prpsCd,
            @RequestParam(value = "langTpCd", defaultValue = "KO") LangTpCd langTpCd,
            Pageable pageable
    ) {
        return actionService.getCategoryList(placeCd, prpsCd, langTpCd, pageable);
    }

    @GetMapping(path = "/recom/list")
    @Operation(summary = "추천 리스트 조회")
    public Response<PageImpl<RecommendListRes>> getRecommendList(
            @RequestParam(value = "langTpCd", defaultValue = "KO") LangTpCd langTpCd,
            Pageable pageable
    ) {
        return actionService.getRecommendList(langTpCd, pageable);
    }

    @GetMapping(path = "/cate/{cateId}")
    @Operation(
            summary = "카테고리 상세",
            responses = {
                    @ApiResponse(responseCode = "0000", description = "성공"),
                    @ApiResponse(responseCode = "A105", description = "카테고리 정보 없음")
            }
    )
    public Response<CategoryDtlRes> getCategoryDtl(
            @PathVariable(value = "cateId") Long cateId,
            @RequestParam(value = "langTpCd", defaultValue = "KO") LangTpCd langTpCd
    ) {
        return actionService.getCategoryDtl(cateId, langTpCd);
    }

    @PostMapping(path = "/cate/add", consumes = {"multipart/form-data"})
    @Operation(
            summary = "카테고리 등록",
            responses = {
                    @ApiResponse(responseCode = "0000", description = "성공"),
            }
    )
    public Response<Void> registerCategory(@ModelAttribute RegCateReq req){
        return actionService.registerCate(req);
    }


    @PostMapping(path = "/cate/mod", consumes = {"multipart/form-data"})
    @Operation(
            summary = "카테고리 수정",
            responses = {
                    @ApiResponse(responseCode = "0000", description = "성공"),
                    @ApiResponse(responseCode = "A105", description = "카테고리 정보 없음")
            }
    )
    public Response<Void> modifyCategory(
            @ModelAttribute ModCateReq req
    ) {
        return actionService.modifyCate(req);
    }

    @PostMapping(path = "/add", consumes = {"multipart/form-data"})
    @Operation(summary = "액션 등록")
    public Response<Void> registerAction(@ModelAttribute RegActionReq req) {
        return actionService.registerAction(req);
    }

    @GetMapping(path = "/{mssnId}")
    @Operation(
            summary = "액션 상세",
            responses = {
            @ApiResponse(responseCode = "0000", description = "성공"),
            @ApiResponse(responseCode = "A101", description = "행동 정보 없음")
    })
    public Response<ActionDtlRes> getActionDtl(
            @PathVariable(value = "mssnId") Long mssnId,
            @RequestParam(value = "langTpCd", defaultValue = "KO") LangTpCd langCd
    ) {
        return actionService.getActionDtl(mssnId, langCd);
    }

    @PostMapping(path = "/mod", consumes = {"multipart/form-data"})
    @Operation(summary = "액션 수정",
            responses = {
                    @ApiResponse(responseCode = "0000", description = "성공"),
                    @ApiResponse(responseCode = "A101", description = "액션 정보 없음"),
                    @ApiResponse(responseCode = "A105", description = "카테고리 정보 없음"),
            })
    public Response<Void> modifyAction(
            @ModelAttribute ModActionReq req) {
        return actionService.modifyAction(req);
    }

    @GetMapping(path = "/recom/{recomId}")
    @Operation(summary = "추천 상세",
            responses = {
                    @ApiResponse(responseCode = "0000", description = "성공"),
                    @ApiResponse(responseCode = "A106", description = "추천 정보 없음")
            })
    public Response<RecomDtlRes> getRecomDtl(
            @PathVariable(value = "recomId") Long recomId,
            @RequestParam(value = "langTpCd", defaultValue = "KO") LangTpCd langTpCd
    ) {
        return actionService.getRecomDtl(recomId, langTpCd);
    }

    @PostMapping(path = "/recom/add", consumes = {"multipart/form-data"})
    @Operation(summary = "추천 등록")
    public Response<Void> registerRecom(
            @ModelAttribute RegRecomReq req
    ) {
        return actionService.registerRecom(req);
    }

    @PostMapping(path = "/recom/mod", consumes = {"multipart/form-data"})
    @Operation(summary = "추천 수정")
    public Response<Void> modifyRecom(@ModelAttribute ModRecomReq req) {
        return actionService.modifyRecom(req);
    }
}
