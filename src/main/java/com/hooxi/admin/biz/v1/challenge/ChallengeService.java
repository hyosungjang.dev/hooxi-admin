package com.hooxi.admin.biz.v1.challenge;

import com.hooxi.admin.biz.v1.aws.AwsApplication;
import com.hooxi.admin.biz.v1.aws.model.req.BucketFile;
import com.hooxi.admin.biz.v1.challenge.model.req.RegChallengeReq;
import com.hooxi.admin.biz.v1.challenge.model.res.ChallengeDtlRes;
import com.hooxi.admin.biz.v1.challenge.model.res.ChallengeListRes;
import com.hooxi.admin.biz.v1.challenge.model.res.RecomChallengeListRes;
import com.hooxi.admin.constants.code.LangTpCd;
import com.hooxi.admin.db.entity.mission.TbMssn;
import com.hooxi.admin.db.entity.mission.TbMssnChallenge;
import com.hooxi.admin.db.repository.mission.*;
import com.hooxi.admin.exception.ErrorCode;
import com.hooxi.admin.exception.common.CommonCodeException;
import com.hooxi.admin.util.Response;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ChallengeService {
    private final Logger log = LoggerFactory.getLogger(ChallengeService.class);

    //mssn
    private final TbMssnRepository mssnRepository;
    private final TbMssnContentsRepository mssnContentsRepository;

    //cate
    private final TbMssnCateRepository cateRepository;
    private final TbMssnCateContentsRepository cateContentsRepository;
    private final TbMssnCateMetaRepository cateMetaRepository;
    private final TbMssnCatePlaceRepository catePlaceRepository;

    //aws
    private final AwsApplication awsApplication;

    private final TbMssnChallengeRepository challengeRepository;

    /**
     * @apiNote 챌린지 리스트 조회
     * @param pageable
     * @param langTpCd
     * @return
     */
    public Response<PageImpl<ChallengeListRes>> getChallengeList(Pageable pageable, LangTpCd langTpCd) {
        PageImpl<ChallengeListRes> res = mssnRepository.findChallengeList(pageable, langTpCd);

        res.getContent().forEach(item -> {
            item.setPlaceCdDesc(item.getPlaceCd().getDesc());
            item.setPrpsCdDesc(item.getPrpsCd().getDesc());
        });

        return new Response<>(res);
    }

    /**
     * @apiNote 추천 챌린지 리스트 조회
     * @param pageable
     * @param langTpCd
     * @return
     */
    public Response<PageImpl<RecomChallengeListRes>> getRecomList(Pageable pageable, LangTpCd langTpCd) {
        return new Response<>(cateRepository.findChallengeCateList(pageable, langTpCd));
    }

    public Response<Void> registerChallenge(RegChallengeReq req) {
        if (!cateRepository.existsByCateId(req.getCateId())) throw new CommonCodeException(ErrorCode.NOT_FOUND_CATE);
        if (req.getLangTpCd() == null) throw new CommonCodeException(ErrorCode.INVALID_LANG_TP_CD);

        BucketFile file = null;
        if (req.getImgUrl() != null && !req.getImgUrl().isEmpty()) {
            file =  awsApplication.uploadOneImage(req.getImgUrl());
        }

        TbMssn mssn = mssnRepository.save(req.toMssnEntity(file != null ? file.getFileUrl() : null));

        mssnContentsRepository.save(req.toMssnContentsEntity(mssn.getMssnId()));

        challengeRepository.save(req.toChallengeEntity(mssn.getMssnId()));
        return new Response<>();
    }

    public Response<ChallengeDtlRes> getChallengeDtl(Long mssnId) {
        ChallengeDtlRes  res = mssnRepository.findChallengeDtlByMssnId(mssnId);
        if (res == null) throw new CommonCodeException(ErrorCode.NOT_FOUND_ACTION);
        res.setPlaceCdDesc(res.getPlaceCd().getDesc());
        res.setPrpsCdDesc(res.getPrpsCd().getDesc());
        return new Response<>(res);
    }


}
