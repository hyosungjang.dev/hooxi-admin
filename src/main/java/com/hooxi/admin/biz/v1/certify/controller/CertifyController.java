package com.hooxi.admin.biz.v1.certify.controller;

import com.hooxi.admin.biz.v1.certify.model.CertifyListRes;
import com.hooxi.admin.biz.v1.certify.service.CertifyService;
import com.hooxi.admin.biz.v1.story.model.StoryRes;
import com.hooxi.admin.constants.code.FeedTpCd;
import com.hooxi.admin.constants.code.StoryStatCd;
import com.hooxi.admin.constants.code.StoryTpCd;
import com.hooxi.admin.util.Response;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@RestController
@RequestMapping(path = "/certify")
@CrossOrigin("*")
@RequiredArgsConstructor
@Api(tags = {"인증"})
public class CertifyController {
    private final Logger logger = LoggerFactory.getLogger(CertifyController.class);
    private final CertifyService certifyService;

    @GetMapping(path = "/list")
    public Response<PageImpl<CertifyListRes>> getCertifyList(Pageable pageable, StoryTpCd storyTpCd, StoryStatCd storyStatCd, FeedTpCd feedTpCd) {
        return certifyService.getCertifyList(pageable, storyTpCd, storyStatCd, feedTpCd);
    }

    @GetMapping(path = "/count")
    @Operation(summary = "상태별 카운트")
    public Response<HashMap<String, Object>> getCount(
            @RequestParam(value = "type") StoryTpCd type,
            @RequestParam(value = "feed") FeedTpCd feed) {
        return certifyService.getCount(type, feed);
    }

    @GetMapping(path = "/{storyId}")
    @Operation(summary = "인증 상세")
    public Response<StoryRes> getCertify(@PathVariable Long storyId) {
        return certifyService.getCertify(storyId);
    }
}
