package com.hooxi.admin.biz.v1.story.model;


import com.hooxi.admin.biz.v1.reply.model.ReplyRes;
import com.hooxi.admin.constants.code.FeedTpCd;
import com.hooxi.admin.constants.code.StoryStatCd;
import com.hooxi.admin.constants.code.StoryTpCd;
import com.hooxi.admin.db.entity.story.TbStoryFile;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class StoryReq {
    private Long storyId;

    private String storyCntn;

    private StoryStatCd storyStatCd;
}
