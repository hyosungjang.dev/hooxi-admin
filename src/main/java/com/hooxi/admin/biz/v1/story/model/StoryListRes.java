package com.hooxi.admin.biz.v1.story.model;


import com.hooxi.admin.constants.code.FeedTpCd;
import com.hooxi.admin.constants.code.GradTpCd;
import com.hooxi.admin.constants.code.StoryStatCd;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class StoryListRes {

    private Long storyId;

    private Long mmbrId;

    private StoryStatCd storyStatCd;

    private String storyStatCdDesc;

    private FeedTpCd feedTpCd;

    private String feedTpCdDesc;

    private String mainImgUrl;

    private String storyCntn;

    private Long viewCnt;

    private Long shareCnt;

    private Long replyCnt;

    private Long likeCnt;

    private String nickname;

    private String profileImg;

    private GradTpCd gradCd;

    private String gradCdDesc;

    private LocalDateTime regDt;

    private LocalDateTime modDt;
}
