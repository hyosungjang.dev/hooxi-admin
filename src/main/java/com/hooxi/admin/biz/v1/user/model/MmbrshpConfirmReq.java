package com.hooxi.admin.biz.v1.user.model;


import com.hooxi.admin.constants.code.GradTpCd;
import com.hooxi.admin.constants.code.MmbrShpStatCd;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MmbrshpConfirmReq {
    private Long mmbrshpId;
    private MmbrShpStatCd mmbrshpStateCd;
    private GradTpCd gradCd;
    private LocalDateTime confirmDt;
}
