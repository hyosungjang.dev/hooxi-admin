package com.hooxi.admin.biz.v1.user.controller;

import com.hooxi.admin.biz.v1.user.model.UserListReq;
import com.hooxi.admin.biz.v1.user.model.UserListRes;
import com.hooxi.admin.biz.v1.user.model.MmbrshpRes;
import com.hooxi.admin.biz.v1.user.model.UserProfileRes;
import com.hooxi.admin.biz.v1.user.service.UserService;
import com.hooxi.admin.util.Response;
import com.hooxi.admin.constants.code.MmbrShpStatCd;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@RestController
@RequestMapping(path = "/user")
@RequiredArgsConstructor
@CrossOrigin("*")
@Api(tags = {"유저"})
public class UserController {

    private final Logger logger = LoggerFactory.getLogger(UserController.class);
    private final MessageSourceAccessor messageSourceAccessor;

    @Autowired
    private final UserService userService;

    @GetMapping(path = "/list")
    @Operation(summary = "회원 리스트 조회")
    public Response<PageImpl<UserListRes>> getList(
            Pageable pageable,
            @RequestParam(value = "type", required = false) MmbrShpStatCd type
    ) {
        UserListReq req = new UserListReq();
        req.setType(type);

        return userService.getList(pageable, req);
    }

    @GetMapping(path = "/count")
    @Operation(summary = "상태별 회원 카운트")
    public Response<HashMap<String, Object>> getCount() {
        return userService.getCount();
    }

    @GetMapping(path = "/profile/{mmbrId}")
    @Operation(summary = "프로필 조회")
    public Response<UserProfileRes> getProfile(@PathVariable Long mmbrId) {
        return userService.getProfile(mmbrId);
    }

    @GetMapping(path = "/mmbrshp/{mmbrshpId}")
    @Operation(summary = "멤버십 조회")
    public Response<MmbrshpRes> getMmbrshp(@PathVariable Long mmbrshpId) {
        return userService.getMmbrshp(mmbrshpId);
    }

    @PostMapping(path = "/mmbrshp/confirm")
    @Operation(summary = "멤버십 승인")
    public Response<Void> confirmMmbrshp(@RequestBody Long mmbrshpId) {
        return userService.confirmMmbrshp(mmbrshpId);
    }
}
