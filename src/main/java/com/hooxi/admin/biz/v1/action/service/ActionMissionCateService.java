package com.hooxi.admin.biz.v1.action.service;

import com.google.gson.Gson;
import com.hooxi.admin.biz.v1.action.model.*;
import com.hooxi.admin.config.type.ListDto;
import com.hooxi.admin.config.type.ResultCodeConst;
import com.hooxi.admin.util.Paging;
import com.hooxi.admin.util.Response;
import com.hooxi.admin.db.entity.code.TbCmnCode;
import com.hooxi.admin.db.entity.company.TbCompany;
import com.hooxi.admin.db.entity.mission.TbMssnCate;
import com.hooxi.admin.db.repository.code.TbCmnCodeRepository;
import com.hooxi.admin.db.repository.company.TbCompanyRepository;
import com.hooxi.admin.db.repository.mission.TbMssnCateRepository;
import com.querydsl.core.QueryResults;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class ActionMissionCateService {

    private final Logger logger = LoggerFactory.getLogger(ActionMissionCateService.class);
    private final ModelMapper modelMapper;
    private final HttpServletRequest httpServletRequest;
    private final MessageSourceAccessor messageSourceAccessor;

    private final TbMssnCateRepository tbMssnCateRepository;
    private final TbCompanyRepository tbCompanyRepository;
    private final TbCmnCodeRepository tbCmnCodeRepository;

    public Response<ListDto> getList(ActionMissionCateListReq dto) {

        Response<ListDto> res = new Response<>();

        try{
            //리스트 내용 가져오기
            List<ActionMissionCateListRes> resList =
                    tbMssnCateRepository.selectListTbMssnCate(dto).stream()
                            .map(e -> modelMapper.map(e, ActionMissionCateListRes.class))
                            .peek(f -> {
                                if(f.getTpcTpCd() != null) {
                                    //회사명 가져오기
                                    TbCompany company = tbCompanyRepository.findByCompanyId(f.getCompanyId()).orElseThrow(IllegalArgumentException::new);
                                    f.setCompanyName(company.getCompanyName());

                                    //코드명 가져오기
                                    TbCmnCode code = tbCmnCodeRepository.findByGrpCdAndCd("TOPIC_TP_CD", f.getTpcTpCd()).orElseThrow(IllegalArgumentException::new);
                                    f.setTpcTpCdName(code.getCdNm());
                                }
                            })
                            .collect(Collectors.toList());
            logger.info("//// 리스트 출력 /////////////////////////////");
            logger.info("list - {}", new Gson().toJson(resList));
            logger.info("//// 리스트 출력 완료 /////////////////////////////");

            //조회내용 없을 때
            if(resList == null || resList.size() < 1){
                res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
                res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
                return res;
            }

            ListDto list = new ListDto();

            //총 카운트
            QueryResults<TbMssnCate> page = tbMssnCateRepository.selectPageInfoTbMssnCate(dto);
            Paging pa = new Paging();
            pa.setTotalSize(page.getTotal());
            pa.setTotalPage((page.getTotal() % dto.getSize() > 0) ? page.getTotal() / dto.getSize() + 1 : page.getTotal() / dto.getSize());
            pa.setSize(dto.getSize());
            pa.setPage(dto.getPage());

            //Return 값 등롣
            list.setList(resList);
            list.setSize(pa.getSize());
            list.setPage(pa.getPage());
            list.setTotalPage(pa.getTotalPage());
            list.setTotalSize(pa.getTotalSize());

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            res.setResult(list);
            return res;
        }  catch (Exception e){
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

    }

    public Response<ActionMissionCateVO> getOne(Long id) {

        Response<ActionMissionCateVO> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {
            //VO로 변환
            ActionMissionCateVO view = modelMapper.map(tbMssnCateRepository.findByCateId(id).orElseThrow(IllegalArgumentException::new), ActionMissionCateVO.class);
            logger.info("//// 내용 view 출력 /////////////////////////////");
            logger.info("view - {}", new Gson().toJson(view));
            logger.info("//// 내용 view 출력 완료 /////////////////////////////");

            //조회내용 없을 때
            if (view == null) {
                res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
                res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
                return res;
            }

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            res.setResult(view);
            return res;
        }  catch (Exception e){
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

    }

    @Transactional
    public Response<Void> saveContent(ActionMissionCateVO request, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        try {

            /******* 검사용
    //        Optional<ContinentEntity> continentEntity =
    //                Optional.ofNullable(continentRepository.findByCode(request.getCountryDto().getContinentCode()));
    //        logger.info("continentEntity - {}", continentEntity);


            //부모코드 조회내용 없을 때
    //        if(!continentEntity.isPresent()){
    //            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
    //            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
    //            return res;
    //        }

            //USEYN 값 없거나 틀릴 때
    //        if(request.getCountryDto().getUseType() == null || (request.getCountryDto().getUseType() != YnTypeEnum.Y && request.getCountryDto().getUseType() != YnTypeEnum.N)){
    //            res.setResultCd(ResultCodeConst.NOT_VALID_USEYN.getCode());
    //            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
    //            return res;
    //        }

            //Value 값 없을 때
    //        if(request.getCountryDto().getCountry() == null){
    //            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
    //            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
    //            return res;
    //        }
             ******/
            //임시로
            Long regId = 1L;
            Long modId = 1L;

            //미션 저장
            TbMssnCate newObj = TbMssnCate.builder()
//                    .cateId(request.getCateId())
                    .tpcTpCd(request.getTpcTpCd())
                    .imgUrl(request.getImgUrl())
                    .companyId(request.getCompanyId())
                    .useYn("Y")
                    .regId(regId)
                    .regDt(LocalDateTime.now())
                    .modId(modId)
                    .modDt(LocalDateTime.now())
                    .build();

            TbMssnCate saved = tbMssnCateRepository.save(newObj);

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));

            return res;
        }  catch (Exception e){
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }
    }

    @Transactional
    public Response<Void> updateContent(Long id, ActionMissionCateVO request, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //id 값 검사
        Optional<TbMssnCate> view = tbMssnCateRepository.findByCateId(id);

        //조회내용 없을 때
        if(!view.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try{
            //update - 콘텐츠도 같이 저장
            view.get().update(request);
            logger.info("//// 업데이트 내용 view 출력 /////////////////////////////");
            logger.info("view - {}", new Gson().toJson(view));
            logger.info("//// 업데이트 내용 view 출력 완료 /////////////////////////////");

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        } catch (Exception e){
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }
    }

    @Transactional
    public Response<Void> deleteContent(Long id, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //id 값 검사
        Boolean isContents = tbMssnCateRepository.existsByCateId(id);

        //조회내용 없을 때
        if(!isContents){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try{
            //delete - 컨텐츠 내용, 매핑도 같이 삭제해야 함
            tbMssnCateRepository.deleteByCateId(id);

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        } catch (Exception e){
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }
    }

}
