package com.hooxi.admin.biz.v1.story.model;

import com.hooxi.admin.constants.code.StoryStatCd;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class StoryResponse {
    private Long storyId;
    private String nickname;
    private String gradCd;
    private Long subscriberCnt;
    private Long viewCnt;
    private Long likeCnt;
    private String storyCntn;
    private Long replyCnt;
    private StoryStatCd storyStatCd;
    private String storyTpCd;
    private String feedTpCd;
    private LocalDateTime regDt;
    private LocalDateTime modDt;
}
