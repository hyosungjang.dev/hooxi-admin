package com.hooxi.admin.biz.v1.story.model;


import com.hooxi.admin.constants.code.FeedTpCd;
import com.hooxi.admin.constants.code.MmbrShpStatCd;
import com.hooxi.admin.constants.code.StoryStatCd;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class StoryListReq {


    private int page;
    private int size;

    private StoryStatCd state;
    private FeedTpCd feed;

    private String searchName;
    private String searchNameVal;

    private String startDt;
    private String endDt;


}
