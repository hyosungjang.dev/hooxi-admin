package com.hooxi.admin.biz.v1.aws;

import com.hooxi.admin.biz.v1.aws.model.res.BucketFileResponse;
import com.hooxi.admin.util.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/aws")
@CrossOrigin("*")
@RequiredArgsConstructor
@Api(tags = {"aws"})
public class AwsController {
    private final AwsService awsService;

    @Operation(
            summary = "s3 파일 업로드 presigned Url 생성"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "0000")
    })
    @GetMapping(value = "/pre-signed-url")
    public Response<List<BucketFileResponse>> countShare(
            @RequestParam("fileNames") List<String> fileNames
    ) {
        return awsService.getPreSignUrlCreate(fileNames);
    }
}
