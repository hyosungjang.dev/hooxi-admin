package com.hooxi.admin.biz.v1.aws;

import com.hooxi.admin.biz.v1.aws.model.res.BucketFileResponse;
import com.hooxi.admin.exception.common.CommonCodeException;
import com.hooxi.admin.util.Response;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
@RequiredArgsConstructor
public class AwsService {

    private final Logger logger = LoggerFactory.getLogger(AwsService.class);
    private final AwsApplication awsApplication;

    public Response<Void> uploadMultipartFile(List<MultipartFile> files) {
        logger.info("uploadMultipartFile");
        awsApplication.uploadImage(files);
        return new Response<>();
    }

    /**
     * Pre Sing Url create
     * @param fileNames
     * @return
     */
    public Response<List<BucketFileResponse>> getPreSignUrlCreate(List<String> fileNames) {
        logger.info("getPreSignUrlCreate");

        List<BucketFileResponse> result = awsApplication.makePreSignUrl(fileNames);

        if ( result == null ) {
            throw new CommonCodeException(String.format("fail to make presign url"));
        }

        return new Response<>(result);
    }

    /**
     * imaage 지우기
     * @param filePath
     * @return
     */
    public Response<Void> deleteImageByFilePath(String filePath) {
        logger.info("deleteImageByFilePath");
//        awsApplication.deleteImage(filePath);
        return new Response<>();
    }

}
