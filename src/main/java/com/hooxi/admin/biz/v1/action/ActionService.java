package com.hooxi.admin.biz.v1.action;

import com.hooxi.admin.biz.v1.action.model.req.*;
import com.hooxi.admin.biz.v1.action.model.res.*;
import com.hooxi.admin.biz.v1.aws.AwsApplication;
import com.hooxi.admin.biz.v1.aws.model.req.BucketFile;
import com.hooxi.admin.constants.code.ActionPlaceTpCd;
import com.hooxi.admin.constants.code.ActionPurposeTpCd;
import com.hooxi.admin.db.entity.mission.*;
import com.hooxi.admin.db.entity.mission.id.TbMssnRecomMappingId;
import com.hooxi.admin.util.Response;
import com.hooxi.admin.constants.code.LangTpCd;
import com.hooxi.admin.constants.code.TpcTpCd;
import com.hooxi.admin.db.repository.mission.*;
import com.hooxi.admin.exception.ErrorCode;
import com.hooxi.admin.exception.common.CommonCodeException;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ActionService {
    private final Logger log = LoggerFactory.getLogger(ActionService.class);

    //mssn
    private final TbMssnRepository mssnRepository;
    private final TbMssnContentsRepository mssnContentsRepository;

    //cate
    private final TbMssnCateRepository cateRepository;
    private final TbMssnCateContentsRepository cateContentsRepository;
    private final TbMssnCateMetaRepository cateMetaRepository;
    private final TbMssnCatePlaceRepository catePlaceRepository;

    // recom
    private final TbMssnRecomRepository recomRepository;
    private final TbMssnRecomMappingRepository recomMappingRepository;
    private final TbMssnRecomContentsRepository recomContentsRepository;

    //aws
    private final AwsApplication awsApplication;

    /**
     * @apiNote 액션 리스트 조회
     * @param langTpCd
     * @param pageable
     * @return
     */
    public Response<PageImpl<ActionListRes>> getActionList(ActionPlaceTpCd placeCd, ActionPurposeTpCd prpsCd, LangTpCd langTpCd, Pageable pageable) {
        PageImpl<ActionListRes> query = mssnRepository.findActionList(placeCd, prpsCd, langTpCd, pageable);
        query.getContent().forEach(item -> {
            item.setPrpsCdDesc(item.getPrpsCd().getDesc());
            item.setPlaceCdDesc(item.getPlaceCd().getDesc());
        });

        return new Response<>(query);
    }

    /**
     * @apiNote 카테고리 리스트 조회
     * @param langTpCd
     * @param pageable
     * @return
     */
    public Response<PageImpl<CategoryListRes>> getCategoryList(ActionPlaceTpCd placeCd, ActionPurposeTpCd prpsCd, LangTpCd langTpCd, Pageable pageable) {
        PageImpl<CategoryListRes> query = cateRepository.findCategoryList(placeCd, prpsCd, langTpCd, pageable);
        query.getContent().forEach(item -> {
            item.setPrpsCdDesc(item.getPrpsCd().getDesc());
            item.setPlaceCdDesc(item.getPlaceCd().getDesc());
        });
        return new Response<>(query);
    }

    /**
     * @apiNote 추천 액션 리스트 조회
     * @param langTpCd
     * @param pageable
     * @return
     */
    public Response<PageImpl<RecommendListRes>> getRecommendList(LangTpCd langTpCd, Pageable pageable) {
        PageImpl<RecommendListRes> query = recomRepository.findRecomList(langTpCd, pageable);
        return new Response<>(query);
    }

    /**
     * @apiNote 카테고리 상세 조회
     * @param cateId
     * @param langTpCd
     * @return
     */
    public Response<CategoryDtlRes> getCategoryDtl(Long cateId, LangTpCd langTpCd) {
        CategoryDtlRes res = cateRepository.findCategoryByCateIdAndLangTpCd(cateId, langTpCd);
        if (res == null) {
            throw new CommonCodeException(ErrorCode.NOT_FOUND_CATE);
        }
        return new Response<>(res);
    }

    /**
     * @apiNote 카테고리 등록
     * @param req
     * @return
     */
    @Transactional
    public Response<Void> registerCate(RegCateReq req) {
        BucketFile bucketFile = null;
        if (req.getCateImg() != null && !req.getCateImg().isEmpty()) {
            bucketFile = awsApplication.uploadOneImage(req.getCateImg());
        }
        TbMssnCate cate = cateRepository
                .save(req.toCateEntity(TpcTpCd.ACTION, bucketFile == null ? null : bucketFile.getFileUrl()));
        cateContentsRepository.save(req.toCateContentsEntity(cate.getCateId()));
        cateMetaRepository.save(req.toCateMetaEntity(cate.getCateId()));
        catePlaceRepository.save(req.toCatePlaceEntity(cate.getCateId()));
        return new Response<>();
    }

    /**
     * @apiNote 카테고리 수정
     * @param req
     * @return
     */
    @Transactional
    public Response<Void> modifyCate(ModCateReq req) {
        TbMssnCate cate = cateRepository.findTbMssnCateByCateId(req.getCateId());
        if (cate == null) throw new CommonCodeException(ErrorCode.NOT_FOUND_CATE);

        BucketFile bucketFile = null;
        // s3 업로드 후 db update
        if (req.getCateImg() != null && !req.getCateImg().isEmpty()) {
            // 이미지 삭제
            awsApplication.deleteByFileUrl(cate.getImgUrl());
            bucketFile = awsApplication.uploadOneImage(req.getCateImg());
        }

        cate.updateCate(req, bucketFile != null ? bucketFile.getFileUrl() : cate.getImgUrl());

        //컨텐츠 정보
        TbMssnCateContents contents = cateContentsRepository.findByCateIdAndLangCd(req.getCateId(),  req.getLangTpCd());

        if (contents == null) {
            cateContentsRepository.save(req.toContentsEntity());
        } else {
            contents.update(req);
        }

        // placeCd, prpsCd update
        catePlaceRepository.updatePlaceCdByCateId(req.getCateId(), req.getPlaceCd().getLegacyCode());
        cateMetaRepository.updatePrpsCdByCateId(req.getCateId(), req.getPrpsCd().getLegacyCode());

        return new Response<>();
    }

    /**
     * @apiNote 액션 상세 조회
     * @param mssnId
     * @param langTpCd
     * @return
     */
    public Response<ActionDtlRes> getActionDtl(Long mssnId, LangTpCd langTpCd) {
        ActionDtlRes res = mssnRepository.findActionDtlByMssnId(mssnId, langTpCd);
        if (res == null) throw new CommonCodeException(ErrorCode.NOT_FOUND_ACTION);

        res.setPrpsCdDesc(res.getPrpsCd().getDesc());
        res.setPlaceCdDesc(res.getPlaceCd().getDesc());

        List<TbMssnRecomMapping> mappings = recomMappingRepository.findByMssnId(mssnId);
        List<Long> recomIdList =  mappings.stream().map(TbMssnRecomMapping::getRecomId).collect(Collectors.toList());

        res.setRecAction(recomRepository.findRecomMappingByRecomId(recomIdList, langTpCd));


        return new Response<>(res);
    }

    /**
     * @apiNote 액션 등록
     * @param req
     * @return
     */
    @Transactional
    public Response<Void> registerAction(RegActionReq req) {
        BucketFile file = null;

        if (req.getActImg() != null && !req.getActImg().isEmpty()) {
            file = awsApplication.uploadOneImage(req.getActImg());
        }

        TbMssn mssn = mssnRepository.save(req.toMssnEntity(TpcTpCd.ACTION, file != null ? file.getFileUrl() : null));

        mssnContentsRepository.save(req.toContentsEntity(mssn.getMssnId()));

        List<TbMssnRecomMapping> recomMappings = new ArrayList<>();
        if (req.getRecomIdList() != null && req.getRecomIdList().size() > 0) {
            for (Long recomId : req.getRecomIdList()) {
                TbMssnRecomMapping mapping = TbMssnRecomMapping.builder()
                        .recomId(recomId)
                        .mssnId(mssn.getMssnId())
                        .build();
                recomMappings.add(mapping);
            }
        }

        recomMappingRepository.saveAll(recomMappings);
        return new Response<>();
    }

    /**
     * @apiNote 액션 수정
     * @param req
     * @return
     */
    @Transactional
    public Response<Void> modifyAction(ModActionReq req) {
        TbMssn mssn = mssnRepository.findTbMssnByMssnId(req.getMssnId());

        if (mssn == null) throw new CommonCodeException(ErrorCode.NOT_FOUND_ACTION);

        Optional<TbMssnCate> cate = cateRepository.findByCateId(req.getCateId());

        if (cate.isEmpty()) throw  new CommonCodeException(ErrorCode.NOT_FOUND_CATE);

        BucketFile file = null;
        if (req.getActImg() != null && !req.getActImg().isEmpty()) {
            //이미지 삭제
            awsApplication.deleteByFileUrl(mssn.getImgUrl());
            file = awsApplication.uploadOneImage(req.getActImg());
        }

        mssn.updateMssn(req, file != null ? file.getFileUrl() : mssn.getImgUrl());

        TbMssnContents contents = mssnContentsRepository
                .findTbMssnContentsByMssnIdAndLangCd(req.getMssnId(), LangTpCd.KO);
        if (contents ==  null) {
            mssnContentsRepository.save(req.toContentsEntify());
        } else {
            contents.updateContents(req);
        }

        List<TbMssnRecomMapping> mappings = new ArrayList<>();
        if (req.getRecomIdList() != null && req.getRecomIdList().size() > 0) {
            for (Long recomId : req.getRecomIdList()) {
                // 전달받은 recomId 로 조회한 recom 이 없을 경우 저장하지 않음
                if (recomRepository.existsByRecomId(recomId)) {
                    TbMssnRecomMapping mapping = TbMssnRecomMapping.builder()
                            .mssnId(req.getMssnId())
                            .recomId(recomId)
                            .useYn("Y")
                            .build();
                    mappings.add(mapping);
                }
            }
        }
        recomMappingRepository.deleteByMssnId(req.getMssnId());
        recomMappingRepository.saveAll(mappings);

        return new Response<>();
    }

    /**
     * @apiNote 추천 상세 조회
     * @param recomId
     * @param langCd
     * @return
     */
    public Response<RecomDtlRes> getRecomDtl(Long recomId, LangTpCd langCd) {
        RecomDtlRes res = recomRepository.findRecomDtlByRecomId(recomId, langCd);
        if (res == null) throw new CommonCodeException(ErrorCode.NOT_FOUND_RECOM);
        List<Long> mssnIdList = recomMappingRepository.findByRecomId(recomId)
                .stream().map(TbMssnRecomMapping::getMssnId)
                .collect(Collectors.toList());

        res.setActionList(recomRepository.findActionRecomByMssnId(mssnIdList, langCd));

        return new Response<>(res);
    }

    /**
     * @apiNote 추천 등록
     * @param req
     * @return
     */
    public Response<Void> registerRecom(RegRecomReq req) {
        BucketFile bucketFile = null;

        if (req.getRecomImg() != null && !req.getRecomImg().isEmpty()) {
            bucketFile = awsApplication.uploadOneImage(req.getRecomImg());
        }

        TbMssnRecom recom = recomRepository.save(req.toRecomEntity(bucketFile != null ? bucketFile.getFileUrl() : null));
        recomContentsRepository.save(req.toContentsEntity(recom.getRecomId()));

        if (req.getActionIdList() != null && req.getActionIdList().size() > 0) {
            List<TbMssnRecomMapping> mappings = new ArrayList<>();
            for (Long mssnId : req.getActionIdList()) {
                TbMssnRecomMapping mapping = TbMssnRecomMapping.builder()
                        .recomId(recom.getRecomId())
                        .mssnId(mssnId)
                        .build();
                mappings.add(mapping);
            }
            recomMappingRepository.saveAll(mappings);
        }

        return new Response<>();
    }

    /**
     * @apiNote 추천 수정
     * @param req
     * @return
     */
    @Transactional
    public Response<Void> modifyRecom(ModRecomReq req) {
        TbMssnRecom recom = recomRepository.findByRecomId(req.getRecomId());

        if (recom == null) throw new CommonCodeException(ErrorCode.NOT_FOUND_RECOM);

        BucketFile bucketFile = null;
        if (req.getRecomImg() != null && !req.getRecomImg().isEmpty()) {
            awsApplication.deleteByFileUrl(recom.getImgUrl());
            bucketFile = awsApplication.uploadOneImage(req.getRecomImg());
        }

        recom.updateRecom(req, bucketFile != null ? bucketFile.getFileUrl() : recom.getImgUrl());

        TbMssnRecomContents contents = recomContentsRepository.findByRecomIdAndLangCd(req.getRecomId(), LangTpCd.KO);

        // 컨텐츠 업데이트
        contents.updateContents(req);

        List<TbMssnRecomMapping> mappings = new ArrayList<>();
        if (req.getActionIdList() != null && req.getActionIdList().size() > 0) {
            for (Long mssnId : req.getActionIdList()) {
                // 전달받은 mssnId 로 조회한 mssn 이 존재하지 않을 경우 저장하지 않음
                if (mssnRepository.existsByMssnId(mssnId)) {
                    TbMssnRecomMapping mapping = TbMssnRecomMapping.builder()
                            .mssnId(mssnId)
                            .recomId(req.getRecomId())
                            .useYn("Y")
                            .build();
                    mappings.add(mapping);
                }
            }
        }

        recomMappingRepository.deleteByRecomId(req.getRecomId());
        recomMappingRepository.saveAll(mappings);

        return new Response<>();
    }

}
