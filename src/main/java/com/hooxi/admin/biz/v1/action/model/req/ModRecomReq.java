package com.hooxi.admin.biz.v1.action.model.req;

import com.hooxi.admin.constants.code.LangTpCd;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Getter
@Setter
public class ModRecomReq {
    private Long recomId;
    private String recomTitle;
    private String recomContents;
    private String useYn;
    private List<Long> actionIdList;
    @Value("KO")
    private LangTpCd langTpCd;
    private MultipartFile recomImg;
}
