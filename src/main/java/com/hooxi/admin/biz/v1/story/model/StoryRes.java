package com.hooxi.admin.biz.v1.story.model;


import com.hooxi.admin.biz.v1.certify.model.CertifyRes;
import com.hooxi.admin.biz.v1.reply.model.ReplyRes;
import com.hooxi.admin.constants.code.FeedTpCd;
import com.hooxi.admin.constants.code.StoryStatCd;
import com.hooxi.admin.constants.code.StoryTpCd;
import com.hooxi.admin.db.entity.story.TbStoryFile;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class StoryRes {
    private StoryStatCd storyStatCd;

    private StoryTpCd storyTpCd;

    private FeedTpCd feedTpCd;

    private Long actTlId;

    private String storyCntn;

    private String linkUrl;

    private String profileImg;

    private String nickname;

    private String afltn;

    private List<TbStoryFile> files;

    private List<ReplyRes> replies;

    private CertifyRes certifyRes;
}
