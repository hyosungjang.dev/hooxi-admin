package com.hooxi.admin.biz.v1.user.model;

import com.hooxi.admin.constants.code.GradTpCd;
import com.hooxi.admin.constants.code.LangTpCd;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class UserProfileRes {

    private Long mmbrId;

    private Long mmbrshpId;

    private String nickname;

    private String email;

    private LangTpCd mainLang;

    private String profileImg;

    private LocalDateTime regDt;

    private GradTpCd gradCd;

    private String mmbrshpNum;

    private String stateMsg;

    private Long report;

    private Long subscribe;

    private Long subscriber;

    private Long view;

    private Long certify;

    private String deviceOs;

    private String deviceName;

    private LocalDateTime loginDttm;

    private Long loginDays;

    private List<MmbrshpJobsRes> jobs;
}
