package com.hooxi.admin.biz.v1.certify.model;

import com.hooxi.admin.constants.code.FeedTpCd;
import com.hooxi.admin.constants.code.GradTpCd;
import com.hooxi.admin.constants.code.StoryStatCd;
import com.hooxi.admin.constants.code.StoryTpCd;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
public class CertifyListRes {
    private Long storyId;
    private Long mmbrId;
    private Long actTlId;
    private StoryStatCd storyStatCd;
    private StoryTpCd storyTpCd;
    private FeedTpCd feedTpCd;
    private String storyCntn;
    private Long viewCnt;
    private Long shareCnt;
    private Long likeCnt;
    private LocalDateTime regDt;
    private LocalDateTime modDt;
    private String profileImg;
    private String nickname;
    private String gradCd;
    private String gradCdDesc;
    private String mainImgUrl;
    private Long replyCnt;
    private String storyStatCdDesc;
    private String storyTpCdDesc;
    private String feedTpCdDesc;
}
