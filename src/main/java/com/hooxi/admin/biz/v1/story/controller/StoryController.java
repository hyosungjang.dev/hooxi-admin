package com.hooxi.admin.biz.v1.story.controller;

import com.hooxi.admin.biz.v1.story.model.*;
import com.hooxi.admin.biz.v1.story.service.StoryService;
import com.hooxi.admin.constants.code.FeedTpCd;
import com.hooxi.admin.constants.code.StoryStatCd;
import com.hooxi.admin.util.Response;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@RestController
@RequestMapping(path = "/story")
@CrossOrigin("*")
@RequiredArgsConstructor
@Api(tags = {"스토리"})
public class StoryController {
    private final StoryService storyService;

    @GetMapping(path = "/list")
    @Operation(summary = "스토리 리스트 조회")
    public Response<PageImpl<StoryListRes>> getList(Pageable pageable, @RequestParam(value = "state") StoryStatCd state, @RequestParam(value = "feed") FeedTpCd feed) {
        StoryListReq req = new StoryListReq();
        req.setState(state);
        req.setFeed(feed);

        return storyService.getList(pageable, req);
    }

    @GetMapping(path = "/count")
    @Operation(summary = "상태별 스토리 카운트")
    public Response<HashMap<String, Object>> getCount(@RequestParam(value = "feed") FeedTpCd feed) {
        return storyService.getCount(feed);
    }

    @GetMapping(path = "/{storyId}")
    @Operation(summary = "스토리 상세")
    public Response<StoryRes> getStory(@PathVariable Long storyId) {
        return storyService.getStory(storyId);
    }

    @PostMapping(path = "/update/cntn")
    @Operation(summary = "스토리 내용 수정")
    public Response<Void> updateStoryCntn(@RequestBody StoryReq storyReq) {
        return storyService.updateStoryCntn(storyReq);
    }

    @PostMapping(path = "/update/state")
    @Operation(summary = "스토리 승인/반려")
    public Response<Void> updateStoryState(@RequestBody StoryReq storyReq) {
        return storyService.updateStoryState(storyReq);
    }

}
