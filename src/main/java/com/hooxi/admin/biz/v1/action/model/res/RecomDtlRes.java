package com.hooxi.admin.biz.v1.action.model.res;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class RecomDtlRes {
    private Long recomId;
    private String imgUrl;
    private String recomTitle;
    private String recomContents;
    private String useYn;
    private List<ActionRecomRes> actionList;
}
