package com.hooxi.admin.biz.v1.user.model;


import com.hooxi.admin.constants.code.MmbrShpStatCd;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserListReq {


    private int page;
    private int size;

    //멤버십 상태
    private MmbrShpStatCd type;

    private String searchName;
    private String searchNameVal;

    private String startDt;
    private String endDt;


}
