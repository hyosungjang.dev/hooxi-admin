package com.hooxi.admin.biz.v1.action.model.res;

import com.hooxi.admin.constants.code.ActionPlaceTpCd;
import com.hooxi.admin.constants.code.ActionPurposeTpCd;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class CategoryListRes {
    @ApiModelProperty(value = "카테고리 id")
    private Long cateId;
    @ApiModelProperty(value = "카테고리 타이틀")
    private String cateTitle;
    @ApiModelProperty(value = "부문 코드")
    private ActionPurposeTpCd prpsCd;
    @ApiModelProperty(value = "장소 코드")
    private ActionPlaceTpCd placeCd;
    @ApiModelProperty(value = "이미지 url")
    private String imgUrl;
    @ApiModelProperty(value = "등록자 회원 id")
    private Long regId;
    @ApiModelProperty(value = "등록자")
    private String nickname;
    @ApiModelProperty(value = "등록일")
    private LocalDateTime regDt;
    @ApiModelProperty(value = "수정일")
    private LocalDateTime modDt;
    @ApiModelProperty(value = "부문")
    private String prpsCdDesc;
    @ApiModelProperty(value = "장소")
    private String placeCdDesc;
    @ApiModelProperty(value = "사용 여부")
    private String useYn;
}
