package com.hooxi.admin.biz.v1.challenge.model.res;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class RecomChallengeListRes {
    private Long cateId;
    private String imgUrl;
    private String cateTitle;
    private String cateContents;
    private Long challengeCnt;
    private Integer totalPoint;
    private Long regId;
    private String nickname;
    private LocalDateTime regDt;
    private LocalDateTime modDt;
}
