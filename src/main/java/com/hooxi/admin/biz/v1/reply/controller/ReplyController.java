package com.hooxi.admin.biz.v1.reply.controller;

import com.hooxi.admin.biz.v1.reply.model.ReplyReq;
import com.hooxi.admin.biz.v1.reply.model.ReplyRes;
import com.hooxi.admin.biz.v1.reply.service.ReplyService;
import com.hooxi.admin.constants.code.StoryStatCd;
import com.hooxi.admin.db.entity.story.TbReply;
import com.hooxi.admin.util.Response;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping(path = "/reply")
@CrossOrigin("*")
@RequiredArgsConstructor
@Api(tags = {"댓글"})
public class ReplyController {
    private final ReplyService replyService;

    @GetMapping(path = "/re/list/{replyId}")
    @Operation(summary = "대댓글 리스트")
    public Response<List<ReplyRes>> getReReplyList(@PathVariable Long replyId) {
        return replyService.getReReplyList(replyId);
    }

    @PostMapping(path = "/update/state")
    @Operation(summary = "댓글 보이기/숨기기")
    public Response<Void> updateState(@RequestBody ReplyReq replyReq) {
        return replyService.updateState(replyReq);
    }
}
