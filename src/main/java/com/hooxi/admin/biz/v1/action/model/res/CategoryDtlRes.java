package com.hooxi.admin.biz.v1.action.model.res;

import com.hooxi.admin.constants.code.ActionPlaceTpCd;
import com.hooxi.admin.constants.code.ActionPurposeTpCd;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CategoryDtlRes {
    private Long cateId;
    private String cateTitle;
    private String cateContents;
    private String imgUrl;
    private ActionPurposeTpCd prpsCd;
    private ActionPlaceTpCd placeCd;
}