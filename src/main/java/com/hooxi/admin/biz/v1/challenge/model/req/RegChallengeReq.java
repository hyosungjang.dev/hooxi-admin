package com.hooxi.admin.biz.v1.challenge.model.req;

import com.hooxi.admin.constants.code.LangTpCd;
import com.hooxi.admin.constants.code.ParticptnTpCd;
import com.hooxi.admin.constants.code.TpcTpCd;
import com.hooxi.admin.db.entity.mission.TbMssn;
import com.hooxi.admin.db.entity.mission.TbMssnChallenge;
import com.hooxi.admin.db.entity.mission.TbMssnContents;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Getter
@Setter
public class RegChallengeReq {
    @ApiModelProperty(value = "챌린지 타이틀")
    private String mssnTitle;
    @ApiModelProperty(value = "챌린지 소개")
    private String mssnTitleDtl;
    @ApiModelProperty(value = "챌린지 방법")
    private String mssnEtc;
    @ApiModelProperty(value = "챌린지 효과")
    private String mssnEffect;
    @ApiModelProperty(value = "링크")
    private String linkUrl;
    @ApiModelProperty(value = "챌린지 주최 아이디")
    private Long cateId;
    @ApiModelProperty(value = "포인트")
    private Integer point;
    @ApiModelProperty(value = "exp")
    private Integer exp;
    @ApiModelProperty(value = "언어코드")
    private LangTpCd langTpCd;
    @ApiModelProperty(value = "사용 여부")
    private String useYn;
    @ApiModelProperty(value = "이미지")
    private MultipartFile imgUrl;

    @ApiModelProperty(value = "참여 유형 코드", example = "MULTI")
    private ParticptnTpCd particptnTpCd;

    @ApiModelProperty(value = "기간 여부", example = "Y")
    private String rangeYn;
    @ApiModelProperty(value = "시작일자", example = "20220907")
    private String startDt;
    @ApiModelProperty(value = "종료일자", example = "20220907")
    private String endDt;

    public TbMssn toMssnEntity(String imgUrl) {
        return TbMssn.builder()
                .tpcTpCd(TpcTpCd.CHALLENGE)
                .imgUrl(imgUrl)
                .useYn(useYn)
                .linkUrl(linkUrl)
                .cateId(cateId)
                .exp(exp)
                .point(point)
                .regDt(LocalDateTime.now())
                .regId(3L)
                .modDt(LocalDateTime.now())
                .modId(3L)
                .build();
    }

    public TbMssnContents toMssnContentsEntity(Long mssnId) {
        return TbMssnContents.builder()
                .mssnId(mssnId)
                .langCd(langTpCd)
                .mssnEffect(mssnEffect)
                .mssnEtc(mssnEtc)
                .mssnTitle(mssnTitle)
                .mssnTitleDtl(mssnTitleDtl)
                .build();
    }

    public TbMssnChallenge toChallengeEntity(Long mssnId) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
        LocalDateTime endDt = LocalDate.parse(this.endDt,  formatter).atStartOfDay();
        LocalDateTime startDt = LocalDate.parse(this.startDt,  formatter).atStartOfDay();
        return TbMssnChallenge.builder()
                .endDt(endDt)
                .particptnTpCd(particptnTpCd)
                .startDt(startDt)
                .rangeYn(rangeYn)
                .mssnId(mssnId)
                .build();
    }
}
