package com.hooxi.admin.biz.v1.user.model;

import java.time.LocalDateTime;

public interface UserListResInterface {

    Long getMmbr_id();

    String getNickname();

    String getEmail();

    String getMain_lang();

    String getOauth_tp_cd();

    String getCreator();

    String getMmbrshp_state_cd();

    LocalDateTime getReg_dt();

    LocalDateTime getLogin_dttm();

}
