package com.hooxi.admin.biz.v1.user.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class MmbrshpJobsRes {

    private Long mmbrshpJobId;

    private Long mmbrshpId;

    private String jobKywdTpCd;

    private String jobKywdTpCdDesc;
}
