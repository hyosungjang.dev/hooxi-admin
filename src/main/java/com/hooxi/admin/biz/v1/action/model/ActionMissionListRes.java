package com.hooxi.admin.biz.v1.action.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Setter
@Getter
@NoArgsConstructor
public class ActionMissionListRes {

    @Schema(description = "mssn id값", nullable = false, example = "1")
    private Long mssnId;
    @Schema(description = "category id값", nullable = false, example = "1")
    private Long cateId;
    @Schema(description = "토픽 코드", nullable = false, example = "TPC002")
    private String tpcTpCd;
    @Schema(description = "토픽 코드명", nullable = false, example = "액션")
    private String tpcTpCdName;
    @Schema(description = "이미지URL", nullable = false, example = "https://...")
    private String imgUrl;
    @Schema(description = "화폐표식", nullable = false, example = "100")
    private int exp;
    @Schema(description = "포인트", nullable = false, example = "1000")
    private int point;
    @Schema(description = "조회수", nullable = false, example = "118")
    private long hit;
    @Schema(description = "사용여부", nullable = false, example = "118")
    private String uesYn;
    @Schema(description = "등록자", nullable = false, example = "118")
    private Long regId;
    @Schema(description = "등록자 이름", nullable = false, example = "Yoon Jung Sun")
    private String regUsrName;
    @Schema(description = "등록일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime regDt;
    @Schema(description = "수정자", nullable = false, example = "118")
    private Long modId;
    @Schema(description = "수정자 이름", nullable = false, example = "Yoon Jung Sun")
    private String modUsrName;
    @Schema(description = "수정일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime modDt;

    @Builder
    public ActionMissionListRes(Long mssnId,
                                Long cateId,
                                String tpcTpCd,
                                String tpcTpCdName,
                                String imgUrl,
                                int exp,
                                int point,
                                long hit,
                                String uesYn,
                                Long regId,
                                String regUsrName,
                                LocalDateTime regDt,
                                Long modId,
                                String modUsrName,
                                LocalDateTime modDt
    ) {
        this.mssnId = mssnId;
        this.cateId = cateId;
        this.tpcTpCd = tpcTpCd;
        this.tpcTpCdName = tpcTpCdName;
        this.imgUrl = imgUrl;
        this.exp = exp;
        this.point = point;
        this.hit = hit;
        this.uesYn = uesYn;
        this.regDt = regDt;
        this.regId = regId;
        this.regUsrName = regUsrName;
        this.modDt = modDt;
        this.modId = modId;
        this.modUsrName = modUsrName;
    }

}
