package com.hooxi.admin.biz.v1.certify.model;

import com.hooxi.admin.constants.code.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class CertifyRes {
    private ActStatusCd actStatusCd;

    private String actStatusCdDesc;

    private ActionPlaceTpCd placeCd;

    private ActionPurposeTpCd prpsCd;

    private String cateTitle;

    private String cateContents;

    private String companyName;
}
