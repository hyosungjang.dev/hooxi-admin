package com.hooxi.admin.biz.v1.admin.model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AdminListRes {


    private Long adminId;
    private String adminEmail;
    private String adminName;
    private String adminPwd;
    private Long adminPmsId;
    private String useYn;
    private Long regId;
    private LocalDateTime regDttm;
    private String adminPmsNm;


}
