package com.hooxi.admin.biz.v1.user.model;

import com.hooxi.admin.config.type.YnType;
import com.hooxi.admin.constants.code.LangTpCd;
import com.hooxi.admin.constants.code.MmbrShpStatCd;
import com.hooxi.admin.constants.code.OauthTpCd;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
public class UserListRes {

    @Schema(description = "mmbr id값")
    private Long mmbrId;

    @Schema(description = "활동명")
    private String nickname;

    @Schema(description = "계정")
    private String email;

    @Schema(description = "주언어", nullable = true)
    private LangTpCd mainLang;

    @Schema(description = "가입유형")
    private OauthTpCd oauthTpCd;

    @Schema(description = "가입일", pattern = "yy-MM-dd hh:mm:ss")
    private LocalDateTime regDt;

    @Schema(description = "최근접속일", pattern = "yy-MM-dd hh:mm:ss")
    private LocalDateTime loginDttm;

    @Schema(description = "멤버십Id", nullable = true)
    private Long mmbrshpId;

    @Schema(description = "크리에이터명", nullable = true)
    private String creator;

    @Schema(description = "멤버십 상태", nullable = true)
    private MmbrShpStatCd mmbrshpStateCd;

    private String mmbrshpDesc;

}
