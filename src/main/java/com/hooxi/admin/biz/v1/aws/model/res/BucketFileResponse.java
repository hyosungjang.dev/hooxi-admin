package com.hooxi.admin.biz.v1.aws.model.res;

import lombok.Builder;
import lombok.Data;

@Data
public class BucketFileResponse {

    private String fileName;
    private String filePath;
    private String preSignUrl;

    @Builder
    BucketFileResponse(String fileName, String preSignUrl, String filePath) {
        this.fileName = fileName;
        this.filePath = filePath;
        this.preSignUrl = preSignUrl;
    }


}
