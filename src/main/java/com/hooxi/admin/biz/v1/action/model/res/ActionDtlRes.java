package com.hooxi.admin.biz.v1.action.model.res;

import com.hooxi.admin.constants.code.ActionPlaceTpCd;
import com.hooxi.admin.constants.code.ActionPurposeTpCd;
import com.hooxi.admin.constants.code.LangTpCd;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ActionDtlRes {
    private Long mssnId;
    private Long cateId;
    private ActionPurposeTpCd prpsCd;
    private ActionPlaceTpCd placeCd;
    private String useYn;
    private String cateTitle;
    private Integer point;
    private Integer exp;
    private String imgUrl;
    private String mssnTitle;
    private String mssnEtc;
    private String mssnTitleDtl;
    private String mssnEffect;
    private LangTpCd langCd;

    private String prpsCdDesc;
    private String placeCdDesc;

    private List<RecomDtlRes> recAction;
}
