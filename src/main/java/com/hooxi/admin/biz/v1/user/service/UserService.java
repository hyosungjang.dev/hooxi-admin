package com.hooxi.admin.biz.v1.user.service;

import com.hooxi.admin.biz.v1.user.model.*;
import com.hooxi.admin.config.type.ResultCodeConst;
import com.hooxi.admin.constants.code.GradTpCd;
import com.hooxi.admin.constants.code.MmbrShpStatCd;
import com.hooxi.admin.db.entity.member.TbMmbrshpArticle;
import com.hooxi.admin.db.entity.member.TbMmbrshpSnsLink;
import com.hooxi.admin.db.repository.member.TbMmbrshpArticleRepository;
import com.hooxi.admin.db.repository.member.TbMmbrshpJobRepository;
import com.hooxi.admin.db.repository.member.TbMmbrshpRepository;
import com.hooxi.admin.db.repository.member.TbMmbrshpSnsLinkRepository;
import com.hooxi.admin.db.repository.user.TbLoginHistRepository;
import com.hooxi.admin.db.repository.user.TbMmbrRepository;
import com.hooxi.admin.util.Response;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UserService {

    private final Logger logger = LoggerFactory.getLogger(UserService.class);
    private final ModelMapper modelMapper;
    private final HttpServletRequest httpServletRequest;
    private final MessageSourceAccessor messageSourceAccessor;

    private final TbMmbrRepository tbMmbrRepository;
    private final TbLoginHistRepository tbLoginHistRepository;
    private final TbMmbrshpRepository tbMmbrshpRepository;
    private final TbMmbrshpArticleRepository tbMmbrshpArticleRepository;
    private final TbMmbrshpSnsLinkRepository tbMmbrshpSnsLinkRepository;
    private final TbMmbrshpJobRepository tbMmbrshpJobRepository;

    public Response<PageImpl<UserListRes>> getList(Pageable pageable, UserListReq dto) {

        Response<PageImpl<UserListRes>> res = new Response<>();

        try{
            PageImpl<UserListRes> resList =
                    tbMmbrRepository.selectUserList(pageable, dto);

            resList.getContent().stream().forEach(e -> {
                if(e.getMmbrshpStateCd() != null) e.setMmbrshpDesc(e.getMmbrshpStateCd().getDesc());
            });

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            res.setResult(resList);
            return res;
        }  catch (Exception e){
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

    }

    public Response<HashMap<String, Object>> getCount() {
        Response<HashMap<String, Object>> res = new Response<>();
        HashMap<String, Object> map = new HashMap<>();

        try{
            Long all = tbMmbrRepository.selectAllCount();
            Long normal = tbMmbrRepository.selectMmbrshpCount(MmbrShpStatCd.NORMAL);
            Long waiting = tbMmbrRepository.selectMmbrshpCount(MmbrShpStatCd.WAITING);
            Long reject = tbMmbrRepository.selectMmbrshpCount(MmbrShpStatCd.REJECT);
            Long stop = tbMmbrRepository.selectMmbrshpCount(MmbrShpStatCd.STOP);

            map.put("all", all);
            map.put("normal", normal);
            map.put("waiting", waiting);
            map.put("reject", reject);
            map.put("stop", stop);

            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            res.setResult(map);

            return res;
        }  catch (Exception e){
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

    }

    public Response<UserProfileRes> getProfile(Long mmbrId) {
        Response<UserProfileRes> res = new Response<>();
        List<MmbrshpJobsRes> jobs = new ArrayList<>();

        try{
            UserProfileRes userProfileRes = tbMmbrRepository.selectUserProfile(mmbrId);
            UserProfileRes loginInfo = tbLoginHistRepository.selectCurrentLoginInfo(mmbrId);



            if(loginInfo != null){
                userProfileRes.setLoginDttm(loginInfo.getLoginDttm());
                userProfileRes.setLoginDays(loginInfo.getLoginDays());
                userProfileRes.setDeviceOs(loginInfo.getDeviceOs());
                userProfileRes.setDeviceName(loginInfo.getDeviceName());
            }

            if(userProfileRes.getMmbrshpId() != null) {
                jobs = tbMmbrshpJobRepository.selectInfo(userProfileRes.getMmbrshpId());
            }
            userProfileRes.setJobs(jobs);

            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            res.setResult(userProfileRes);

            return res;
        }  catch (Exception e){
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

    }

    public Response<MmbrshpRes> getMmbrshp(Long mmbrshpId) {
        Response<MmbrshpRes> res = new Response<>();

        try{
            MmbrshpRes userMmbrshpRes = tbMmbrshpRepository.selectInfo(mmbrshpId);
            MmbrShpStatCd mmbrShpStatCd = userMmbrshpRes.getMmbrshpStateCd();
            if(mmbrShpStatCd != null) userMmbrshpRes.setMmbrshpStateDesc(mmbrShpStatCd.getDesc());

            List<TbMmbrshpArticle> tbMmbrshpArticles = tbMmbrshpArticleRepository.selectInfo(mmbrshpId);
            List<TbMmbrshpSnsLink> tbMmbrshpSnsLinks = tbMmbrshpSnsLinkRepository.selectInfo(mmbrshpId);

            userMmbrshpRes.setArticles(tbMmbrshpArticles);
            userMmbrshpRes.setSnsLinks(tbMmbrshpSnsLinks);

            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            res.setResult(userMmbrshpRes);

            return res;
        }  catch (Exception e){
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

    }

    public Response<Void> confirmMmbrshp(Long mmbrshpId){
        Response<Void> res = new Response<>();

        try{
            MmbrshpConfirmReq mmbrshpConfirmReq = new MmbrshpConfirmReq();
            mmbrshpConfirmReq.setMmbrshpId(mmbrshpId);
            mmbrshpConfirmReq.setMmbrshpStateCd(MmbrShpStatCd.NORMAL);
            mmbrshpConfirmReq.setGradCd(GradTpCd.WELCOME);
            mmbrshpConfirmReq.setConfirmDt(LocalDateTime.now());

            Long confirm = tbMmbrshpRepository.confirmMmbrshp(mmbrshpConfirmReq);
            res.setResultCd(confirm > 0 ? ResultCodeConst.SUCCESS.getCode() : ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));

            return res;
        }  catch (Exception e){
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }
    }
}
