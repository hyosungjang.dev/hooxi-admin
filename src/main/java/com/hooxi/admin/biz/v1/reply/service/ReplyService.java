package com.hooxi.admin.biz.v1.reply.service;

import com.hooxi.admin.biz.v1.reply.model.*;
import com.hooxi.admin.config.type.ResultCodeConst;
import com.hooxi.admin.constants.code.StoryStatCd;
import com.hooxi.admin.db.entity.story.TbReply;
import com.hooxi.admin.db.entity.story.TbStoryFile;
import com.hooxi.admin.db.repository.story.TbReplyRepository;
import com.hooxi.admin.db.repository.story.TbStoryFileRepository;
import com.hooxi.admin.db.repository.story.TbStoryRepository;
import com.hooxi.admin.util.Response;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ReplyService {
    private final Logger log = LoggerFactory.getLogger(ReplyService.class);

    private final MessageSourceAccessor messageSourceAccessor;

    private final TbReplyRepository tbReplyRepository;

    public Response<List<ReplyRes>> getReReplyList(Long replyId) {
        Response<List<ReplyRes>> res = new Response<>();

        try{
            List<ReplyRes> list = tbReplyRepository.selectReReplies(replyId);

            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            res.setResult(list);

            return res;
        }  catch (Exception e){
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }
    }

    public Response<Void> updateState(ReplyReq replyReq) {
        Response<Void> res = new Response<>();

        try{
            Long update = tbReplyRepository.updateState(replyReq);

            res.setResultCd(update > 0 ? ResultCodeConst.SUCCESS.getCode() : ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));

            return res;
        }  catch (Exception e){
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

    }
}
