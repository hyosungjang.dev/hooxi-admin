package com.hooxi.admin.biz.v1.challenge.model.res;

import com.hooxi.admin.constants.code.ActionPlaceTpCd;
import com.hooxi.admin.constants.code.ActionPurposeTpCd;
import com.hooxi.admin.constants.code.LangTpCd;
import com.hooxi.admin.constants.code.ParticptnTpCd;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class ChallengeDtlRes {
    private Long mssnId;
    private Long cateId;
    private Long companyId;
    private String mssnTitle;
    private String mssnEffect;
    private String mssnEtc;
    private String mssnTitleDtl;
    private String companyName;
    private String cateTitle;
    private String imgUrl;
    private Integer point;
    private Integer exp;
    private String useYn;
    private LocalDateTime startDt;
    private LocalDateTime endDt;
    private ParticptnTpCd particptnTpCd;
    private String rangeYn;
    private String linkUrl;
    private ActionPlaceTpCd placeCd;
    private ActionPurposeTpCd prpsCd;
    private String placeCdDesc;
    private String prpsCdDesc;
}
