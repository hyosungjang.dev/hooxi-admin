package com.hooxi.admin.biz.v1.story.service;

import com.hooxi.admin.biz.v1.certify.model.CertifyRes;
import com.hooxi.admin.biz.v1.reply.model.ReplyRes;
import com.hooxi.admin.biz.v1.story.model.*;
import com.hooxi.admin.biz.v1.user.model.UserListReq;
import com.hooxi.admin.biz.v1.user.model.UserListRes;
import com.hooxi.admin.config.type.ResultCodeConst;
import com.hooxi.admin.constants.code.FeedTpCd;
import com.hooxi.admin.constants.code.MmbrShpStatCd;
import com.hooxi.admin.constants.code.StoryStatCd;
import com.hooxi.admin.constants.code.StoryTpCd;
import com.hooxi.admin.db.entity.story.TbStoryFile;
import com.hooxi.admin.db.repository.mission.TbMssnRepository;
import com.hooxi.admin.db.repository.story.TbReplyRepository;
import com.hooxi.admin.db.repository.story.TbStoryFileRepository;
import com.hooxi.admin.db.repository.story.TbStoryRepository;
import com.hooxi.admin.util.Response;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
@RequiredArgsConstructor
public class StoryService {
    private final Logger log = LoggerFactory.getLogger(StoryService.class);

    private final MessageSourceAccessor messageSourceAccessor;
    private final TbStoryRepository tbStoryRepository;
    private final TbStoryFileRepository tbStoryFileRepository;
    private final TbReplyRepository tbReplyRepository;
    private final TbMssnRepository tbMssnRepository;

    public Response<PageImpl<StoryListRes>> getList(Pageable pageable, StoryListReq dto) {

        Response<PageImpl<StoryListRes>> res = new Response<>();

        try{
            PageImpl<StoryListRes> resList =
                    tbStoryRepository.selectStoryList(pageable, dto);

            resList.getContent().stream().forEach(e -> {
                if(e.getStoryStatCd() != null) e.setStoryStatCdDesc(e.getStoryStatCd().getDesc());
                if(e.getFeedTpCd() != null) e.setFeedTpCdDesc(e.getFeedTpCd().getDesc());
                if(e.getGradCd() != null) e.setGradCdDesc(e.getGradCd().getDesc());
            });

            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            res.setResult(resList);

            return res;
        }  catch (Exception e){
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }
    }

    public Response<HashMap<String, Object>> getCount(FeedTpCd feed) {
        Response<HashMap<String, Object>> res = new Response<>();
        HashMap<String, Object> map = new HashMap<>();

        try{
            Long all = tbStoryRepository.selectAllCount(StoryTpCd.STORY, feed);
            Long active = tbStoryRepository.selectStoryCount(StoryTpCd.STORY, feed, StoryStatCd.ACTIVE);
            Long waiting = tbStoryRepository.selectStoryCount(StoryTpCd.STORY, feed, StoryStatCd.WAITING);
            Long reject = tbStoryRepository.selectStoryCount(StoryTpCd.STORY, feed, StoryStatCd.REJECT);
            Long inActive = tbStoryRepository.selectStoryCount(StoryTpCd.STORY, feed, StoryStatCd.INACTIVE);

            map.put("all", all);
            map.put("active", active);
            map.put("waiting", waiting);
            map.put("reject", reject);
            map.put("inActive", inActive);

            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            res.setResult(map);

            return res;
        }  catch (Exception e){
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

    }

    public Response<StoryRes> getStory(Long storyId) {
        Response<StoryRes> res = new Response<>();
        List<TbStoryFile> files = new ArrayList<>();
        List<ReplyRes> replies = new ArrayList<>();

        try{
            StoryRes storyRes = tbStoryRepository.selectStory(storyId);
            if(storyRes.getActTlId() != null) {
                CertifyRes certifyRes = tbMssnRepository.selectCertify(storyRes.getActTlId());
                certifyRes.setActStatusCdDesc(certifyRes.getActStatusCd().getDesc());
                storyRes.setCertifyRes(certifyRes);
            }

            files = tbStoryFileRepository.findTbStoryFileByStoryId(storyId);
            replies = tbReplyRepository.selectReplies(storyId);
            replies.stream().forEach(e -> {
                e.setReplyCnt(tbReplyRepository.selectReReplyCount(e.getReplyId()));
            });

            storyRes.setFiles(files);
            storyRes.setReplies(replies);

            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            res.setResult(storyRes);

            return res;
        }  catch (Exception e){
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

    }

    public Response<Void> updateStoryCntn(StoryReq storyReq) {
        Response<Void> res = new Response<>();

        try{
            Long update = tbStoryRepository.updateStoryCntn(storyReq);

            res.setResultCd(update > 0 ? ResultCodeConst.SUCCESS.getCode() : ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));

            return res;
        }  catch (Exception e){
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

    }

    public Response<Void> updateStoryState(StoryReq storyReq) {
        Response<Void> res = new Response<>();

        try{
            Long update = tbStoryRepository.updateStoryState(storyReq);

            res.setResultCd(update > 0 ? ResultCodeConst.SUCCESS.getCode() : ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));

            return res;
        }  catch (Exception e){
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

    }

}
