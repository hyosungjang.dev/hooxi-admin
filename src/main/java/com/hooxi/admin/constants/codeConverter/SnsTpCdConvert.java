package com.hooxi.admin.constants.codeConverter;

import com.hooxi.admin.constants.code.SnsTpCd;
import com.hooxi.admin.util.AbstractLegacyEnumAttributeConverter;

import javax.persistence.Converter;

@Converter
public class SnsTpCdConvert extends AbstractLegacyEnumAttributeConverter<SnsTpCd> {
	public static final String ENUM_NAME = "SNS타입코드";

	public SnsTpCdConvert() {
		super(SnsTpCd.class,false, ENUM_NAME);
	}
}
