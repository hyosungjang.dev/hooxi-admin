package com.hooxi.admin.constants.codeConverter;

import com.hooxi.admin.constants.code.TmpltDtlTpCd;
import com.hooxi.admin.util.AbstractLegacyEnumAttributeConverter;

import javax.persistence.Converter;

@Converter
public class TmpltDtlTpCdConvert extends AbstractLegacyEnumAttributeConverter<TmpltDtlTpCd> {
	public static final String ENUM_NAME = "템플릿상세유형코드";

	public TmpltDtlTpCdConvert() {
		super(TmpltDtlTpCd.class,true, ENUM_NAME);
	}
}
