package com.hooxi.admin.constants.codeConverter;

import com.hooxi.admin.constants.code.EventTpCd;
import com.hooxi.admin.util.AbstractLegacyEnumAttributeConverter;

import javax.persistence.Converter;

@Converter
public class EventTpCdConvert extends AbstractLegacyEnumAttributeConverter<EventTpCd> {
	public static final String ENUM_NAME = "이벤트타입코드";

	public EventTpCdConvert() {
		super(EventTpCd.class,true, ENUM_NAME);
	}
}
