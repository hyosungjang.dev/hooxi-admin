package com.hooxi.admin.constants.codeConverter;

import com.hooxi.admin.constants.code.LoginStatusCd;
import com.hooxi.admin.util.AbstractLegacyEnumAttributeConverter;

import javax.persistence.Converter;

@Converter
public class LoginStatusCdConvert extends AbstractLegacyEnumAttributeConverter<LoginStatusCd> {
	public static final String ENUM_NAME = "로그인상태코드";

	public LoginStatusCdConvert() {
		super(LoginStatusCd.class,true, ENUM_NAME);
	}
}
