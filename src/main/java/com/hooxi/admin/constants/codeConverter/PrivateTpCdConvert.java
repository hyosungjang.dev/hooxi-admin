package com.hooxi.admin.constants.codeConverter;

import com.hooxi.admin.constants.code.PrivateTpCd;
import com.hooxi.admin.util.AbstractLegacyEnumAttributeConverter;

import javax.persistence.Converter;

@Converter
public class PrivateTpCdConvert extends AbstractLegacyEnumAttributeConverter<PrivateTpCd> {
	public static final String ENUM_NAME = "공개타입코드";

	public PrivateTpCdConvert() {
		super(PrivateTpCd.class,false, ENUM_NAME);
	}
}
