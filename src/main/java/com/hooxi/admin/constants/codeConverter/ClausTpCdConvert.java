package com.hooxi.admin.constants.codeConverter;

import com.hooxi.admin.constants.code.ClausTpCd;
import com.hooxi.admin.util.AbstractLegacyEnumAttributeConverter;

import javax.persistence.Converter;

@Converter
public class ClausTpCdConvert extends AbstractLegacyEnumAttributeConverter<ClausTpCd> {
	public static final String ENUM_NAME = "약관타입";

	public ClausTpCdConvert() {
		super(ClausTpCd.class,true, ENUM_NAME);
	}
}
