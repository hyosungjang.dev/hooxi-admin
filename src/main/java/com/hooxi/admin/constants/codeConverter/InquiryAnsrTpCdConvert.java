package com.hooxi.admin.constants.codeConverter;

import com.hooxi.admin.constants.code.InquiryAnsrTpCd;
import com.hooxi.admin.util.AbstractLegacyEnumAttributeConverter;

import javax.persistence.Converter;

@Converter
public class InquiryAnsrTpCdConvert extends AbstractLegacyEnumAttributeConverter<InquiryAnsrTpCd> {
	public static final String ENUM_NAME = "문의답변타입코드";

	public InquiryAnsrTpCdConvert() {
		super(InquiryAnsrTpCd.class,true, ENUM_NAME);
	}
}
