package com.hooxi.admin.constants.codeConverter;

import com.hooxi.admin.constants.code.SendTpCd;
import com.hooxi.admin.util.AbstractLegacyEnumAttributeConverter;

import javax.persistence.Converter;

@Converter
public class SendTpCdConvert extends AbstractLegacyEnumAttributeConverter<SendTpCd> {
	public static final String ENUM_NAME = "발송유형코드";

	public SendTpCdConvert() {
		super(SendTpCd.class,true, ENUM_NAME);
	}
}
