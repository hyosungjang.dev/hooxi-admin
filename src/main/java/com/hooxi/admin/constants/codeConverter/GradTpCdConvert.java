package com.hooxi.admin.constants.codeConverter;

import com.hooxi.admin.constants.code.GradTpCd;
import com.hooxi.admin.util.AbstractLegacyEnumAttributeConverter;

import javax.persistence.Converter;

@Converter
public class GradTpCdConvert extends AbstractLegacyEnumAttributeConverter<GradTpCd> {
	public static final String ENUM_NAME = "등급타입코드";

	public GradTpCdConvert() {
		super(GradTpCd.class,true, ENUM_NAME);
	}
}
