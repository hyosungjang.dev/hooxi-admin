package com.hooxi.admin.constants.codeConverter;

import com.hooxi.admin.constants.code.BannerTpCd;
import com.hooxi.admin.util.AbstractLegacyEnumAttributeConverter;

import javax.persistence.Converter;

@Converter
public class BannerTpCdConvert extends AbstractLegacyEnumAttributeConverter<BannerTpCd> {
	public static final String ENUM_NAME = "배너타입";

	public BannerTpCdConvert() {
		super(BannerTpCd.class,true, ENUM_NAME);
	}
}
