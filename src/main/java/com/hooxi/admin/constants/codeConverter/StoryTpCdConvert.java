package com.hooxi.admin.constants.codeConverter;

import com.hooxi.admin.constants.code.StoryTpCd;
import com.hooxi.admin.util.AbstractLegacyEnumAttributeConverter;

import javax.persistence.Converter;

@Converter
public class StoryTpCdConvert extends AbstractLegacyEnumAttributeConverter<StoryTpCd> {
	public static final String ENUM_NAME = "스토리유형코드";

	// null 허용
	public StoryTpCdConvert() {
		super(StoryTpCd.class,true, ENUM_NAME);
	}
}
