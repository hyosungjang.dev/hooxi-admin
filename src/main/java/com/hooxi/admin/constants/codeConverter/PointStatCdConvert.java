package com.hooxi.admin.constants.codeConverter;

import com.hooxi.admin.constants.code.PointStatCd;
import com.hooxi.admin.util.AbstractLegacyEnumAttributeConverter;

import javax.persistence.Converter;

@Converter
public class PointStatCdConvert extends AbstractLegacyEnumAttributeConverter<PointStatCd> {
	public static final String ENUM_NAME = "포인트상태코드";

	public PointStatCdConvert() {
		super(PointStatCd.class,false, ENUM_NAME);
	}
}
