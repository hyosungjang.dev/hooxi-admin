package com.hooxi.admin.constants.codeConverter;

import com.hooxi.admin.constants.code.DltTpCd;
import com.hooxi.admin.util.AbstractLegacyEnumAttributeConverter;

import javax.persistence.Converter;

@Converter
public class DltTpCdConvert extends AbstractLegacyEnumAttributeConverter<DltTpCd> {
	public static final String ENUM_NAME = "댓글삭제유형코드";

	public DltTpCdConvert() {
		super(DltTpCd.class,true, ENUM_NAME);
	}
}
