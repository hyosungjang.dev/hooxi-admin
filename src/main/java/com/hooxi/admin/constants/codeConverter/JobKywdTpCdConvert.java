package com.hooxi.admin.constants.codeConverter;

import com.hooxi.admin.constants.code.JobKywdTpCd;
import com.hooxi.admin.util.AbstractLegacyEnumAttributeConverter;

import javax.persistence.Converter;

@Converter
public class JobKywdTpCdConvert extends AbstractLegacyEnumAttributeConverter<JobKywdTpCd> {
	public static final String ENUM_NAME = "직종키워드타입코드";

	public JobKywdTpCdConvert() {
		super(JobKywdTpCd.class,true, ENUM_NAME);
	}
}
