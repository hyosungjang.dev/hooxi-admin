package com.hooxi.admin.constants.codeConverter;

import com.hooxi.admin.constants.code.TrstnDtlTpCd;
import com.hooxi.admin.util.AbstractLegacyEnumAttributeConverter;

import javax.persistence.Converter;

@Converter
public class TrstnDtlTpCdConvert extends AbstractLegacyEnumAttributeConverter<TrstnDtlTpCd> {
	public static final String ENUM_NAME = "거래상세유형코드";

	public TrstnDtlTpCdConvert() {
		super(TrstnDtlTpCd.class,true, ENUM_NAME);
	}
}
