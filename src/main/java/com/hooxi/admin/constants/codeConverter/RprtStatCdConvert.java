package com.hooxi.admin.constants.codeConverter;

import com.hooxi.admin.constants.code.RprtStatCd;
import com.hooxi.admin.util.AbstractLegacyEnumAttributeConverter;

import javax.persistence.Converter;

@Converter
public class RprtStatCdConvert extends AbstractLegacyEnumAttributeConverter<RprtStatCd> {
	public static final String ENUM_NAME = "신고상태코드";

	public RprtStatCdConvert() {
		super(RprtStatCd.class,false, ENUM_NAME);
	}
}
