package com.hooxi.admin.constants.codeConverter;

import com.hooxi.admin.util.AbstractLegacyEnumAttributeConverter;
import com.hooxi.admin.constants.code.ActionPlaceTpCd;

import javax.persistence.Converter;

@Converter
public class ActionPlaceTpCdConvert extends AbstractLegacyEnumAttributeConverter<ActionPlaceTpCd> {
	public static final String ENUM_NAME = "액션장소타입코드";

	public ActionPlaceTpCdConvert() {
		super(ActionPlaceTpCd.class,true, ENUM_NAME);
	}
}
