package com.hooxi.admin.constants.codeConverter;

import com.hooxi.admin.constants.code.TpcTpCd;
import com.hooxi.admin.util.AbstractLegacyEnumAttributeConverter;

import javax.persistence.Converter;

@Converter
public class TpcTpCdConvert extends AbstractLegacyEnumAttributeConverter<TpcTpCd> {
	public static final String ENUM_NAME = "토픽타입";

	public TpcTpCdConvert() {
		super(TpcTpCd.class,true, ENUM_NAME);
	}
}
