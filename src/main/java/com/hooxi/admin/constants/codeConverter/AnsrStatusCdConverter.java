package com.hooxi.admin.constants.codeConverter;

import com.hooxi.admin.constants.code.AnsrStatusCd;
import com.hooxi.admin.util.AbstractLegacyEnumAttributeConverter;

import javax.persistence.Converter;

@Converter
public class AnsrStatusCdConverter extends AbstractLegacyEnumAttributeConverter<AnsrStatusCd> {
	public static final String ENUM_NAME = "문의답변상태코드";

	public AnsrStatusCdConverter() {
		super(AnsrStatusCd.class,true, ENUM_NAME);
	}
}
