package com.hooxi.admin.constants.codeConverter;

import com.hooxi.admin.constants.code.FeedTpCd;
import com.hooxi.admin.util.AbstractLegacyEnumAttributeConverter;

import javax.persistence.Converter;

@Converter
public class FeedTpCdConvert extends AbstractLegacyEnumAttributeConverter<FeedTpCd> {
	public static final String ENUM_NAME = "피드타입코드";

	public FeedTpCdConvert() {
		super(FeedTpCd.class,false, ENUM_NAME);
	}
}
