package com.hooxi.admin.constants.codeConverter;

import com.hooxi.admin.constants.code.PopupTpCd;
import com.hooxi.admin.util.AbstractLegacyEnumAttributeConverter;

import javax.persistence.Converter;

@Converter
public class PopupTpCdConvert extends AbstractLegacyEnumAttributeConverter<PopupTpCd> {
	public static final String ENUM_NAME = "팝업타입";

	public PopupTpCdConvert() {
		super(PopupTpCd.class,true, ENUM_NAME);
	}
}
