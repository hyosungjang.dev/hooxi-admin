package com.hooxi.admin.constants.codeConverter;

import com.hooxi.admin.constants.code.ScreenTpCd;
import com.hooxi.admin.util.AbstractLegacyEnumAttributeConverter;

import javax.persistence.Converter;

@Converter
public class ScreenTpCdConvert extends AbstractLegacyEnumAttributeConverter<ScreenTpCd> {
	public static final String ENUM_NAME = "화면타입";

	public ScreenTpCdConvert() {
		super(ScreenTpCd.class,true, ENUM_NAME);
	}
}
