package com.hooxi.admin.constants.codeConverter;

import com.hooxi.admin.constants.code.PopupCookieTpCd;
import com.hooxi.admin.util.AbstractLegacyEnumAttributeConverter;

import javax.persistence.Converter;

@Converter
public class PopupCookieTpCdConvert extends AbstractLegacyEnumAttributeConverter<PopupCookieTpCd> {
	public static final String ENUM_NAME = "팝업쿠키유형";

	public PopupCookieTpCdConvert() {
		super(PopupCookieTpCd.class,true, ENUM_NAME);
	}
}
