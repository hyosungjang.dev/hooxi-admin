package com.hooxi.admin.constants.codeConverter;

import com.hooxi.admin.constants.code.ParticptnTpCd;
import com.hooxi.admin.util.AbstractLegacyEnumAttributeConverter;

import javax.persistence.Converter;

@Converter
public class ParticptnTpCdConvert extends AbstractLegacyEnumAttributeConverter<ParticptnTpCd> {
	public static final String ENUM_NAME = "챌린지 참여 타입";

	public ParticptnTpCdConvert() {
		super(ParticptnTpCd.class,true, ENUM_NAME);
	}
}
