package com.hooxi.admin.constants.codeConverter;

import com.hooxi.admin.constants.code.MemberStatusCd;
import com.hooxi.admin.util.AbstractLegacyEnumAttributeConverter;

import javax.persistence.Converter;

@Converter
public class MemberStatusCdConvert extends AbstractLegacyEnumAttributeConverter<MemberStatusCd> {
	public static final String ENUM_NAME = "유저활동상태";

	public MemberStatusCdConvert() {
		super(MemberStatusCd.class,true, ENUM_NAME);
	}
}
