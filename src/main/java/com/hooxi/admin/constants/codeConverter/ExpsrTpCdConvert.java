package com.hooxi.admin.constants.codeConverter;

import com.hooxi.admin.constants.code.ExpsrTpCd;
import com.hooxi.admin.util.AbstractLegacyEnumAttributeConverter;

import javax.persistence.Converter;

@Converter
public class ExpsrTpCdConvert extends AbstractLegacyEnumAttributeConverter<ExpsrTpCd> {
	public static final String ENUM_NAME = "노출타입";

	public ExpsrTpCdConvert() {
		super(ExpsrTpCd.class,true, ENUM_NAME);
	}
}
