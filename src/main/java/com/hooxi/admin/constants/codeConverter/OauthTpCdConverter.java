package com.hooxi.admin.constants.codeConverter;

import com.hooxi.admin.constants.code.OauthTpCd;
import com.hooxi.admin.util.AbstractLegacyEnumAttributeConverter;

import javax.persistence.Converter;

@Converter
public class OauthTpCdConverter extends AbstractLegacyEnumAttributeConverter<OauthTpCd> {
	public static final String ENUM_NAME = "인증타입";

	public OauthTpCdConverter() {
		super(OauthTpCd.class,true, ENUM_NAME);
	}
}
