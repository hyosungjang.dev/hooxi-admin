package com.hooxi.admin.constants.codeConverter;

import com.hooxi.admin.constants.code.MmbrShpStatCd;
import com.hooxi.admin.util.AbstractLegacyEnumAttributeConverter;

import javax.persistence.Converter;

@Converter
public class MmbrShpStatCdConvert extends AbstractLegacyEnumAttributeConverter<MmbrShpStatCd> {
	public static final String ENUM_NAME = "멤버쉽 신청 상태";

	public MmbrShpStatCdConvert() {
		super(MmbrShpStatCd.class,true, ENUM_NAME);
	}
}
