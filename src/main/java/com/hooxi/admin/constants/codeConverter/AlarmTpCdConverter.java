package com.hooxi.admin.constants.codeConverter;

import com.hooxi.admin.constants.code.AlarmTpCd;
import com.hooxi.admin.util.AbstractLegacyEnumAttributeConverter;

import javax.persistence.Converter;

@Converter
public class AlarmTpCdConverter extends AbstractLegacyEnumAttributeConverter<AlarmTpCd> {
	public static final String ENUM_NAME = "알람타입코드";

	public AlarmTpCdConverter() {
		super(AlarmTpCd.class,true, ENUM_NAME);
	}
}
