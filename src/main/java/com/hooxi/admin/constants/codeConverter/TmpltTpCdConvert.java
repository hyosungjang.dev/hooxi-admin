package com.hooxi.admin.constants.codeConverter;

import com.hooxi.admin.constants.code.TmpltTpCd;
import com.hooxi.admin.util.AbstractLegacyEnumAttributeConverter;

import javax.persistence.Converter;

@Converter
public class TmpltTpCdConvert extends AbstractLegacyEnumAttributeConverter<TmpltTpCd> {
	public static final String ENUM_NAME = "템플릿유형코드";

	public TmpltTpCdConvert() {
		super(TmpltTpCd.class,true, ENUM_NAME);
	}
}
