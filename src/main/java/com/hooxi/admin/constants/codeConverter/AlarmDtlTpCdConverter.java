package com.hooxi.admin.constants.codeConverter;

import com.hooxi.admin.constants.code.AlarmDtlTpCd;
import com.hooxi.admin.util.AbstractLegacyEnumAttributeConverter;

import javax.persistence.Converter;

@Converter
public class AlarmDtlTpCdConverter extends AbstractLegacyEnumAttributeConverter<AlarmDtlTpCd> {
	public static final String ENUM_NAME = "알람상세타입코드";

	public AlarmDtlTpCdConverter() {
		super(AlarmDtlTpCd.class,true, ENUM_NAME);
	}
}
