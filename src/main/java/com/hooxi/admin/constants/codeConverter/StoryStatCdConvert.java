package com.hooxi.admin.constants.codeConverter;

import com.hooxi.admin.constants.code.StoryStatCd;
import com.hooxi.admin.util.AbstractLegacyEnumAttributeConverter;

import javax.persistence.Converter;

@Converter
public class StoryStatCdConvert extends AbstractLegacyEnumAttributeConverter<StoryStatCd> {
	public static final String ENUM_NAME = "스토리상태코드";

	public StoryStatCdConvert() {
		super(StoryStatCd.class,false, ENUM_NAME);
	}
}
