package com.hooxi.admin.constants.codeConverter;

import com.hooxi.admin.constants.code.AlarmDivCd;
import com.hooxi.admin.util.AbstractLegacyEnumAttributeConverter;

import javax.persistence.Converter;

@Converter
public class AlarmDivCdConverter extends AbstractLegacyEnumAttributeConverter<AlarmDivCd> {
	public static final String ENUM_NAME = "알람구분코드";

	public AlarmDivCdConverter() {
		super(AlarmDivCd.class,true, ENUM_NAME);
	}
}
