package com.hooxi.admin.constants.codeConverter;

import com.hooxi.admin.util.AbstractLegacyEnumAttributeConverter;
import com.hooxi.admin.constants.code.LangTpCd;

import javax.persistence.Converter;

@Converter
public class LangTpCdConvert extends AbstractLegacyEnumAttributeConverter<LangTpCd> {
	public static final String ENUM_NAME = "언어코드";

	public LangTpCdConvert() {
		super(LangTpCd.class,true, ENUM_NAME);
	}
}
