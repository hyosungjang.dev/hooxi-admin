package com.hooxi.admin.constants.codeConverter;

import com.hooxi.admin.constants.code.UrlLnkngTpCd;
import com.hooxi.admin.util.AbstractLegacyEnumAttributeConverter;

import javax.persistence.Converter;

@Converter
public class UrlLnkngTpCdConvert extends AbstractLegacyEnumAttributeConverter<UrlLnkngTpCd> {
	public static final String ENUM_NAME = "URL연경유형";

	public UrlLnkngTpCdConvert() {
		super(UrlLnkngTpCd.class,true, ENUM_NAME);
	}
}
