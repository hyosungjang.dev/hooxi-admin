package com.hooxi.admin.constants.codeConverter;

import com.hooxi.admin.constants.code.ExpStateCd;
import com.hooxi.admin.util.AbstractLegacyEnumAttributeConverter;

import javax.persistence.Converter;

@Converter
public class ExpStateCdConvert extends AbstractLegacyEnumAttributeConverter<ExpStateCd> {
	public static final String ENUM_NAME = "경험치상태코드";

	public ExpStateCdConvert() {
		super(ExpStateCd.class,true, ENUM_NAME);
	}
}
