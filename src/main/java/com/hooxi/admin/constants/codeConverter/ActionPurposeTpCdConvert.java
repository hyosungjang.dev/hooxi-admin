package com.hooxi.admin.constants.codeConverter;

import com.hooxi.admin.util.AbstractLegacyEnumAttributeConverter;
import com.hooxi.admin.constants.code.ActionPurposeTpCd;

import javax.persistence.Converter;

@Converter
public class ActionPurposeTpCdConvert extends AbstractLegacyEnumAttributeConverter<ActionPurposeTpCd> {
	public static final String ENUM_NAME = "액션부문타입코드";

	public ActionPurposeTpCdConvert() {
		super(ActionPurposeTpCd.class,true, ENUM_NAME);
	}
}
