package com.hooxi.admin.constants.codeConverter;

import com.hooxi.admin.constants.code.RewardTpCd;
import com.hooxi.admin.util.AbstractLegacyEnumAttributeConverter;

import javax.persistence.Converter;

@Converter
public class RewardTpCdConvert extends AbstractLegacyEnumAttributeConverter<RewardTpCd> {
	public static final String ENUM_NAME = "보상유형코드";

	public RewardTpCdConvert() {
		super(RewardTpCd.class,true, ENUM_NAME);
	}
}
