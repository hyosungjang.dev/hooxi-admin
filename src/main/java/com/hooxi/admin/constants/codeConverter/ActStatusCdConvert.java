package com.hooxi.admin.constants.codeConverter;

import com.hooxi.admin.constants.code.ActStatusCd;
import com.hooxi.admin.util.AbstractLegacyEnumAttributeConverter;

import javax.persistence.Converter;

@Converter
public class ActStatusCdConvert extends AbstractLegacyEnumAttributeConverter<ActStatusCd> {
	public static final String ENUM_NAME = "액션상태코드";

	public ActStatusCdConvert() {
		super(ActStatusCd.class,true, ENUM_NAME);
	}
}
