package com.hooxi.admin.constants.codeConverter;

import com.hooxi.admin.constants.code.RprtTpCd;
import com.hooxi.admin.util.AbstractLegacyEnumAttributeConverter;

import javax.persistence.Converter;

@Converter
public class RprtTpCdConvert extends AbstractLegacyEnumAttributeConverter<RprtTpCd> {
	public static final String ENUM_NAME = "신고타입코드";

	public RprtTpCdConvert() {
		super(RprtTpCd.class,false, ENUM_NAME);
	}
}
