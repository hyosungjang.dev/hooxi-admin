package com.hooxi.admin.constants.code;

import com.hooxi.admin.util.LegacyCommonType;
import com.hooxi.admin.exception.common.CommonCodeException;
import lombok.Getter;

@Getter
public enum TmpltTpCd implements LegacyCommonType {
    REPORT("신고","TPL001"),
    STORY("스토리","TPL002"),
    ACTION("행동","TPL003"),
    CHALLENGE("챌린지","TPL004"),
    TARGET("타겟","TPL005"),
    POINT("포인트","TPL006"),
    EXP("경험치","TPL007"),
    SUBSCRIBE("구독","TPL008")
    ;

    private String desc;
    private String legacyCode;

    TmpltTpCd(String desc
            , String legacyCode) {
        this.desc = desc;
        this.legacyCode = legacyCode;
    }

    public static TmpltTpCd lookup(String code) {
        if( code == null ) {
            throw new CommonCodeException("Code Is Null");
        }
        for (TmpltTpCd element : TmpltTpCd.values()) {
            if (element.legacyCode.equalsIgnoreCase(code)) {
                return element;
            }
        }
        return null;
    }

}
