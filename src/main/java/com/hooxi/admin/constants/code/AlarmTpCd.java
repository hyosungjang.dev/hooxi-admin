package com.hooxi.admin.constants.code;

import com.hooxi.admin.util.LegacyCommonType;
import com.hooxi.admin.exception.common.CommonCodeException;
import lombok.Getter;

@Getter
public enum AlarmTpCd implements LegacyCommonType {

    //INFO
    HOOXI("후시플래닛","ART001"),
    SUBSCRIBE("구독","ART002"),
    STORY("스토리","ART003"),
    ACTION("행동","ART004"),
    CHALLENGE("챌린지","ART005"),

    //REWARD
    POINT("포인트","ART006"),
    EXP("경험치","ART007"),
    ;

    private String desc;
    private String legacyCode;

    AlarmTpCd(String desc
            , String legacyCode) {
        this.desc = desc;
        this.legacyCode = legacyCode;
    }

    public static AlarmTpCd lookup(String code) {
        if( code == null ) {
            throw new CommonCodeException("Code Is Null");
        }
        for (AlarmTpCd element : AlarmTpCd.values()) {
            if (element.legacyCode.equalsIgnoreCase(code)) {
                return element;
            }
        }
        return null;
    }

}
