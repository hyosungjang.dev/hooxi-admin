package com.hooxi.admin.constants.code;

import com.hooxi.admin.util.LegacyCommonType;
import com.hooxi.admin.exception.common.CommonCodeException;
import lombok.Getter;

@Getter
public enum TmpltDtlTpCd implements LegacyCommonType {
    FEED("피드","TPD001"),
    REPLY("댓글","TPD002"),
    RE_REPLY("답글","TPD003"),
    UPDATE("업데이트","TPD004"),
    REMIND("리마인드","TPD005"),
    EVENT("이벤트","TPD006"),
    LEVEL_UP("레벨업","TPD007"),
    LEVEL_UP_WGP("레벨업WGP","TPD008");

    private String desc;
    private String legacyCode;

    TmpltDtlTpCd(String desc
            , String legacyCode) {
        this.desc = desc;
        this.legacyCode = legacyCode;
    }

    public static TmpltDtlTpCd lookup(String code) {
        if( code == null ) {
            throw new CommonCodeException("Code Is Null");
        }
        for (TmpltDtlTpCd element : TmpltDtlTpCd.values()) {
            if (element.legacyCode.equalsIgnoreCase(code)) {
                return element;
            }
        }
        return null;
    }

}
