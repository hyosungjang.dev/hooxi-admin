package com.hooxi.admin.constants.code;

import com.hooxi.admin.util.LegacyCommonType;
import com.hooxi.admin.exception.common.CommonCodeException;
import lombok.Getter;

@Getter
public enum AnsrStatusCd implements LegacyCommonType {
    REGISTER("접수","ASC001"),
    ANSWER("답변완료","ASC002");

    private String desc;
    private String legacyCode;

    AnsrStatusCd(String desc
            , String legacyCode) {
        this.desc = desc;
        this.legacyCode = legacyCode;
    }

    public static AnsrStatusCd lookup(String code) {
        if( code == null ) {
            throw new CommonCodeException("Code Is Null");
        }
        for (AnsrStatusCd element : AnsrStatusCd.values()) {
            if (element.legacyCode.equalsIgnoreCase(code)) {
                return element;
            }
        }
        return null;
    }

}
