package com.hooxi.admin.constants.code;

import com.hooxi.admin.util.LegacyCommonType;
import com.hooxi.admin.exception.common.CommonCodeException;
import lombok.Getter;

@Getter
public enum RewardTpCd implements LegacyCommonType {
    CONTINUE("지속","RDT001"),
    TIME_PERSON("1인1회","RDT002"),
    TIME_DAY("1일1회","RDT003");

    private String desc;
    private String legacyCode;

    RewardTpCd(String desc
            , String legacyCode) {
        this.desc = desc;
        this.legacyCode = legacyCode;
    }

    public static RewardTpCd lookup(String code) {
        if( code == null ) {
            throw new CommonCodeException("Code Is Null");
        }
        for (RewardTpCd element : RewardTpCd.values()) {
            if (element.legacyCode.equalsIgnoreCase(code)) {
                return element;
            }
        }
        return null;
    }
}
