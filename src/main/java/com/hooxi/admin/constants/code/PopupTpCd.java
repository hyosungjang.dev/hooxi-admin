package com.hooxi.admin.constants.code;

import com.hooxi.admin.util.LegacyCommonType;
import com.hooxi.admin.exception.common.CommonCodeException;
import lombok.Getter;

@Getter
public enum PopupTpCd implements LegacyCommonType {
    CLAUS("약관","PUT001"),
    POPUP("팝업","PUT002"),
    EVENT("이벤트","PUT003");

    private String desc;
    private String legacyCode;

    PopupTpCd(String desc
            , String legacyCode) {
        this.desc = desc;
        this.legacyCode = legacyCode;
    }

    public static PopupTpCd lookup(String code) {
        if( code == null ) {
            throw new CommonCodeException("Code Is Null");
        }
        for (PopupTpCd element : PopupTpCd.values()) {
            if (element.legacyCode.equalsIgnoreCase(code)) {
                return element;
            }
        }
        return null;
    }

}
