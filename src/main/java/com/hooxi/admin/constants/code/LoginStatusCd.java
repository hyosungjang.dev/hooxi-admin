package com.hooxi.admin.constants.code;

import com.hooxi.admin.util.LegacyCommonType;
import com.hooxi.admin.exception.common.CommonCodeException;
import lombok.Getter;

@Getter
public enum LoginStatusCd implements LegacyCommonType {
    LOGIN("LOGIN","LSC001"),
    LOGOUT("LOGOUT","LSC002"),
    LOGIN_FAIL("LOGIN_FAIL","LSC003"),
    PASSWORD_FAIL("PASSWORD_FAIL","LSC004");

    private String desc;
    private String legacyCode;

    LoginStatusCd(String desc
            , String legacyCode) {
        this.desc = desc;
        this.legacyCode = legacyCode;
    }

    public static LoginStatusCd lookup(String code) {
        if( code == null ) {
            throw new CommonCodeException("Code Is Null");
        }
        for (LoginStatusCd element : LoginStatusCd.values()) {
            if (element.legacyCode.equalsIgnoreCase(code)) {
                return element;
            }
        }
        return null;
    }

}
