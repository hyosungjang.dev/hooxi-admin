package com.hooxi.admin.constants.code;

import com.hooxi.admin.util.LegacyCommonType;
import lombok.Getter;

@Getter
public enum DltTpCd implements LegacyCommonType {
    USER("유저삭제","DLT001"),
    REPORT("신고삭제","DLT002"),
    ETC("기타삭제","DLT003");

    private String desc;
    private String legacyCode;

    DltTpCd(String desc
            , String legacyCode) {
        this.desc = desc;
        this.legacyCode = legacyCode;
    }

}
