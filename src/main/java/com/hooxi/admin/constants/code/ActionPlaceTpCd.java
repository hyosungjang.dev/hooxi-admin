package com.hooxi.admin.constants.code;

import com.hooxi.admin.util.LegacyCommonType;
import com.hooxi.admin.exception.common.CommonCodeException;
import lombok.Getter;

@Getter
public enum ActionPlaceTpCd implements LegacyCommonType {
    FAMILY("가정","PLT001"),
    COMPANY("회사","PLT002"),
    SCHOOL("학교","PLT003"),
    PUBLIC("야외","PLT004"),
    EVENT("이벤트","PLT005"),
    OFFICIAL_EVENT("공식이벤트","PLT006");

    private String desc;
    private String legacyCode;

    ActionPlaceTpCd(String desc
            , String legacyCode) {
        this.desc = desc;
        this.legacyCode = legacyCode;
    }

    public static ActionPlaceTpCd lookup(String code) {
        if( code == null ) {
            throw new CommonCodeException("Code Is Null");
        }
        for (ActionPlaceTpCd element : ActionPlaceTpCd.values()) {
            if (element.legacyCode.equalsIgnoreCase(code)) {
                return element;
            }
        }
        return null;
    }

}
