package com.hooxi.admin.constants.code;

import com.hooxi.admin.util.LegacyCommonType;
import com.hooxi.admin.exception.common.CommonCodeException;
import lombok.Getter;

@Getter
public enum ParticptnTpCd implements LegacyCommonType {
    MULTI("MULTI","PTC001"),
    ONCE("ONCE","PTC002");

    private String desc;
    private String legacyCode;

    ParticptnTpCd(String desc
            , String legacyCode) {
        this.desc = desc;
        this.legacyCode = legacyCode;
    }

    public static ParticptnTpCd lookup(String code) {
        if( code == null ) {
            throw new CommonCodeException("Code Is Null");
        }
        for (ParticptnTpCd element : ParticptnTpCd.values()) {
            if (element.legacyCode.equalsIgnoreCase(code)) {
                return element;
            }
        }
        return null;
    }

}
