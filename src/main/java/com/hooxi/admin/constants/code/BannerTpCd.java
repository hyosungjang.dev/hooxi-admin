package com.hooxi.admin.constants.code;

import com.hooxi.admin.util.LegacyCommonType;
import com.hooxi.admin.exception.common.CommonCodeException;
import lombok.Getter;

@Getter
public enum BannerTpCd implements LegacyCommonType {
    SINGLE("단일","BNT001"),
    ROLLING("롤링","BNT002"),
    RANDOM("랜덤","BNT003");

    private String desc;
    private String legacyCode;

    BannerTpCd(String desc
            , String legacyCode) {
        this.desc = desc;
        this.legacyCode = legacyCode;
    }

    public static BannerTpCd lookup(String code) {
        if( code == null ) {
            throw new CommonCodeException("Code Is Null");
        }
        for (BannerTpCd element : BannerTpCd.values()) {
            if (element.legacyCode.equalsIgnoreCase(code)) {
                return element;
            }
        }
        return null;
    }

}
