package com.hooxi.admin.constants.code;

import com.hooxi.admin.util.LegacyCommonType;
import com.hooxi.admin.exception.common.CommonCodeException;
import lombok.Getter;

import java.util.LinkedList;
import java.util.List;

@Getter
public enum PointStatCd implements LegacyCommonType {
    ACCUMULATE("적립","PIS001"),
    USE("사용","PIS002"),
    CANCEL("취소","PIS003"),
    USE_CANCEL("사용취소","PIS004"),
    COLLECT("회수","PIS005"),
    EXCHANGE("교환","PIS006"),
    EXPIRATION("만료","PIS007"),
    WAIT_ACCUMULATE("적립 대기중","PIS100"),
    WAIT_USE("사용 대기중","PIS200"),
    WAIT_CANCEL("취소 대기중", "PIS300")
    ;


    private String desc;
    private String legacyCode;

    PointStatCd(String desc
            , String legacyCode) {
        this.desc = desc;
        this.legacyCode = legacyCode;
    }

    public static PointStatCd lookup(String code) {
        if( code == null ) {
            throw new CommonCodeException("Code Is Null");
        }
        for (PointStatCd element : PointStatCd.values()) {
            if (element.legacyCode.equalsIgnoreCase(code)) {
                return element;
            }
        }
        return null;
    }

    public static String checkPointState(String code) {
        if( code == null ) {
            throw new CommonCodeException("Code Is Null");
        }
        for (PointStatCd element : PointStatCd.values()) {
            if (element.legacyCode.equalsIgnoreCase(code)) {
                if (element.name().startsWith("WAIT_")) return "승인 대기중";
            }
        }
        return "승인 완료";
    }


    public static List<PointStatCd> getWaitState() {
        List<PointStatCd> list = new LinkedList<>();
        for (PointStatCd element : PointStatCd.values()) {
            if (element.name().startsWith("WAIT_")) {
                list.add(element);
            }
        }
        return list;
    }

    public static List<PointStatCd> getNotInWaitState() {
        List<PointStatCd> list = new LinkedList<>();
        for (PointStatCd element : PointStatCd.values()) {
            if (!element.name().startsWith("WAIT_")) {
                list.add(element);
            }
        }
        return list;
    }
}
