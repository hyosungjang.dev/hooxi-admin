package com.hooxi.admin.constants.code;

import com.hooxi.admin.util.LegacyCommonType;
import lombok.Getter;

@Getter
public enum JobKywdTpCd implements LegacyCommonType {
    NORMAL("일반","JKT001"),
    CREATOR("크리에이터","JKT002"),
    EXPERT("전문가","JKT003"),
    INFLUENCER("인플루언서","JKT004"),
    CORPORATION("기업","JKT005"),
    BRAND("브랜드","JKT006"),
    GROUP("단체","JKT007"),
    PUBLIC("공공기관","JKT008"),
    SOCIETY("사회활동가","JKT009"),
    ETC("기타","JKT010");

    private String desc;
    private String legacyCode;

    JobKywdTpCd(String desc
            , String legacyCode) {
        this.desc = desc;
        this.legacyCode = legacyCode;
    }

    public static JobKywdTpCd lookup(String code) {
//        if( code == null ) {
//            throw new CommonCodeException("Code Is Null");
//        }
        for (JobKywdTpCd element : JobKywdTpCd.values()) {
            if (element.legacyCode.equalsIgnoreCase(code)) {
                return element;
            }
        }
        return null;
    }

}
