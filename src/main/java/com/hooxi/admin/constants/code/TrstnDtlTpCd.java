package com.hooxi.admin.constants.code;

import com.hooxi.admin.util.LegacyCommonType;
import com.hooxi.admin.exception.common.CommonCodeException;
import lombok.Getter;

@Getter
public enum TrstnDtlTpCd implements LegacyCommonType {


    // 공통
    COMMON_COMMENT("댓글","TDT001"),
    COMMON_LIKE("좋아요","TDT002"),
    COMMON_VIEWS("뷰","TDT003"),
    COMMON_SHARE("공유하기","TDT004"),

    // 스토리
    STORY_CREATE("스토리 작성","TDT100"),
    STORY_STICKER("스토리 스티커","TDT101"),
    STORY_SUBSCRIBE("멤버 구독하기","TDT102"),


    // 액션
    ACTION_PROMISE("액션 서약","TDT200"),
    ACTION_REVIEW_TEXT("텍스트형 인증","TDT201"),
    ACTION_REVIEW_PHOTO("사진형 인증","TDT202"),
    ACTION_ADD_LIST("추가","TDT203"),

    // 챌린지
    CHALLENGE("챌린지","TDT300"),


    // 이벤트
    EVENT_JOIN("회원가입","TDT800"),
    EVENT_DARK_MODE("다크모드","TDT801"),
    EVENT_LEVEL_UP("레벨업","TDT802"),

    // 후시몰
    HOOXIMALL("후시몰","TDT900"),
    EMPTY("없음","TDT000");

    private String desc;
    private String legacyCode;

    TrstnDtlTpCd(String desc
            , String legacyCode) {
        this.desc = desc;
        this.legacyCode = legacyCode;
    }

    public static TrstnDtlTpCd lookup(String code) {
        if( code == null ) {
            throw new CommonCodeException("Code Is Null");
        }
        for (TrstnDtlTpCd element : TrstnDtlTpCd.values()) {
            if (element.legacyCode.equalsIgnoreCase(code)) {
                return element;
            }
        }
        return null;
    }
}
