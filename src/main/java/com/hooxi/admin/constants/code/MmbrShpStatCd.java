package com.hooxi.admin.constants.code;

import com.hooxi.admin.util.LegacyCommonType;
import lombok.Getter;

@Getter
public enum MmbrShpStatCd implements LegacyCommonType {
    ALL("전체", "ALL"),
    NORMAL("승인","MSS001"),
    WAITING("대기","MSS002"),
    REJECT("반려","MSS003"),
    STOP("정지","MSS004");

    private String desc;
    private String legacyCode;

    MmbrShpStatCd(String desc
            , String legacyCode) {
        this.desc = desc;
        this.legacyCode = legacyCode;
    }

    public static MmbrShpStatCd lookup(String code) {
//        if( code == null ) {
//            throw new CommonCodeException("Code Is Null");
//        }
        for (MmbrShpStatCd element : MmbrShpStatCd.values()) {
            if (element.legacyCode.equalsIgnoreCase(code)) {
                return element;
            }
        }
        return null;
    }

}
