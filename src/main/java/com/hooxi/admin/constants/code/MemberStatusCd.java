package com.hooxi.admin.constants.code;

import com.hooxi.admin.util.LegacyCommonType;
import com.hooxi.admin.exception.common.CommonCodeException;
import lombok.Getter;

@Getter
public enum MemberStatusCd implements LegacyCommonType {
    ACTIVE("ACTIVE","MRS001"),
    DROP("DROP","MRS002"),
    STOP("STOP","MRS003"),
    DORMANT("DORMANT","MRS004");

    private String desc;
    private String legacyCode;

    MemberStatusCd(String desc
            , String legacyCode) {
        this.desc = desc;
        this.legacyCode = legacyCode;
    }

    public static MemberStatusCd lookup(String code) {
        if( code == null ) {
            throw new CommonCodeException("Code Is Null");
        }
        for (MemberStatusCd element : MemberStatusCd.values()) {
            if (element.legacyCode.equalsIgnoreCase(code)) {
                return element;
            }
        }
        return null;
    }

}
