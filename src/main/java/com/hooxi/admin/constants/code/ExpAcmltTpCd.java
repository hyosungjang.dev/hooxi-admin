package com.hooxi.admin.constants.code;

import com.hooxi.admin.util.LegacyCommonType;
import lombok.Getter;

@Getter
public enum ExpAcmltTpCd implements LegacyCommonType {
    STORY_CREATE("스토리 작성","EAT001"),
    ACTION("액션 수행","EAT002"),
    COMMUNITY("커뮤니티 글 작성","EAT003"),
    CHALLENGE("챌린지 수행","EAT004"),
    EVENT("이벤트","EAT005"),
    ACTION_PROMISE("액션 서약","EAT006"),

    JOIN_MMBRID("회원가입","EAT500");

    private String desc;
    private String legacyCode;

    ExpAcmltTpCd(String desc
            , String legacyCode) {
        this.desc = desc;
        this.legacyCode = legacyCode;
    }

}
