package com.hooxi.admin.constants.code;

import com.hooxi.admin.util.LegacyCommonType;
import com.hooxi.admin.exception.common.CommonCodeException;
import lombok.Getter;

@Getter
public enum ExpsrTpCd implements LegacyCommonType {
    MODAL("모달","EXT001"),
    SCREEN("스크린","EXT002"),
    BOTTOM("하단","EXT003");

    private String desc;
    private String legacyCode;

    ExpsrTpCd(String desc
            , String legacyCode) {
        this.desc = desc;
        this.legacyCode = legacyCode;
    }

    public static ExpsrTpCd lookup(String code) {
        if( code == null ) {
            throw new CommonCodeException("Code Is Null");
        }
        for (ExpsrTpCd element : ExpsrTpCd.values()) {
            if (element.legacyCode.equalsIgnoreCase(code)) {
                return element;
            }
        }
        return null;
    }

}
