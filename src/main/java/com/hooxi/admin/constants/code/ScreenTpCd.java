package com.hooxi.admin.constants.code;

import com.hooxi.admin.util.LegacyCommonType;
import com.hooxi.admin.exception.common.CommonCodeException;
import lombok.Getter;

@Getter
public enum ScreenTpCd implements LegacyCommonType {
    JOIN("회원가입 완료","SCT001"),
    STORY("스토리","SCT002"),
    STORY_DTL("스토리 상세","SCT003"),
    ACTION("액션","SCT004"),
    CHALLENGE("챌린지","SCT005"),
    MORE("더보기","SCT006");

    private String desc;
    private String legacyCode;

    ScreenTpCd(String desc
            , String legacyCode) {
        this.desc = desc;
        this.legacyCode = legacyCode;
    }

    public static ScreenTpCd lookup(String code) {
        if( code == null ) {
            throw new CommonCodeException("Code Is Null");
        }
        for (ScreenTpCd element : ScreenTpCd.values()) {
            if (element.legacyCode.equalsIgnoreCase(code)) {
                return element;
            }
        }
        return null;
    }

}
