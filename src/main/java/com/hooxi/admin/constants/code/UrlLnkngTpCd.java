package com.hooxi.admin.constants.code;

import com.hooxi.admin.util.LegacyCommonType;
import com.hooxi.admin.exception.common.CommonCodeException;
import lombok.Getter;

@Getter
public enum UrlLnkngTpCd implements LegacyCommonType {
    NEW_WINDOW("새창","ULT001"),
    PARENT_WINDOW("부모창","ULT002"),
    API_GET("api get","ULT003"),
    API_POST("api post","ULT004");

    private String desc;
    private String legacyCode;

    UrlLnkngTpCd(String desc
            , String legacyCode) {
        this.desc = desc;
        this.legacyCode = legacyCode;
    }

    public static UrlLnkngTpCd lookup(String code) {
        if( code == null ) {
            throw new CommonCodeException("Code Is Null");
        }
        for (UrlLnkngTpCd element : UrlLnkngTpCd.values()) {
            if (element.legacyCode.equalsIgnoreCase(code)) {
                return element;
            }
        }
        return null;
    }

}
