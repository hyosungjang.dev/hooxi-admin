package com.hooxi.admin.constants.code;

import com.hooxi.admin.util.LegacyCommonType;
import com.hooxi.admin.exception.common.CommonCodeException;
import lombok.Getter;

@Getter
public enum ActStatusCd implements LegacyCommonType {
    IN("진행중","ACS001"),
    OUT("삭제","ACS002"),
    SUCCESS("성공","ACS003"),
    FAIL("실패","ACS004");

    private String desc;
    private String legacyCode;

    ActStatusCd(String desc
            , String legacyCode) {
        this.desc = desc;
        this.legacyCode = legacyCode;
    }

    public static ActStatusCd lookup(String code) {
        if( code == null ) {
            throw new CommonCodeException("Code Is Null");
        }
        for (ActStatusCd element : ActStatusCd.values()) {
            if (element.legacyCode.equalsIgnoreCase(code)) {
                return element;
            }
        }
        return null;
    }

}
