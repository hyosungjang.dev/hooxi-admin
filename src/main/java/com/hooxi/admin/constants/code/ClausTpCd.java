package com.hooxi.admin.constants.code;

import com.hooxi.admin.util.LegacyCommonType;
import com.hooxi.admin.exception.common.CommonCodeException;
import lombok.Getter;

@Getter
public enum ClausTpCd implements LegacyCommonType {
    HOOXI("후시 이용 약관","CLT001"),
    PRIVACY("개인정보처리방침","CLT002"),
    MEMBERSHIP("멤버쉽등록약관","CLT003"),
    ACTION("멤버쉽등록약관","CLT004");

    private String desc;
    private String legacyCode;

    ClausTpCd(String desc
            , String legacyCode) {
        this.desc = desc;
        this.legacyCode = legacyCode;
    }

    public static ClausTpCd lookup(String code) {
        if( code == null ) {
            throw new CommonCodeException("Code Is Null");
        }
        for (ClausTpCd element : ClausTpCd.values()) {
            if (element.legacyCode.equalsIgnoreCase(code)) {
                return element;
            }
        }
        return null;
    }

}
