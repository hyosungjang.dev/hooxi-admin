package com.hooxi.admin.constants.code;

import com.hooxi.admin.util.LegacyCommonType;
import com.hooxi.admin.exception.common.CommonCodeException;
import lombok.Getter;

@Getter
public enum StoryStatCd implements LegacyCommonType {
    ALL("전체", "ALL"),

    ACTIVE("승인","SRS001"),

    WAITING("대기","SRS002"),

    REJECT("반려","SRS003"),

    INACTIVE("정지","SRS004");

    private String desc;
    private String legacyCode;

    StoryStatCd(String desc
            , String legacyCode) {
        this.desc = desc;
        this.legacyCode = legacyCode;
    }

    public static StoryStatCd lookup(String code) {
        if( code == null ) {
            throw new CommonCodeException("Code Is Null");
        }
        for (StoryStatCd element : StoryStatCd.values()) {
            if (element.legacyCode.equalsIgnoreCase(code)) {
                return element;
            }
        }
        return null;
    }

}
