package com.hooxi.admin.constants.code;

import com.hooxi.admin.util.LegacyCommonType;
import lombok.Getter;

@Getter
public enum SnsTpCd implements LegacyCommonType {
    FACEBOOK("Facebook","SNS001"),
    TWITTER("Twitter","SNS002"),
    INSTA("Instagram","SNS003"),
    YOUTUBE("Youtube","SNS004"),
    LINK("Link","SNS005"),
    KAKAOTALK("KAKAOTALK","SNS006");

    private String desc;
    private String legacyCode;

    SnsTpCd(String desc
            , String legacyCode) {
        this.desc = desc;
        this.legacyCode = legacyCode;
    }

}
