package com.hooxi.admin.constants.code;

import com.hooxi.admin.util.LegacyCommonType;
import lombok.Getter;

@Getter
public enum RprtTpCd implements LegacyCommonType {
    STORY("스토리","RPT001"),
    COMMUNITY("커뮤니티","RPT002"),
    REPLY("댓글","RPT003");

    private String desc;
    private String legacyCode;

    RprtTpCd(String desc
            , String legacyCode) {
        this.desc = desc;
        this.legacyCode = legacyCode;
    }

}
