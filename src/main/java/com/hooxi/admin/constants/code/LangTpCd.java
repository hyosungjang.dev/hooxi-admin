package com.hooxi.admin.constants.code;

import com.hooxi.admin.util.LegacyCommonType;
import lombok.Getter;

@Getter
public enum LangTpCd implements LegacyCommonType {
    KO("Korea","ko"),

    EN("English", "en");

    private String desc;
    private String legacyCode;

    LangTpCd(String desc
            , String legacyCode) {
        this.desc = desc;
        this.legacyCode = legacyCode;
    }

    public static LangTpCd lookup(String code) {
//        if( code == null ) {
//            throw new CommonCodeException("Code Is Null");
//        }
        for (LangTpCd element : LangTpCd.values()) {
            if (element.legacyCode.equalsIgnoreCase(code)) {
                return element;
            }
        }
        return null;
    }

}
