package com.hooxi.admin.constants.code;

import com.hooxi.admin.util.LegacyCommonType;
import com.hooxi.admin.exception.common.CommonCodeException;
import lombok.Getter;

@Getter
public enum TpcTpCd implements LegacyCommonType {
    FEED("FEED","TPC001"),
    ACTION("ACTION","TPC002"),
    CHALLENGE("CHALLENGE","TPC003"),
    STORY("STORY","TPC004"),
    COMMUNITY("COMMUNITY","TPC05");

    private String desc;
    private String legacyCode;

    TpcTpCd(String desc
            , String legacyCode) {
        this.desc = desc;
        this.legacyCode = legacyCode;
    }

    public static TpcTpCd lookup(String code) {
        if( code == null ) {
            throw new CommonCodeException("Code Is Null");
        }
        for (TpcTpCd element : TpcTpCd.values()) {
            if (element.legacyCode.equalsIgnoreCase(code)) {
                return element;
            }
        }
        return null;
    }

}
