package com.hooxi.admin.constants.code;

import com.hooxi.admin.util.LegacyCommonType;
import com.hooxi.admin.exception.common.CommonCodeException;
import lombok.Getter;

@Getter
public enum StoryTpCd implements LegacyCommonType {
    ALL("전체","ALL"),
    STORY("스토리","SRT001"),
    ACTION("행동","SRT002"),
    COMMUNITY("커뮤니티","SRT003"),
    CHALLENGE("챌린지","SRT004");

    private String desc;
    private String legacyCode;

    StoryTpCd(String desc
            , String legacyCode) {
        this.desc = desc;
        this.legacyCode = legacyCode;
    }

    public static StoryTpCd lookup(String code) {
        if( code == null ) {
            throw new CommonCodeException("Code Is Null");
        }
        for (StoryTpCd element : StoryTpCd.values()) {
            if (element.legacyCode.equalsIgnoreCase(code)) {
                return element;
            }
        }
        return null;
    }

}
