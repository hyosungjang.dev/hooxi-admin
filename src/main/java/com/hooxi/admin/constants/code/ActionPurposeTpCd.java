package com.hooxi.admin.constants.code;

import com.hooxi.admin.util.LegacyCommonType;
import com.hooxi.admin.exception.common.CommonCodeException;
import lombok.Getter;

@Getter
public enum ActionPurposeTpCd implements LegacyCommonType {
    ENERGY("에너지","PPT001"),
    CONSUMPTION("소비","PPT002"),
    TRANSPORT("수송","PPT003"),
    RESOURCE("자원순환","PPT004"),
    CARBON("흡수원","PPT005");

    private String desc;
    private String legacyCode;

    ActionPurposeTpCd(String desc
            , String legacyCode) {
        this.desc = desc;
        this.legacyCode = legacyCode;
    }

    public static ActionPurposeTpCd lookup(String code) {
        if( code == null ) {
            throw new CommonCodeException("Code Is Null");
        }
        for (ActionPurposeTpCd element : ActionPurposeTpCd.values()) {
            if (element.legacyCode.equalsIgnoreCase(code)) {
                return element;
            }
        }
        return null;
    }

}
