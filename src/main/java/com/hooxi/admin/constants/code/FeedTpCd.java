package com.hooxi.admin.constants.code;

import com.hooxi.admin.util.LegacyCommonType;
import com.hooxi.admin.exception.common.CommonCodeException;
import lombok.Getter;

@Getter
public enum FeedTpCd implements LegacyCommonType {
    ALL("전체","ALL"),
    LINK("링크형","FDT001"),
    PHOTO("사진형","FDT002"),
    VIDEO("비디오형","FDT003"),
    SHARE("퍼오기형","FDT004"),
    AD("광고형","FDT005"),
    TEXT("텍스트형","FDT006"),
    YOUTUBE("유튜브형","FDT007")
    ;

    private String desc;
    private String legacyCode;

    FeedTpCd(String desc
            , String legacyCode) {
        this.desc = desc;
        this.legacyCode = legacyCode;
    }

    public static FeedTpCd lookup(String code) {
        if( code == null ) {
            throw new CommonCodeException("Code Is Null");
        }
        for (FeedTpCd element : FeedTpCd.values()) {
            if (element.legacyCode.equalsIgnoreCase(code)) {
                return element;
            }
        }
        return null;
    }
}
