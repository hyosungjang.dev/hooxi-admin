package com.hooxi.admin.constants.code;

import com.hooxi.admin.util.LegacyCommonType;
import lombok.Getter;

@Getter
public enum RprtStatCd implements LegacyCommonType {
    RECEIPT("접수","RPS001"),
    IN("처리중","RPS002"),
    DONE("처리완료","RPS003"),
    REJECT("신고반려","RPS004");

    private String desc;
    private String legacyCode;

    RprtStatCd(String desc
            , String legacyCode) {
        this.desc = desc;
        this.legacyCode = legacyCode;
    }

}

