package com.hooxi.admin.constants.code;

import com.hooxi.admin.util.LegacyCommonType;
import com.hooxi.admin.exception.common.CommonCodeException;
import lombok.Getter;

@Getter
public enum GradTpCd implements LegacyCommonType {
    WAITING("WAITING MEMBER","GDT001"),
    WELCOME("WELCOME MEMBER","GDT002"),
    GREEN("GREEN MEMBER","GDT003"),
    CLIMATE("CLIMATE LEADER MEMBER","GDT004");

    private String desc;
    private String legacyCode;

    GradTpCd(String desc
            , String legacyCode) {
        this.desc = desc;
        this.legacyCode = legacyCode;
    }

    public static GradTpCd lookup(String code) {
        if( code == null ) {
            throw new CommonCodeException("Code Is Null");
        }
        for (GradTpCd element : GradTpCd.values()) {
            if (element.legacyCode.equalsIgnoreCase(code)) {
                return element;
            }
        }
        return null;
    }
}
