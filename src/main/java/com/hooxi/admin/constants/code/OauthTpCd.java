package com.hooxi.admin.constants.code;

import com.hooxi.admin.util.LegacyCommonType;
import com.hooxi.admin.exception.common.CommonCodeException;
import lombok.Getter;

@Getter
public enum OauthTpCd implements LegacyCommonType {
    GOOGLE("GOOGLE","OAT001"),
    FACEBOOK("FACEBOOK","OAT002"),
    APPLE("APPLE","OAT003"),
    EMAIL("EMAIL","OAT004");

    private String desc;
    private String legacyCode;

    OauthTpCd(String desc
            , String legacyCode) {
        this.desc = desc;
        this.legacyCode = legacyCode;
    }

    public static OauthTpCd lookup(String code) {
        if( code == null ) {
            throw new CommonCodeException("Code Is Null");
        }
        for (OauthTpCd element : OauthTpCd.values()) {
            if (element.legacyCode.equalsIgnoreCase(code)) {
                return element;
            }
        }
        return null;
    }

}
