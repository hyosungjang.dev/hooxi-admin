package com.hooxi.admin.constants.code;

import com.hooxi.admin.util.LegacyCommonType;
import com.hooxi.admin.exception.common.CommonCodeException;
import lombok.Getter;

@Getter
public enum PrivateTpCd implements LegacyCommonType {
    TOTAL("전체","PTT001"),
    SUBSCRIBE("구독자","PTT002"),
    ONLY_ME("나만","PTT003");

    private String desc;
    private String legacyCode;

    PrivateTpCd(String desc
            , String legacyCode) {
        this.desc = desc;
        this.legacyCode = legacyCode;
    }

    public static PrivateTpCd lookup(String code) {
        if( code == null ) {
            throw new CommonCodeException("Code Is Null");
        }
        for (PrivateTpCd element : PrivateTpCd.values()) {
            if (element.legacyCode.equalsIgnoreCase(code)) {
                return element;
            }
        }
        return null;
    }

}
