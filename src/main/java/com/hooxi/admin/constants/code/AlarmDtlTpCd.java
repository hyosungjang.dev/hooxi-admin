package com.hooxi.admin.constants.code;

import com.hooxi.admin.util.LegacyCommonType;
import com.hooxi.admin.exception.common.CommonCodeException;
import lombok.Getter;

@Getter
public enum AlarmDtlTpCd implements LegacyCommonType {
    NOTICE("공지","ADT001"),
    PROMOTION("프로모션","ADT002"),
    EVENT("이벤트","ADT003"),
    REPORT("신고","ADT004"),
    NEW_SUB_STORY("구독중인 계정 새 글","ADT005"),
    REPLY("댓글","ADT006"),
    SUBSCRIBE("구독","ADT007"),
    LIKE("좋아요","ADT008"),
    ACTION("행동","ADT009"),
    CHALLENGE("챌린지","ADT010"),


    //exp
    LEVEL_UP("레벨업","ADT011")
    ;
    private String desc;
    private String legacyCode;

    AlarmDtlTpCd(String desc
            , String legacyCode) {
        this.desc = desc;
        this.legacyCode = legacyCode;
    }

    public static AlarmDtlTpCd lookup(String code) {
        if( code == null ) {
            throw new CommonCodeException("Code Is Null");
        }
        for (AlarmDtlTpCd element : AlarmDtlTpCd.values()) {
            if (element.legacyCode.equalsIgnoreCase(code)) {
                return element;
            }
        }
        return null;
    }

}
