package com.hooxi.admin.constants.code;

import com.hooxi.admin.util.LegacyCommonType;
import com.hooxi.admin.exception.common.CommonCodeException;
import lombok.Getter;

@Getter
public enum InquiryAnsrTpCd implements LegacyCommonType {
    INQUIRY("문의","IAT001"),
    ANSWER("답변","IAT002");

    private String desc;
    private String legacyCode;

    InquiryAnsrTpCd(String desc
            , String legacyCode) {
        this.desc = desc;
        this.legacyCode = legacyCode;
    }

    public static InquiryAnsrTpCd lookup(String code) {
        if( code == null ) {
            throw new CommonCodeException("Code Is Null");
        }
        for (InquiryAnsrTpCd element : InquiryAnsrTpCd.values()) {
            if (element.legacyCode.equalsIgnoreCase(code)) {
                return element;
            }
        }
        return null;
    }

}
