package com.hooxi.admin.constants.code;

import com.hooxi.admin.util.LegacyCommonType;
import com.hooxi.admin.exception.common.CommonCodeException;
import lombok.Getter;

@Getter
public enum PopupCookieTpCd implements LegacyCommonType {
    TODAY("오늘만 안보기","PCT001"),
    WEEK("7일간 안보기","PCT002"),
    NEVER("다시 보지않기","PCT003");

    private String desc;
    private String legacyCode;

    PopupCookieTpCd(String desc
            , String legacyCode) {
        this.desc = desc;
        this.legacyCode = legacyCode;
    }

    public static PopupCookieTpCd lookup(String code) {
        if( code == null ) {
            throw new CommonCodeException("Code Is Null");
        }
        for (PopupCookieTpCd element : PopupCookieTpCd.values()) {
            if (element.legacyCode.equalsIgnoreCase(code)) {
                return element;
            }
        }
        return null;
    }

}
