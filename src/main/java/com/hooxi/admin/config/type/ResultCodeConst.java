package com.hooxi.admin.config.type;

public enum ResultCodeConst {
    //공통처리
    SUCCESS("0000"),            //성공하였습니다.
    NOT_FOUND_INFO("0010"),     //정보를 찾을 수 없습니다.

    //valid
    NOT_EXIST_PCODE("1001"),    //부모코드가 존재하지 않습니다.
    NOT_EXIST_DCODE("1002"),    //상세코드가 존재하지 않습니다.
    NOT_EXIST_ID("1003"),       //id가 존재하지 않습니다.
    NOT_FOUND_PCODE("1101"),    //부모코드 값이 존재하지 않습니다.
    NOT_FOUND_DCODE("1102"),    //상세코드 값이 존재하지 않습니다.
    NOT_VALID_PCODE("1201"),    //부모코드 값 형식이 잘못되었습니다.
    NOT_VALID_DCODE("1202"),    //상세코드 값 형식이 잘못되었습니다.
    NOT_VALID_USEYN("1203"),    //Use Type 값 형식이 잘못되었습니다.
    NOT_VALID_FORMAT("1204"),   //Format Type 값 형식이 잘못되었습니다.
    NOT_VALID_POPULAR("1205"),  //Popular Type 값 형식이 잘못되었습니다.
    NOT_VALID_VALUE("1206"),    //Value 형식이 잘못되었습니다.
    NOT_VALID_OVERLAP("1207"),    //V중복된 데이터가 있습니다.

    NOT_VALID_CREATE("1301"),    //작성 권한이 없습니다.
    NOT_VALID_READ("1302"),    //조회 권한이 없습니다.
    NOT_VALID_UPDATE("1303"),    //수정 권한이 없습니다.
    NOT_VALID_DELETE("1304"),    //삭제 권한이 없습니다.


    //Fail
    FAIL("2000"),               //서비스 실행에 실패하였습니다.

    //System Error
    ERROR("9999");              //시스템에러가 발생하였습니다.
    private String code;

    ResultCodeConst(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

}
