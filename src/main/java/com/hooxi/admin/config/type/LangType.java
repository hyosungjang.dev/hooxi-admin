package com.hooxi.admin.config.type;

public enum LangType {
    en("영어",39l),
    ko("한국어",104l),
    ;

    private String des;
    private Long seq;

    public static final LangType[] ALL = {ko, en};

    LangType(String des, Long seq) {
        this.des = des;
        this.seq = seq;
    }

    public String getName(){
        return name();
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public Long getSeq() {
        return seq;
    }

    public void setSeq(Long seq) {
        this.seq = seq;
    }
}
