package com.hooxi.admin.config.type;

public enum YnType {
    Y("Y","사용"),
    N("N","미사용");

    private String val;
    private String des;

    YnType(String val, String des) {
        this.val = val;
        this.des = des;
    }

    public String getVal() {
        return val;
    }

    public String getDes() {
        return des;
    }
    public String getName(){
        return name();
    }

}
