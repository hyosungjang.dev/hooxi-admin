package com.hooxi.admin.config.type;

import java.util.Arrays;


public enum CmnCodeType {
    ACTIVE_MRS("MRS0001","정상회원"),
    DROP_MRS("MRS0002","정지회원"),
    STOP_MRS("MRS0003","휴면회원"),
    DORMANT_MRS("MRS0004","탈퇴회원"),

    NORMAL_MST("MST0001","일반"),
    WELCOME_MST("MST0002","웰컴"),
    ENVIRONMENTALLEADER_MST("MST0003","환경리더"),


    NORMAL_MSS("MSS0001","정상"),
    WAITING_MSS("MSS0002","대기"),
    REJECT_MSS("MSS0003","반려"),
    STOP_MSS("MSS0004","정지"),

    NORMAL_MJT("MJT0001","일반"),
    CREATOR_MJT("MJT0002","크리에이터"),
    EXPERT_MJT("MJT0003","특정분야전문가"),
    POPULAR_MJT("MJT0004","인기인"),
    ENTERPRISE_MJT("MJT0005","기업"),
    BRAND_MJT("MJT0006","브랜드"),
    ORG_MJT("MJT0007","단체"),
    PUBLIC_MJT("MJT0008","공공"),
    ACTIVIST_MJT("MJT0009","사회활동가"),
    ETC_MJT("MJT0010","기타"),

    NORMAL_SRS("SRS0001","정상"),
    WAITING_SRS("SRS0002","대기"),
    REJECT_SRS("SRS0003","반려"),
    STOP_SRS("SRS0004","정지"),

    STORY_SRT("SRT0001","스토리"),
    COMMUNITY_SRT("SRT0002","커뮤니티"),
    GROUP_SRT("SRT0003","모임"),

    NEWS_FDT("FDT0001","뉴스형"),
    PHOTO_FDT("FDT0002","사진형"),
    VODEO_FDT("FDT0003","비디오형"),
    SHARE_FDT("FDT0004","퍼오기형"),
    AD_FDT("FDT0005","광고형"),
    TEST_FDT("FDT0006","텍스트형"),
    OFFICEAD_FDT("FDT0007","기업광고형"),

    TOTAL_PTT("PTT0001","전체"),
    SUBSCRIPBE_PTT("PTT0002","구독자"),
    ONLYME_PTT("PTT0003","나만"),
    FOLLOWER_PTT("PTT0004","팔로워"),

    GOOGLE_OAT("OAT0001","구글"),
    FACEBOOK_OAT("OAT0002","페북"),
    APPLE_OAT("OAT0003","애플"),
    MAIL_OAT("OAT0004","메일"),
    NAVER_OAT("OAT0005","네이버"),
    KAKAO_OAT("OAT0006","카카오"),
    INSTAR_OAT("OAT0007","인스타"),

    KOREA_CNT("CNT0082","대한민국"),
    USA_CNT("CNT0001","미국"),
    UK_CNT("CNT0044","영국"),

    LOGIN_LGT("LGT0001","로그인"),
    LOGOUT_LGT("LGT0002","로그아웃"),
    LOGINFAIL_LGT("LGT0003","로그인실패"),
    PASSWORDFAIL_LGT("LGT0004","비밀번호 불일치"),

    USERSURVEY_SRT("SRT0001","유저설문조사"),
    TETATESTER_SRT("SRT0002","베타테스터설문"),
    MEMBERSHIP_SRT("SRT0003","멤버쉽설문"),

    SUBJECTIVE_SRA("SRA0001","주관식"),
    MULTIPLE_SRA("SRA0002","객관식"),
    ETC_SRA("SRA0003","기타"),

    FAMILY_GRT("GRT0001","가족"),
    SCHOOL_GRT("GRT0002","학교"),
    COMPANY_GRT("GRT0003","회사"),
    TEAM_GRT("GRT0004","팀"),

    ACCUMULATE_PIS("PIS0001","적립"),
    USE_PIS("PIS0002","사용"),
    CANCEL_PIS("PIS0003","취소"),
    COLLECT_PIS("PIS0004","회수"),
    EXCHANGE_PIS("PIS0005","교환"),
    EXPIRATION_PIS("PIS0006","만료"),

    FEED_PIT("PIT0001","피드"),
    ACTION_PIT("PIT0002","액션"),
    CHALLINGE_PIT("PIT0003","챌린지"),
    STORY_PIT("PIT0004","스토리"),




    USER_FLT("FLT0001","유저"),
    TAG_FLT("FLT0002","태그"),

    MAIL_ART("ART0001","이메일"),
    PUSH_ART("ART0002","푸시"),




    LEVEL1_LVT("LVT0001","Level1"),
    LEVEL2_LVT("LVT0002","Level2"),
    LEVEL3_LVT("LVT0003","Level3"),
    LEVEL4_LVT("LVT0004","Level4"),




    ACTIVE("01","정상회원"),
    DROP("02","정지회원"),
    STOP("03","휴면회원"),
    DORMANT("04","탈퇴회원");

    private String code;
    private String des;

    CmnCodeType(String code, String des) {
        this.code = code;
        this.des = des;
    }

    public static CmnCodeType ofCode(String code){
        return Arrays.stream(CmnCodeType.values())
                .filter(e -> e.getCode().equals(code))
                .findAny()
                .orElseThrow(() -> new IllegalArgumentException());
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name();
    }
}
