package com.hooxi.admin.config.type;

public enum LoginStatus {

    LOGIN_LGT("LGT0001","로그인"),
    LOGOUT_LGT("LGT0002","로그아웃"),
    LOGINFAIL_LGT("LGT0003","로그인실패"),
    PASSWORDFAIL_LGT("LGT0004","비밀번호 불일치");

    private String val;
    private String des;

    LoginStatus(String val, String des) {
        this.val = val;
        this.des = des;
    }

    public String getVal(){
        return this.val;
    }

}
