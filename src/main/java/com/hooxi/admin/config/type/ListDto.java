package com.hooxi.admin.config.type;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class ListDto {

    @Schema(description = "Paging size")
    private int size;

    @Schema(description = "Paging Number")
    private int page;

    @Schema(description = "Total Paging Size")
    private long totalSize;

    @Schema(description = "Total Page")
    private long totalPage;

    @Schema(description = "List DTO")
    private Object list;
    
}
