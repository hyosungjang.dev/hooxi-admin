package com.hooxi.admin.config.type;

import java.util.Arrays;
import java.util.List;

public enum PMenu {

    USER_LIST("회원관리", Arrays.asList(CMenu.USER_R, CMenu.MEMBERSHIP_R, CMenu.COMPANY_R), 1L, "USER_LIST_R"),
    ACTIVE_LIST("활동관리", Arrays.asList(CMenu.STORY_R,CMenu.STORYAC_R,CMenu.STORYCH_R, CMenu.CATE_R, CMenu.ACTION_R, CMenu.CHALLENGE_R, CMenu.CHALLENGE_ACTION_R), 2L, "ACTIVE_LIST_R"),
    ADMIN_LIST("관리자관리", Arrays.asList(CMenu.ADMINLIST_R, CMenu.PERMISSION_R), 3L, "ADMIN_LIST_R");
    private String val;
    private List<CMenu> cMenu;
    private Long seq;
    private String authName;
    public static final PMenu[] ALL = {USER_LIST, ACTIVE_LIST, ADMIN_LIST};

    PMenu(String val, List<CMenu> cMenu, Long seq, String authName) {
        this.val = val;
        this.cMenu = cMenu;
        this.seq = seq;
        this.authName = authName;
    }

    public List<CMenu> getCMenu() {
        return cMenu;
    }

    public String getVal() {
        return val;
    }

    public String getName() {
        return name();
    }

    public Long getSeq() {
        return seq;
    }

    public String getAuthName() {
        return authName;
    }

}
