package com.hooxi.admin.db.repository.member.dsl;

import com.hooxi.admin.biz.v1.user.model.MmbrshpJobsRes;
import com.hooxi.admin.db.entity.code.QTbCmnCode;
import com.hooxi.admin.db.entity.member.QTbMmbrshpJob;
import com.querydsl.core.types.ExpressionUtils;
import com.querydsl.core.types.Projections;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.annotation.Aspect;

import java.util.List;

@Aspect
@RequiredArgsConstructor
public class TbMmbrshpJobRepositoryDslImpl implements TbMmbrshpJobRepositoryDsl {

    private final JPAQueryFactory queryFactory;

    private QTbMmbrshpJob qTbMmbrshpJob = QTbMmbrshpJob.tbMmbrshpJob;
    private QTbCmnCode qTbCmnCode = QTbCmnCode.tbCmnCode;


    @Override
    public List<MmbrshpJobsRes> selectInfo(Long mmbrshpId) {
        List<MmbrshpJobsRes> result = queryFactory
                .select(Projections.fields(MmbrshpJobsRes.class,
                        qTbMmbrshpJob.mmbrshpJobId,
                        qTbMmbrshpJob.mmbrshpId,
                        qTbMmbrshpJob.jobKywdTpCd,
                        ExpressionUtils.as(qTbCmnCode.cdNm, "jobKywdTpCdDesc")
                ))
                .from(qTbMmbrshpJob)
                .join(qTbCmnCode)
                .on(qTbMmbrshpJob.jobKywdTpCd.eq(qTbCmnCode.cd), qTbMmbrshpJob.mmbrshpId.eq(mmbrshpId))
                .where(qTbCmnCode.grpCd.eq("JOB_KYWD_TP_CD"), qTbCmnCode.useYn.eq("Y"))
                .fetch();
        return result;
    }

}
