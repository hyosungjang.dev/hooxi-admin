package com.hooxi.admin.db.repository.user.dsl;

import com.hooxi.admin.biz.v1.user.model.UserProfileRes;
import com.hooxi.admin.db.entity.user.TbLoginHist;

import java.time.LocalDateTime;
import java.util.List;

public interface TbLoginHistRepositoryDsl {
    UserProfileRes selectCurrentLoginInfo(Long mmbrId);
}
