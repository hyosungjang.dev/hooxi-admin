package com.hooxi.admin.db.repository.exp;

import com.hooxi.admin.db.entity.exp.TbMmbrExp;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TbMmbrExpRepository extends JpaRepository<TbMmbrExp, Long> {
}
