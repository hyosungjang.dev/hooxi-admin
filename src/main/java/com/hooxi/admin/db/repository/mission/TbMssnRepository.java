package com.hooxi.admin.db.repository.mission;

import com.hooxi.admin.db.entity.mission.TbMssn;
import com.hooxi.admin.db.repository.mission.dsl.TbMssnRepositoryDsl;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TbMssnRepository extends JpaRepository<TbMssn, Long>, TbMssnRepositoryDsl {

    Optional<TbMssn> findByMssnId(Long id);
    Boolean existsByMssnId(Long id);
    int deleteByMssnId(Long id);

    TbMssn findTbMssnByMssnId(Long mssnId);
}
