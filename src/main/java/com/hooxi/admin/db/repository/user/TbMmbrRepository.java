package com.hooxi.admin.db.repository.user;

import com.hooxi.admin.biz.v1.user.model.UserListResInterface;
import com.hooxi.admin.db.entity.user.TbMmbr;
import com.hooxi.admin.db.repository.user.dsl.TbMmbrRepositoryDsl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface TbMmbrRepository extends JpaRepository<TbMmbr, Long>, TbMmbrRepositoryDsl {
    @Transactional
    @Modifying
    @Query(
            value = "UPDATE tb_mmbr set " +
                    "mmbr_status_cd = :mmbrStatusCd " +
                    "where " +
                    "mmbr_id = :mmbrId "
            , nativeQuery = true
    )
    int UpdateMemberStatus(
            @Param("mmbrStatusCd") String mmbrStatusCd,
            @Param("mmbrId") Long mmbrId);

    @Query(nativeQuery = true,
            value =
                    "SELECT COUNT(T3.MMBR_ID) " +
                    "FROM ( " +
                    "SELECT T1.*, T2.CREATOR, T2.MMBRSHP_STATE_CD " +
                    "FROM( " +
                    "SELECT A.MMBR_ID, A.NICKNAME, A.EMAIL, A.MAIN_LANG, A.REG_DT, B.CD_NM AS OAUTH_TP_CD " +
                    "FROM TB_MMBR A, TB_CMN_CODE B " +
                    "WHERE A.WTD_YN = 'N' " +
                    "AND B.GRP_CD  = 'OAUTH_TP_CD' " +
                    "AND B.CD = A.OAUTH_TP_CD " +
                    ")T1 LEFT OUTER JOIN ( " +
                    "SELECT A.MMBR_ID, A.NICKNAME AS CREATOR, B.CD_NM AS MMBRSHP_STATE_CD " +
                    "FROM TB_MMBRSHP A, TB_CMN_CODE B " +
                    "WHERE B.GRP_CD = 'MMBR_SHP_STAT_CD' " +
                    "AND B.CD = A.MMBRSHP_STATE_CD " +
                    ")T2 " +
                    "ON T1.MMBR_ID = T2.MMBR_ID " +
                    ")T3, ( " +
                    "SELECT MMBR_ID, MAX(LOGIN_DTTM) AS LOGIN_DTTM " +
                    "FROM TB_LOGIN_HIST " +
                    "GROUP BY MMBR_ID " +
                    ")T4 " +
                    "WHERE T3.MMBR_ID= T4.MMBR_ID ")
    int selectUserListPageInfo();
}