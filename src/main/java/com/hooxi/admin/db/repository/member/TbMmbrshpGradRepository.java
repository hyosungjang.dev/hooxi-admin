package com.hooxi.admin.db.repository.member;

import com.hooxi.admin.db.entity.member.TbMmbrshpGrad;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TbMmbrshpGradRepository extends JpaRepository<TbMmbrshpGrad, Long> {
}