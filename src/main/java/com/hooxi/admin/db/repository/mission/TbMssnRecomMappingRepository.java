package com.hooxi.admin.db.repository.mission;

import com.hooxi.admin.db.entity.mission.TbMssnRecomMapping;
import com.hooxi.admin.db.entity.mission.id.TbMssnRecomMappingId;
import com.hooxi.admin.db.entity.mission.id.TbMssnRecomMetaId;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TbMssnRecomMappingRepository extends JpaRepository<TbMssnRecomMapping, TbMssnRecomMappingId> {
    int deleteByMssnId(Long id);

    List<TbMssnRecomMapping> findByMssnId(Long mssnId);
    List<TbMssnRecomMapping> findByRecomId(Long recomId);

    int deleteByRecomId(Long recomId);
}
