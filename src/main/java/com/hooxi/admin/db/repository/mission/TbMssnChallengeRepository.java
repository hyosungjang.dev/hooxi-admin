package com.hooxi.admin.db.repository.mission;

import com.hooxi.admin.db.entity.mission.TbMssnChallenge;
import com.hooxi.admin.db.entity.mission.id.TbMssnChallengeId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TbMssnChallengeRepository extends JpaRepository<TbMssnChallenge, TbMssnChallengeId> {


}
