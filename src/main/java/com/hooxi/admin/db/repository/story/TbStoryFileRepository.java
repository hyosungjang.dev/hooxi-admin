package com.hooxi.admin.db.repository.story;

import com.hooxi.admin.db.entity.story.TbStoryFile;
import com.hooxi.admin.db.entity.story.id.TbStoryFileId;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TbStoryFileRepository extends JpaRepository<TbStoryFile, TbStoryFileId> {
    List<TbStoryFile> findTbStoryFileByStoryId(Long storyId);
}
