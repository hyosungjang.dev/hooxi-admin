package com.hooxi.admin.db.repository.member;

import com.hooxi.admin.db.entity.member.TbMmbrshpArticle;
import com.hooxi.admin.db.repository.member.dsl.TbMmbrshpArticleRepositoryDsl;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TbMmbrshpArticleRepository extends JpaRepository<TbMmbrshpArticle, Long>, TbMmbrshpArticleRepositoryDsl {
}