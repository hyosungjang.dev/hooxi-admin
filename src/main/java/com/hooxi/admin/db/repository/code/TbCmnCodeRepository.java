package com.hooxi.admin.db.repository.code;

import com.hooxi.admin.db.entity.code.TbCmnCode;
import com.hooxi.admin.db.entity.code.id.CmnCodeId;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface TbCmnCodeRepository extends JpaRepository<TbCmnCode, CmnCodeId> {

    List<TbCmnCode> findByUseYn(String use);

    List<TbCmnCode> findByCdContainsOrderByCdDesc(String cdContain);

    Optional<TbCmnCode> findByGrpCdAndCd(String grpCd, String cd);
}
