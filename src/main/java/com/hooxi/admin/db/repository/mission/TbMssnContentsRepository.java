package com.hooxi.admin.db.repository.mission;

import com.hooxi.admin.constants.code.LangTpCd;
import com.hooxi.admin.db.entity.mission.TbMssnContents;
import com.hooxi.admin.db.entity.mission.id.TbMssnContentsId;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface TbMssnContentsRepository extends JpaRepository<TbMssnContents, TbMssnContentsId> {

    boolean existsByMssnId(Long seq);

    int deleteByMssnId(Long seq);

    Optional<TbMssnContents> findByMssnIdAndLangCd(Long id, LangTpCd language);

    TbMssnContents findTbMssnContentsByMssnIdAndLangCd(Long mssnId, LangTpCd langCd);
}
