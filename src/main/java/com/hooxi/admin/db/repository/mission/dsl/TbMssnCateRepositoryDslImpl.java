package com.hooxi.admin.db.repository.mission.dsl;

import com.hooxi.admin.biz.v1.action.model.ActionMissionCateListReq;
import com.hooxi.admin.biz.v1.action.model.res.CategoryDtlRes;
import com.hooxi.admin.biz.v1.action.model.res.CategoryListRes;
import com.hooxi.admin.biz.v1.challenge.model.res.RecomChallengeListRes;
import com.hooxi.admin.constants.code.ActionPlaceTpCd;
import com.hooxi.admin.constants.code.ActionPurposeTpCd;
import com.hooxi.admin.constants.code.LangTpCd;
import com.hooxi.admin.constants.code.TpcTpCd;
import com.hooxi.admin.db.entity.admin.QAdmin;
import com.hooxi.admin.db.entity.mission.*;
import com.hooxi.admin.util.JpaPageUtil;
import com.querydsl.core.QueryResults;
import com.querydsl.core.types.ExpressionUtils;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.JPAExpressions;
import com.querydsl.jpa.JPQLQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.List;

@Aspect
@RequiredArgsConstructor
public class TbMssnCateRepositoryDslImpl implements TbMssnCateRepositoryDsl {

    private final JPAQueryFactory queryFactory;
    private final JpaPageUtil jpaPageUtil;

    private final QTbMssnCate qCate = QTbMssnCate.tbMssnCate;
    private final QTbMssnCateMeta qCateMeta = QTbMssnCateMeta.tbMssnCateMeta;
    private final QTbMssnCatePlace qCatePlace = QTbMssnCatePlace.tbMssnCatePlace;
    private final QTbMssnCateContents qCateContents = QTbMssnCateContents.tbMssnCateContents;

    private final QTbMssn qMssn = QTbMssn.tbMssn;
    
    private final QAdmin qAdmin = QAdmin.admin;

    /**
     * @apiNote 카테고리 상세
     * @param cateId
     * @param langTpCd
     * @return
     */
    @Override
    public CategoryDtlRes findCategoryByCateIdAndLangTpCd(Long cateId, LangTpCd langTpCd) {
        CategoryDtlRes query = queryFactory
                .select(Projections.fields(CategoryDtlRes.class,
                        qCate.cateId,
                        qCate.imgUrl,
                        qCateContents.cateTitle,
                        qCateContents.cateContents,
                        qCatePlace.placeCd,
                        qCateMeta.prpsCd
                ))
                .from(qCate)
                .innerJoin(qCateMeta).on(qCateMeta.cateId.eq(qCate.cateId))
                .innerJoin(qCatePlace).on(qCatePlace.cateId.eq(qCate.cateId))
                .innerJoin(qCateContents).on(qCateContents.cateId.eq(qCate.cateId))
                .where(
                        qCate.cateId.eq(cateId),
                        qCateContents.langCd.eq(langTpCd)
                ).fetchOne();
        return query;
    }

    /**
     * @apiNote 카테고리 리스트 조회
     * @param placeCd
     * @param prpsCd
     * @param langTpCd
     * @param pageable
     * @return
     */
    @Override
    public PageImpl<CategoryListRes> findCategoryList(ActionPlaceTpCd placeCd, ActionPurposeTpCd prpsCd, LangTpCd langTpCd, Pageable pageable) {
        JPQLQuery<CategoryListRes> query = queryFactory
                .select(Projections.fields(CategoryListRes.class,
                        qCate.cateId,
                        qCateContents.cateTitle,
                        qCateMeta.prpsCd,
                        qCatePlace.placeCd,
                        qCate.imgUrl,
                        qCate.regId,
                        qCate.regDt,
                        qCate.modDt,
                        ExpressionUtils.as(qAdmin.adminName, "nickname"),
                        qCate.useYn
                ))
                .from(qCate)
                .innerJoin(qCateContents).on(qCate.cateId.eq(qCateContents.cateId))
                .innerJoin(qCatePlace).on(qCate.cateId.eq(qCatePlace.cateId))
                .innerJoin(qCateMeta).on(qCate.cateId.eq(qCateMeta.cateId))
                .leftJoin(qAdmin).on(qCate.regId.eq(qAdmin.adminId))
                .where(
                        qCateContents.langCd.eq(LangTpCd.KO),
                        qCate.tpcTpCd.eq(TpcTpCd.ACTION),
                        eqPlaceCd(placeCd),
                        eqPrpsCd(prpsCd)
                )
                .orderBy(qCate.cateId.desc());
        return jpaPageUtil.getPageImpl(pageable, query, CategoryListRes.class);
    }

    private BooleanExpression eqPrpsCd(ActionPurposeTpCd prpsCd) {
        if (prpsCd == null) return null;
        return qCateMeta.prpsCd.eq(prpsCd);
    }

    private BooleanExpression eqPlaceCd(ActionPlaceTpCd placeCd) {
        if (placeCd == null) return null;
        return qCatePlace.placeCd.eq(placeCd);
    }

    @Override
    public PageImpl<RecomChallengeListRes> findChallengeCateList(Pageable pageable, LangTpCd langTpCd) {
        JPQLQuery<RecomChallengeListRes> query = queryFactory
                .select(Projections.fields(RecomChallengeListRes.class,
                        qCate.cateId,
                        qCate.imgUrl,
                        qCateContents.cateTitle,
                        qCateContents.cateContents,
                        qCate.regDt,
                        qCate.modDt,
                        qCate.regId,
                        ExpressionUtils.as(
                                JPAExpressions
                                        .select(qAdmin.adminName)
                                        .from(qAdmin)
                                        .where(qAdmin.adminId.eq(qCate.regId)),
                                "nickname"
                        ),
                        ExpressionUtils.as(
                                JPAExpressions
                                        .select(qMssn.count())
                                        .from(qMssn)
                                        .where(qMssn.cateId.eq(qCate.cateId), qMssn.useYn.eq("Y")),
                                "challengeCnt"
                        ),
                        ExpressionUtils.as(
                                JPAExpressions
                                        .select(qMssn.point.sum())
                                        .from(qMssn)
                                        .where(qMssn.cateId.eq(qCate.cateId), qMssn.useYn.eq("Y")),
                                "totalPoint"
                        )
                ))
                .from(qCate)
                .innerJoin(qCateContents).on(qCate.cateId.eq(qCateContents.cateId))
                .where(
                        qCate.tpcTpCd.eq(TpcTpCd.CHALLENGE),
                        qCateContents.langCd.eq(langTpCd)
                )
                .orderBy(qCate.cateId.desc());
        return jpaPageUtil.getPageImpl(pageable, query, RecomChallengeListRes.class);
    }

    @Override
    public List<TbMssnCate> selectListTbMssnCate(ActionMissionCateListReq dto) {
        if(dto.getSize() == 0){
            dto.setSize(10);
        }
        List<TbMssnCate> result = queryFactory.selectFrom(qCate)
                .where(
//                        eqSearchsearchSelect(dto.getSearchSelect(), dto.getSearchString()),
                        qCate.useYn.eq("Y"))
                .orderBy(qCate.regId.desc())
                .offset(dto.getPage()*dto.getSize())
                .limit(dto.getSize())
                .fetch();
        return result;
    }

    @Override
    public QueryResults<TbMssnCate> selectPageInfoTbMssnCate(ActionMissionCateListReq dto) {
        if(dto.getSize() == 0){
            dto.setSize(10);
        }
        QueryResults<TbMssnCate> result = queryFactory.selectFrom(qCate)
                .where(
//                        eqSearchsearchSelect(dto.getSearchSelect(), dto.getSearchString()),
                        qCate.useYn.eq("Y"))
                .orderBy(qCate.regId.desc())
                .offset(dto.getPage()*dto.getSize())
                .limit(dto.getSize())
                .fetchResults();
        return result;

    }

}
