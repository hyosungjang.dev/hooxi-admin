package com.hooxi.admin.db.repository.member;

import com.hooxi.admin.db.entity.member.TbSubscrt;
import com.hooxi.admin.db.entity.member.id.TbSubscrtId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TbSubscrtRepository extends JpaRepository<TbSubscrt, TbSubscrtId> {


}
