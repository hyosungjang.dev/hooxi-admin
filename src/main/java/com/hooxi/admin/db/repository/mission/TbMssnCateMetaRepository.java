package com.hooxi.admin.db.repository.mission;

import com.hooxi.admin.constants.code.ActionPlaceTpCd;
import com.hooxi.admin.constants.code.ActionPurposeTpCd;
import com.hooxi.admin.db.entity.mission.TbMssnCateMeta;
import com.hooxi.admin.db.entity.mission.id.TbMssnCateMetaId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface TbMssnCateMetaRepository extends JpaRepository<TbMssnCateMeta, TbMssnCateMetaId> {

    @Modifying
    @Transactional
    @Query(
            value = "update tb_mssn_cate_meta set prps_cd = :prpsCd where cate_id = :cateId"
            ,nativeQuery = true
    )
    void updatePrpsCdByCateId(@Param("cateId")Long cateId, @Param("prpsCd") String prpsCd);


}
