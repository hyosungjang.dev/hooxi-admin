package com.hooxi.admin.db.repository.mission;

import com.hooxi.admin.constants.code.ActionPlaceTpCd;
import com.hooxi.admin.constants.code.LangTpCd;
import com.hooxi.admin.db.entity.mission.TbMssnCateContents;
import com.hooxi.admin.db.entity.mission.id.TbMssnCateContentsId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface TbMssnCateContentsRepository extends JpaRepository<TbMssnCateContents, TbMssnCateContentsId> {

    TbMssnCateContents findByCateIdAndLangCd(Long cateId, LangTpCd langCd);

    @Modifying
    @Transactional
    @Query(
            value = "update tb_mssn_cate_contents set cate_contents = '', cate_title = '' where cate_id = '' and lang_cd = ''"
            ,nativeQuery = true
    )
    void updateContentsByCateIdAndLangCd(@Param("cateId")Long cateId, @Param("placeCd") ActionPlaceTpCd placeCd);


}
