package com.hooxi.admin.db.repository.exp;

import com.hooxi.admin.db.entity.exp.TbExp;
import com.hooxi.admin.db.entity.exp.id.TbExpId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TbExpRepository extends JpaRepository<TbExp, TbExpId> {

    boolean existsByExpAcmltIdAndExpStateCdAndExpAcmltTpCdAndExpAcmltDtlTpCd(Long seq, String expCode, String expAcmlt, String expAcmltDtl);


}
