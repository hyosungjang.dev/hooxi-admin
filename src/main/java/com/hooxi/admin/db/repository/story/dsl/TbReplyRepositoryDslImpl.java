package com.hooxi.admin.db.repository.story.dsl;

import com.hooxi.admin.biz.v1.reply.model.ReplyReq;
import com.hooxi.admin.biz.v1.reply.model.ReplyRes;
import com.hooxi.admin.constants.code.StoryStatCd;
import com.hooxi.admin.db.entity.exp.QTbMmbrExp;
import com.hooxi.admin.db.entity.member.QTbMmbrshp;
import com.hooxi.admin.db.entity.story.QTbReply;
import com.hooxi.admin.db.entity.story.QTbReplyLike;
import com.hooxi.admin.util.JpaPageUtil;
import com.querydsl.core.types.ExpressionUtils;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.JPAExpressions;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.time.LocalDateTime;
import java.util.List;

@Aspect
public class TbReplyRepositoryDslImpl implements TbReplyRepositoryDsl {
    private EntityManager em;

    private JPAQueryFactory queryFactory;

    private final JpaPageUtil jpaPageUtil;

    private final Logger logger = LoggerFactory.getLogger(TbReplyRepositoryDslImpl.class);
//    private final JpaPageUtil jpaPageUtil;

    private TbReplyRepositoryDslImpl(EntityManager em, JpaPageUtil jpaPageUtil) {
        this.em = em;
        this.queryFactory =  new JPAQueryFactory(em);
        this.jpaPageUtil = jpaPageUtil;
    }

    private final QTbReply qTbReply = QTbReply.tbReply;
    private final QTbReplyLike qTbReplyLike = QTbReplyLike.tbReplyLike;
    private final QTbMmbrshp qTbMmbrshp = QTbMmbrshp.tbMmbrshp;
    private final QTbMmbrExp qTbMmbrExp = QTbMmbrExp.tbMmbrExp;



    @Override
    public List<ReplyRes> selectReplies(Long storyId) {
        List<ReplyRes> result = queryFactory
                .select(Projections.fields(ReplyRes.class,
                        qTbReply.replyId,
                        qTbReply.mmbrId,
                        qTbReply.replyCtnt,
                        qTbReply.regDt,
                        qTbReply.useYn,
                        qTbMmbrshp.profileImg,
                        qTbMmbrshp.nickname,
                        ExpressionUtils.as(
                                JPAExpressions
                                        .select(qTbMmbrExp.lvlCd)
                                        .from(qTbMmbrExp)
                                        .where(qTbReply.mmbrId.eq(qTbMmbrExp.mmbrId)),
                                "lvlCd"),
                        ExpressionUtils.as(
                                JPAExpressions
                                        .select(qTbReplyLike.replyId.count())
                                        .from(qTbReplyLike)
                                        .where(qTbReplyLike.replyId.eq(qTbReply.replyId)),
                                "likeCnt")
                ))
                .from(qTbReply).leftJoin(qTbMmbrshp)
                .on(qTbReply.mmbrId.eq(qTbMmbrshp.mmbrId), qTbMmbrshp.useYn.eq("Y"))
                .where(qTbReply.storyId.eq(storyId), qTbReply.replyPrtId.isNull())
                .fetch();
        return result;
    }

    @Override
    public List<ReplyRes> selectReReplies(Long replyId) {
        List<ReplyRes> result = queryFactory
                .select(Projections.fields(ReplyRes.class,
                        qTbReply.replyId,
                        qTbReply.mmbrId,
                        qTbReply.replyCtnt,
                        qTbReply.regDt,
                        qTbReply.useYn,
                        qTbMmbrshp.profileImg,
                        qTbMmbrshp.nickname,
                        ExpressionUtils.as(
                                JPAExpressions
                                        .select(qTbMmbrExp.lvlCd)
                                        .from(qTbMmbrExp)
                                        .where(qTbReply.mmbrId.eq(qTbMmbrExp.mmbrId)),
                                "lvlCd"),
                        ExpressionUtils.as(
                                JPAExpressions
                                        .select(qTbReplyLike.replyId.count())
                                        .from(qTbReplyLike)
                                        .where(qTbReplyLike.replyId.eq(qTbReply.replyId)),
                                "likeCnt")
                ))
                .from(qTbReply).leftJoin(qTbMmbrshp)
                .on(qTbReply.mmbrId.eq(qTbMmbrshp.mmbrId), qTbMmbrshp.useYn.eq("Y"))
                .where(qTbReply.replyPrtId.eq(replyId))
                .fetch();
        return result;
    }

    @Override
    public Long selectReReplyCount(Long replyId) {
        Long result = queryFactory
                .select(qTbReply.replyId.count())
                .from(qTbReply)
                .where(qTbReply.replyPrtId.eq(replyId))
                .fetchOne();
        return result;
    }

    @Override
    public Long updateState(ReplyReq replyReq) {
        Long result = queryFactory
                .update(qTbReply)
                .set(qTbReply.useYn, replyReq.getUseYn())
                .set(qTbReply.modDt, LocalDateTime.now())
                .where(qTbReply.replyId.eq(replyReq.getReplyId()))
                .execute();
        return result;
    }
}
