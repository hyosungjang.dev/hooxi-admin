package com.hooxi.admin.db.repository.story.dsl;

import com.hooxi.admin.biz.v1.certify.model.CertifyListRes;
import com.hooxi.admin.biz.v1.story.model.*;
import com.hooxi.admin.constants.code.FeedTpCd;
import com.hooxi.admin.constants.code.MmbrShpStatCd;
import com.hooxi.admin.constants.code.StoryStatCd;
import com.hooxi.admin.constants.code.StoryTpCd;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

public interface TbStoryRepositoryDsl {

    PageImpl<StoryListRes> selectStoryList(Pageable pageable, StoryListReq storyListReq);

    Long selectAllCount(StoryTpCd storyTpCd, FeedTpCd feedTpCd);

    Long selectStoryCount(StoryTpCd storyTpCd, FeedTpCd feedTpCd, StoryStatCd storyStatCd);

    StoryRes selectStory(Long storyId);

    @Transactional
    Long updateStoryCntn(StoryReq storyReq);

    @Transactional
    Long updateStoryState(StoryReq storyReq);

    PageImpl<CertifyListRes> findCertifyList(Pageable pageable, StoryTpCd storyTpCd, StoryStatCd storyStatCd, FeedTpCd feedTpCd);

}
