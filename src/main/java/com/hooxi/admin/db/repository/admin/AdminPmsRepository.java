package com.hooxi.admin.db.repository.admin;

import com.hooxi.admin.db.entity.admin.AdminPms;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AdminPmsRepository extends JpaRepository<AdminPms, Long> {

}
