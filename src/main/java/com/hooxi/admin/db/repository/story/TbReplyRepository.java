package com.hooxi.admin.db.repository.story;

import com.hooxi.admin.biz.v1.reply.model.ReplyRes;
import com.hooxi.admin.db.entity.story.TbReply;
import com.hooxi.admin.db.entity.story.id.TbReplyId;
import com.hooxi.admin.db.repository.story.dsl.TbReplyRepositoryDsl;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TbReplyRepository extends JpaRepository<TbReply, TbReplyId>, TbReplyRepositoryDsl {
    List<TbReply> findAllByReplyPrtIdAndUseYn(Long replyId, String useYn);
}
