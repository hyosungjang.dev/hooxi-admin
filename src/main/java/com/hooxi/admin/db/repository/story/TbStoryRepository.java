package com.hooxi.admin.db.repository.story;

import com.hooxi.admin.db.entity.story.TbStory;
import com.hooxi.admin.db.repository.story.dsl.TbStoryRepositoryDsl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface TbStoryRepository extends JpaRepository<TbStory, Long>, TbStoryRepositoryDsl {

    @Transactional
    @Modifying
    @Query(
            value = "UPDATE tb_story set " +
                    "story_stat_cd = :storyStatCd " +
                    "where " +
                    "story_id = :storyId "
            , nativeQuery = true
    )
    int UpdateStoryStatus(
            @Param("storyStatCd") String storyStatCd,
            @Param("storyId") Long storyId);

    @Transactional
    @Modifying
    @Query(
            value = "UPDATE tb_story set " +
                    "story_cntn = :storyCntn " +
                    "where " +
                    "story_id = :storyId "
            , nativeQuery = true
    )
    int UpdateStoryContents(
            @Param("storyCntn") String storyCntn,
            @Param("storyId") Long storyId);

}
