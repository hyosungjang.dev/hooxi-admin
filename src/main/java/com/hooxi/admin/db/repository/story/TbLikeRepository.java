package com.hooxi.admin.db.repository.story;

import com.hooxi.admin.db.entity.story.TbLike;
import com.hooxi.admin.db.entity.story.id.TbLikeId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TbLikeRepository extends JpaRepository<TbLike, TbLikeId> {
}
