package com.hooxi.admin.db.repository.mission;

import com.hooxi.admin.constants.code.LangTpCd;
import com.hooxi.admin.db.entity.mission.TbMssnRecomContents;
import com.hooxi.admin.db.entity.mission.id.TbMssnRecomContentsId;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TbMssnRecomContentsRepository extends JpaRepository<TbMssnRecomContents, TbMssnRecomContentsId> {
    List<TbMssnRecomContents> findByLangCdAndRecomIdIn(LangTpCd langCd, List<Long> recomId);

    TbMssnRecomContents findByRecomIdAndLangCd(Long recomId, LangTpCd langCd);
}
