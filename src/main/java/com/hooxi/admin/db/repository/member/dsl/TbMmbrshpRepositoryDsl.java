package com.hooxi.admin.db.repository.member.dsl;

import com.hooxi.admin.biz.v1.user.model.MmbrshpConfirmReq;
import com.hooxi.admin.biz.v1.user.model.MmbrshpRes;
import org.springframework.transaction.annotation.Transactional;

public interface TbMmbrshpRepositoryDsl {
    MmbrshpRes selectInfo(Long mmbrshpId);

    @Transactional
    Long confirmMmbrshp(MmbrshpConfirmReq mmbrshpConfirmReq);
}
