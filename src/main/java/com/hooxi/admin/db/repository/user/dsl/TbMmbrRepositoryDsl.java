package com.hooxi.admin.db.repository.user.dsl;

import com.hooxi.admin.biz.v1.user.model.UserListReq;
import com.hooxi.admin.biz.v1.user.model.UserListRes;
import com.hooxi.admin.biz.v1.user.model.UserProfileRes;
import com.hooxi.admin.constants.code.MmbrShpStatCd;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

public interface TbMmbrRepositoryDsl {

    Long selectAllCount();

    Long selectMmbrshpCount(MmbrShpStatCd cd);

    PageImpl<UserListRes> selectUserList(Pageable pageable, UserListReq dto);

    UserProfileRes selectUserProfile(Long mmbrId);
}
