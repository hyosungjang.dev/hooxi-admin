package com.hooxi.admin.db.repository.user;

import com.hooxi.admin.db.entity.user.TbClausHist;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TbClausHistRepository extends JpaRepository<TbClausHist, Long> {
}