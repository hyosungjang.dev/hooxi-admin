package com.hooxi.admin.db.repository.member.dsl;

import com.hooxi.admin.biz.v1.user.model.MmbrshpJobsRes;

import java.util.List;

public interface TbMmbrshpJobRepositoryDsl {
    List<MmbrshpJobsRes> selectInfo(Long mmbrshpId);
}
