package com.hooxi.admin.db.repository.company;

import com.hooxi.admin.db.entity.company.TbCompany;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TbCompanyRepository extends JpaRepository<TbCompany, Long> {

    Optional<TbCompany> findByCompanyId(Long id);

}
