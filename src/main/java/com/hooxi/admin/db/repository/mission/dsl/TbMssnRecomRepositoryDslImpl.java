package com.hooxi.admin.db.repository.mission.dsl;

import com.hooxi.admin.biz.v1.action.model.ActionMissionListReq;
import com.hooxi.admin.biz.v1.action.model.res.*;
import com.hooxi.admin.constants.code.ActionPlaceTpCd;
import com.hooxi.admin.constants.code.ActionPurposeTpCd;
import com.hooxi.admin.constants.code.LangTpCd;
import com.hooxi.admin.constants.code.TpcTpCd;
import com.hooxi.admin.db.entity.admin.QAdmin;
import com.hooxi.admin.db.entity.mission.*;
import com.hooxi.admin.util.JpaPageUtil;
import com.querydsl.core.QueryResults;
import com.querydsl.core.types.ExpressionUtils;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.JPAExpressions;
import com.querydsl.jpa.JPQLQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.List;

@Aspect
@RequiredArgsConstructor
public class TbMssnRecomRepositoryDslImpl implements TbMssnRecomRepositoryDsl {

    private final JpaPageUtil jpaPageUtil;
    private final JPAQueryFactory queryFactory;

    // mission
    private final QTbMssn qTbMssn = QTbMssn.tbMssn;
    private final QTbMssnContents qTbMssnContents = QTbMssnContents.tbMssnContents;

    //recommend
    private final QTbMssnRecom qTbMssnRecom = QTbMssnRecom.tbMssnRecom;
    private final QTbMssnRecomContents qTbMssnRecomContents = QTbMssnRecomContents.tbMssnRecomContents;
    private final QTbMssnRecomMapping qTbMssnRecomMapping = QTbMssnRecomMapping.tbMssnRecomMapping;

    //member
    private final QAdmin qAdmin = QAdmin.admin;

    /**
     * @apiNote 추천 리스트 조회
     * @param langTpCd
     * @param pageable
     * @return
     */
    @Override
    public PageImpl<RecommendListRes> findRecomList(LangTpCd langTpCd, Pageable pageable) {
        JPQLQuery<RecommendListRes> query = queryFactory
                .select(Projections.fields(RecommendListRes.class,
                        qTbMssnRecom.recomId,
                        qTbMssnRecomContents.recomTitle,
                        qTbMssnRecomContents.recomContents,
                        qTbMssnRecom.imgUrl,
                        qTbMssnRecom.regDt,
                        qTbMssnRecom.modDt,
                        qTbMssnRecom.regId,
                        ExpressionUtils.as(qAdmin.adminName, "nickname"),
                        ExpressionUtils.as(
                                JPAExpressions
                                        .select(qTbMssnRecomMapping.mssnId.count())
                                        .from(qTbMssnRecomMapping)
                                        .where(qTbMssnRecomMapping.recomId.eq(qTbMssnRecom.recomId)),
                                "actionCnt"
                        ),
                        ExpressionUtils.as(
                                JPAExpressions
                                        .select(qTbMssn.point.sum())
                                        .from(qTbMssn)
                                        .innerJoin(qTbMssnRecomMapping)
                                        .on(qTbMssn.mssnId.eq(qTbMssnRecomMapping.recomId))
                                        .where(qTbMssnRecomMapping.recomId.eq(qTbMssnRecom.recomId)),
                                "totalPoint"
                        )
                ))
                .from(qTbMssnRecom)
                .innerJoin(qTbMssnRecomContents).on(qTbMssnRecom.recomId.eq(qTbMssnRecomContents.recomId))
                .leftJoin(qAdmin).on(qTbMssnRecom.regId.eq(qAdmin.adminId))
                .where(
                        qTbMssnRecomContents.langCd.eq(LangTpCd.KO)
                )
                .orderBy(qTbMssnRecom.recomId.desc());
        return jpaPageUtil.getPageImpl(pageable, query, RecommendListRes.class);
    }

    private JPQLQuery<RecomDtlRes> findRecomDtlBaseQuery(LangTpCd langTpCd) {
        return queryFactory
                .select(Projections.fields(RecomDtlRes.class,
                        qTbMssnRecom.recomId,
                        qTbMssnRecom.imgUrl,
                        qTbMssnRecom.useYn,
                        qTbMssnRecomContents.recomTitle,
                        qTbMssnRecomContents.recomContents
                ))
                .from(qTbMssnRecom)
                .innerJoin(qTbMssnRecomContents).on(qTbMssnRecom.recomId.eq(qTbMssnRecomContents.recomId))
                .where(qTbMssnRecomContents.langCd.eq(langTpCd))
                ;
    }

    @Override
    public List<RecomDtlRes> findRecomMappingByRecomId(List<Long> recomIds, LangTpCd langTpCd) {
        JPQLQuery query = this.findRecomDtlBaseQuery(langTpCd)
                .where(
                        qTbMssnRecom.recomId.in(recomIds)
                );
        return query.fetch();
    }

    @Override
    public RecomDtlRes findRecomDtlByRecomId(Long recomId, LangTpCd langTpCd) {
        RecomDtlRes res = this.findRecomDtlBaseQuery(langTpCd)
                .where(
                        qTbMssnRecom.recomId.eq(recomId)
                )
                .fetchOne()
                ;
        return res;
    }

    @Override
    public List<ActionRecomRes> findActionRecomByMssnId(List<Long> mssnIds, LangTpCd langTpCd) {

        return queryFactory
                .select(Projections.fields(ActionRecomRes.class,
                        qTbMssn.mssnId,
                        qTbMssnContents.mssnTitle
                ))
                .from(qTbMssn)
                .innerJoin(qTbMssnContents).on(qTbMssn.mssnId.eq(qTbMssnContents.mssnId))
                .where(
                        qTbMssn.mssnId.in(mssnIds),
                        qTbMssnContents.langCd.eq(langTpCd)
                )
                .fetch();
    }

}
