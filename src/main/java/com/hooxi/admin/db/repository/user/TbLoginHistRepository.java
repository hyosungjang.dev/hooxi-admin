package com.hooxi.admin.db.repository.user;

import com.hooxi.admin.db.entity.user.TbLoginHist;
import com.hooxi.admin.db.repository.user.dsl.TbLoginHistRepositoryDsl;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TbLoginHistRepository extends JpaRepository<TbLoginHist, Long>, TbLoginHistRepositoryDsl {
}