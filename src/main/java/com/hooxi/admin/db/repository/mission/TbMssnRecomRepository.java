package com.hooxi.admin.db.repository.mission;

import com.hooxi.admin.db.entity.mission.TbMssnRecom;
import com.hooxi.admin.db.repository.mission.dsl.TbMssnRecomRepositoryDsl;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TbMssnRecomRepository extends JpaRepository<TbMssnRecom, Long>, TbMssnRecomRepositoryDsl {
    TbMssnRecom findByRecomId(Long recomId);

    boolean existsByRecomId(Long recomId);
}
