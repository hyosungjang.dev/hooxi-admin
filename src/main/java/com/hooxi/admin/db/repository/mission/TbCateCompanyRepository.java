package com.hooxi.admin.db.repository.mission;

import com.hooxi.admin.db.entity.mission.TbCateCompany;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TbCateCompanyRepository extends JpaRepository<TbCateCompany, Long> {
}
