package com.hooxi.admin.db.repository.admin;

import com.hooxi.admin.db.entity.admin.AdminHis;
import com.hooxi.admin.db.entity.admin.id.AdminHisId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AdminHisRepository extends JpaRepository<AdminHis, AdminHisId>  {

}
