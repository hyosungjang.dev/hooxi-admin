package com.hooxi.admin.db.repository.user.dsl;

import com.hooxi.admin.biz.v1.user.model.UserProfileRes;
import com.hooxi.admin.db.entity.user.QTbLoginHist;
import com.hooxi.admin.db.entity.user.TbLoginHist;
import com.querydsl.core.types.ExpressionUtils;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.JPAExpressions;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.annotation.Aspect;

import java.time.LocalDateTime;
import java.util.List;

@Aspect
@RequiredArgsConstructor
public class TbLoginHistRepositoryDslImpl implements TbLoginHistRepositoryDsl {

    private final JPAQueryFactory queryFactory;

    private QTbLoginHist qTbLoginHist = QTbLoginHist.tbLoginHist;


    @Override
    public UserProfileRes selectCurrentLoginInfo(Long mmbrId) {
        UserProfileRes result = queryFactory
                .select(Projections.fields(UserProfileRes.class,
                        qTbLoginHist.loginDttm,
                        qTbLoginHist.deviceOs,
                        qTbLoginHist.deviceName,
                        ExpressionUtils.as(
                                JPAExpressions
                                        .select(Expressions.stringTemplate("to_char({0}, '{1s}')", qTbLoginHist.loginDttm, "YYYYMMDD").countDistinct())
                                        .from(qTbLoginHist)
                                        .where(qTbLoginHist.mmbrId.eq(mmbrId)),
                                "loginDays")
                ))
                .from(qTbLoginHist)
                .where(qTbLoginHist.mmbrId.eq(mmbrId))
                .orderBy(qTbLoginHist.loginDttm.desc())
                .limit(1)
                .fetchOne();
        return result;
    }

}
