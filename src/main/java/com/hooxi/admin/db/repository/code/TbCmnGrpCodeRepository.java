package com.hooxi.admin.db.repository.code;

import com.hooxi.admin.db.entity.code.TbCmnGrpCode;
import com.hooxi.admin.db.entity.code.id.TbCmnGrpCodeId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TbCmnGrpCodeRepository extends JpaRepository<TbCmnGrpCode, TbCmnGrpCodeId> {
}
