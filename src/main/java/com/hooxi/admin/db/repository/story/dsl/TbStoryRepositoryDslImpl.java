package com.hooxi.admin.db.repository.story.dsl;

import com.hooxi.admin.biz.v1.certify.model.CertifyListRes;
import com.hooxi.admin.biz.v1.story.model.*;
import com.hooxi.admin.biz.v1.user.model.UserProfileRes;
import com.hooxi.admin.constants.code.FeedTpCd;
import com.hooxi.admin.constants.code.MmbrShpStatCd;
import com.hooxi.admin.constants.code.StoryStatCd;
import com.hooxi.admin.constants.code.StoryTpCd;
import com.hooxi.admin.db.entity.user.QTbMmbr;
import com.hooxi.admin.util.JpaPageUtil;
import com.hooxi.admin.db.entity.member.QTbMmbrshp;
import com.hooxi.admin.db.entity.member.QTbSubscrt;
import com.hooxi.admin.db.entity.story.QTbLike;
import com.hooxi.admin.db.entity.story.QTbReply;
import com.hooxi.admin.db.entity.story.QTbStory;
import com.querydsl.core.types.ExpressionUtils;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.JPAExpressions;
import com.querydsl.jpa.JPQLQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import java.time.LocalDateTime;

@Aspect
public class TbStoryRepositoryDslImpl implements TbStoryRepositoryDsl {

    private EntityManager em;
    private JPAQueryFactory queryFactory;

    private final JpaPageUtil jpaPageUtil;

    private final Logger logger = LoggerFactory.getLogger(TbStoryRepositoryDslImpl.class);
//    private final JpaPageUtil jpaPageUtil;

    private TbStoryRepositoryDslImpl(EntityManager em, JpaPageUtil jpaPageUtil) {
        this.em = em;
        this.queryFactory =  new JPAQueryFactory(em);
        this.jpaPageUtil = jpaPageUtil;
    }

    private final QTbStory qTbStory = QTbStory.tbStory;
    private final QTbLike qTbLike = QTbLike.tbLike;
    private final QTbMmbrshp qTbMmbrshp = QTbMmbrshp.tbMmbrshp;
    private final QTbSubscrt qTbSubscrt = QTbSubscrt.tbSubscrt;
    private final QTbReply qTbReply = QTbReply.tbReply;
    private final QTbMmbr qTbMmbr = QTbMmbr.tbMmbr;

    @Override
    public PageImpl<StoryListRes> selectStoryList(Pageable pageable, StoryListReq storyListReq) {
        JPQLQuery<StoryListRes> list = queryFactory
                .select(Projections.fields(StoryListRes.class,
                        qTbStory.mmbrId,
                        qTbStory.storyId,
                        qTbStory.viewCnt,
                        qTbStory.shareCnt,
                        qTbStory.mainImgUrl,
                        qTbStory.storyCntn,
                        qTbStory.storyStatCd,
                        qTbStory.feedTpCd,
                        qTbStory.regDt,
                        qTbStory.modDt,
                        qTbMmbrshp.nickname,
                        qTbMmbrshp.gradCd,
                        qTbMmbrshp.profileImg,
                        ExpressionUtils.as(
                                JPAExpressions
                                        .select(qTbLike.count())
                                        .from(qTbLike)
                                        .where(qTbLike.storyId.eq(qTbStory.storyId)),
                                "likeCnt"
                        ),
                        ExpressionUtils.as(
                                JPAExpressions
                                        .select(qTbReply.count())
                                        .from(qTbReply)
                                        .where(
                                                qTbReply.storyId.eq(qTbStory.storyId),
                                                qTbReply.useYn.eq("Y")
                                        ),
                                "replyCnt"
                        )
                ))
                .from(qTbStory)
                .join(qTbMmbrshp).on(qTbStory.mmbrId.eq(qTbMmbrshp.mmbrId))
                .where(
                        qTbMmbrshp.useYn.eq("Y")
                        , qTbStory.storyTpCd.eq(StoryTpCd.STORY)
                        , eqStoryStatCd(storyListReq.getState())
                        , eqFeedTpCd(storyListReq.getFeed())
                )
                .orderBy(qTbStory.regDt.desc());
        return jpaPageUtil.getPageImpl(pageable, list, StoryListRes.class);
    }

    @Override
    public Long selectAllCount(StoryTpCd storyTpCd, FeedTpCd feedTpCd) {
        Long result = queryFactory
                .select(qTbStory.storyId.count())
                .from(qTbStory)
                .where(eqStoryTpCd(storyTpCd), eqFeedTpCd(feedTpCd))
                .fetchOne();
        return result;
    }

    @Override
    public Long selectStoryCount(StoryTpCd storyTpCd, FeedTpCd feedTpCd, StoryStatCd storyStatCd) {
        Long result = queryFactory
                .select(qTbStory.storyId.count())
                .from(qTbStory)
                .where(eqStoryTpCd(storyTpCd), eqFeedTpCd(feedTpCd), eqStoryStatCd(storyStatCd))
                .fetchOne();
        return result;
    }

    @Override
    public StoryRes selectStory(Long storyId) {
        StoryRes result = queryFactory
                .select(Projections.fields(StoryRes.class,
                        qTbStory.storyStatCd,
                        qTbStory.storyTpCd,
                        qTbStory.feedTpCd,
                        qTbStory.actTlId,
                        qTbStory.storyCntn,
                        qTbStory.linkUrl,
                        qTbMmbrshp.profileImg,
                        qTbMmbrshp.nickname,
                        qTbMmbrshp.afltn
                ))
                .from(qTbStory).leftJoin(qTbMmbrshp)
                .on(qTbStory.mmbrId.eq(qTbMmbrshp.mmbrId), qTbMmbrshp.useYn.eq("Y"))
                .where(qTbStory.storyId.eq(storyId))
                .fetchOne();
        return result;
    }

    @Override
    public Long updateStoryCntn(StoryReq storyReq) {
        Long result = queryFactory
                .update(qTbStory)
                .set(qTbStory.storyCntn, storyReq.getStoryCntn())
                .set(qTbStory.modDt, LocalDateTime.now())
                .where(qTbStory.storyId.eq(storyReq.getStoryId()))
                .execute();
        return result;
    }

    @Override
    public Long updateStoryState(StoryReq storyReq) {
        Long result = queryFactory
                .update(qTbStory)
                .set(qTbStory.storyStatCd, storyReq.getStoryStatCd())
                .set(qTbStory.modDt, LocalDateTime.now())
                .where(qTbStory.storyId.eq(storyReq.getStoryId()))
                .execute();
        return result;
    }

    private BooleanExpression eqStoryStatCd(StoryStatCd storyStatCd) {
        if (storyStatCd == StoryStatCd.ALL) {
            return null;
        }
        return qTbStory.storyStatCd.eq(storyStatCd);
    }

    private BooleanExpression eqFeedTpCd(FeedTpCd feedTpCd) {
        if (feedTpCd == FeedTpCd.ALL) {
            return null;
        }
        return qTbStory.feedTpCd.eq(feedTpCd);
    }

    private BooleanExpression eqStoryTpCd(StoryTpCd storyTpCd) {
        if (storyTpCd == StoryTpCd.ALL) return qTbStory.storyTpCd.eq(StoryTpCd.ACTION).or(qTbStory.storyTpCd.eq(StoryTpCd.CHALLENGE));
        return qTbStory.storyTpCd.eq(storyTpCd);
    }

    @Override
    public PageImpl<CertifyListRes> findCertifyList(Pageable pageable, StoryTpCd storyTpCd, StoryStatCd storyStatCd, FeedTpCd feedTpCd) {
        JPQLQuery<CertifyListRes> query = queryFactory
                .select(Projections.fields(CertifyListRes.class,
                        qTbStory.storyId,
                        qTbStory.mmbrId,
                        qTbStory.actTlId,
                        qTbStory.storyStatCd,
                        qTbStory.storyTpCd,
                        qTbStory.feedTpCd,
                        qTbStory.storyCntn,
                        qTbStory.viewCnt,
                        qTbStory.shareCnt,
                        qTbStory.regDt,
                        qTbStory.modDt,
                        qTbStory.mainImgUrl,
                        ExpressionUtils.as(
                                JPAExpressions.select(qTbLike.count()).from(qTbLike).where(qTbLike.storyId.eq(qTbStory.storyId)),
                                "likeCnt"
                        ),
                        qTbMmbr.profileImg,
                        qTbMmbr.nickname,
                        ExpressionUtils.as(
                                JPAExpressions.select(qTbReply.count()).from(qTbReply).where(qTbReply.storyId.eq(qTbStory.storyId)),
                                "replyCnt"
                        )
                ))
                .from(qTbStory)
                .leftJoin(qTbMmbr).on(qTbStory.mmbrId.eq(qTbMmbr.mmbrId))
                .where(
                        eqStoryTpCd(storyTpCd),
                        eqFeedTpCd(feedTpCd),
                        eqStoryStatCd(storyStatCd)
                );
        return jpaPageUtil.getPageImpl(pageable, query, CertifyListRes.class);
    }
}
