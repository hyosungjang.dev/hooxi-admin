package com.hooxi.admin.db.repository.mission;

import com.hooxi.admin.db.entity.mission.TbMssnCate;
import com.hooxi.admin.db.repository.mission.dsl.TbMssnCateRepositoryDsl;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TbMssnCateRepository extends JpaRepository<TbMssnCate, Long>, TbMssnCateRepositoryDsl {

    Optional<TbMssnCate> findByCateId(Long id);

    TbMssnCate findTbMssnCateByCateId(Long id);
    Boolean existsByCateId(Long id);

    int deleteByCateId(Long id);
}
