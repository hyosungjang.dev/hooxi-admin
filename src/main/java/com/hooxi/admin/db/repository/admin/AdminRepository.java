package com.hooxi.admin.db.repository.admin;

import com.hooxi.admin.db.entity.admin.Admin;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface AdminRepository extends JpaRepository<Admin, Long> {

    Optional<Admin>  findByAdminEmailAndAdminPwd(String email, String pass);
    Optional<Admin> findByAdminEmail(String email);
    boolean existsByAdminEmailAndAdminPwd(String email, String pass);
    boolean existsByAdminEmail(String email);

    List<Admin> findByUseYn(String useYn);

}
