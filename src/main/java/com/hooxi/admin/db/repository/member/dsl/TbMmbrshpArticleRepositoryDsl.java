package com.hooxi.admin.db.repository.member.dsl;

import com.hooxi.admin.db.entity.member.TbMmbrshpArticle;

import java.util.List;

public interface TbMmbrshpArticleRepositoryDsl {
    List<TbMmbrshpArticle> selectInfo(Long mmbrshpId);
}
