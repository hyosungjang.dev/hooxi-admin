package com.hooxi.admin.db.repository.mission.dsl;

import com.hooxi.admin.biz.v1.action.model.ActionMissionListReq;
import com.hooxi.admin.biz.v1.action.model.res.*;
import com.hooxi.admin.constants.code.ActionPlaceTpCd;
import com.hooxi.admin.constants.code.ActionPurposeTpCd;
import com.hooxi.admin.constants.code.LangTpCd;
import com.hooxi.admin.db.entity.mission.TbMssn;
import com.querydsl.core.QueryResults;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface TbMssnRecomRepositoryDsl {

    PageImpl<RecommendListRes> findRecomList(LangTpCd langTpCd, Pageable pageable);

    RecomDtlRes findRecomDtlByRecomId(Long recomId, LangTpCd langTpCd);

    List<RecomDtlRes> findRecomMappingByRecomId(List<Long> recomId, LangTpCd langTpCd);

    List<ActionRecomRes> findActionRecomByMssnId(List<Long> mssnIds, LangTpCd langTpCd);

}
