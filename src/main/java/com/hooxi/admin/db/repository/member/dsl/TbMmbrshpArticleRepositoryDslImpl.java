package com.hooxi.admin.db.repository.member.dsl;

import com.hooxi.admin.biz.v1.user.model.UserProfileRes;
import com.hooxi.admin.db.entity.member.QTbMmbrshpArticle;
import com.hooxi.admin.db.entity.member.TbMmbrshpArticle;
import com.hooxi.admin.db.entity.member.TbMmbrshpSnsLink;
import com.hooxi.admin.db.entity.user.QTbLoginHist;
import com.querydsl.core.types.ExpressionUtils;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.JPAExpressions;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.annotation.Aspect;

import java.util.List;

@Aspect
@RequiredArgsConstructor
public class TbMmbrshpArticleRepositoryDslImpl implements TbMmbrshpArticleRepositoryDsl {

    private final JPAQueryFactory queryFactory;

    private QTbMmbrshpArticle qTbMmbrshpArticle = QTbMmbrshpArticle.tbMmbrshpArticle;


    @Override
    public List<TbMmbrshpArticle> selectInfo(Long mmbrshpId) {
        List<TbMmbrshpArticle> result = queryFactory
                .selectFrom(qTbMmbrshpArticle)
                .where(qTbMmbrshpArticle.useYn.eq("Y"), qTbMmbrshpArticle.mmbrshpId.eq(mmbrshpId))
                .fetch();
        return result;
    }

}
