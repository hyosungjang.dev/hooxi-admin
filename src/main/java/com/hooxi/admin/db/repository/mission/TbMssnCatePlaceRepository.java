package com.hooxi.admin.db.repository.mission;

import com.hooxi.admin.constants.code.ActionPlaceTpCd;
import com.hooxi.admin.db.entity.mission.TbMssnCatePlace;
import com.hooxi.admin.db.entity.mission.id.TbMssnCatePlaceId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface TbMssnCatePlaceRepository extends JpaRepository<TbMssnCatePlace, TbMssnCatePlaceId> {

    @Modifying
    @Transactional
    @Query(
            value = "update tb_mssn_cate_place set place_cd = :placeCd where cate_id = :cateId"
            ,nativeQuery = true
    )
    void updatePlaceCdByCateId(@Param("cateId")Long cateId, @Param("placeCd")String placeCd);


}
