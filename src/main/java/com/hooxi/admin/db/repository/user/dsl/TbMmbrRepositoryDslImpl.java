package com.hooxi.admin.db.repository.user.dsl;

import com.hooxi.admin.biz.v1.user.model.UserListReq;
import com.hooxi.admin.biz.v1.user.model.UserListRes;
import com.hooxi.admin.biz.v1.user.model.UserProfileRes;
import com.hooxi.admin.constants.code.StoryTpCd;
import com.hooxi.admin.db.entity.member.QTbSubscrt;
import com.hooxi.admin.db.entity.report.QTbRprt;
import com.hooxi.admin.db.entity.story.QTbStory;
import com.hooxi.admin.util.JpaPageUtil;
import com.hooxi.admin.constants.code.MmbrShpStatCd;
import com.hooxi.admin.db.entity.member.QTbMmbrshp;
import com.hooxi.admin.db.entity.user.QTbLoginHist;
import com.hooxi.admin.db.entity.user.QTbMmbr;
import com.querydsl.core.types.ExpressionUtils;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.JPAExpressions;
import com.querydsl.jpa.JPQLQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;


@Aspect
@RequiredArgsConstructor
public class TbMmbrRepositoryDslImpl implements TbMmbrRepositoryDsl {

    private final JPAQueryFactory queryFactory;

    private final JpaPageUtil jpaPageUtil;

    private QTbMmbr qTbMmbr = QTbMmbr.tbMmbr;
    private QTbMmbrshp qTbMmbrshp = QTbMmbrshp.tbMmbrshp;
    private QTbLoginHist qTbLoginHist = QTbLoginHist.tbLoginHist;
    private QTbSubscrt qTbSubscrt = QTbSubscrt.tbSubscrt;
    private QTbStory qTbStory = QTbStory.tbStory;
    private QTbRprt qTbRprt = QTbRprt.tbRprt;


    @Override
    public Long selectAllCount() {
        Long result = queryFactory
                .select(qTbMmbr.mmbrId.count())
                .from(qTbMmbr)
                .where(qTbMmbr.wtdYn.eq("N"))
                .fetchOne();
        return result;
    }

    @Override
    public Long selectMmbrshpCount(MmbrShpStatCd cd) {
        Long result = queryFactory
                .select(qTbMmbr.mmbrId.count())
                .from(qTbMmbr)
                .join(qTbMmbrshp)
                .on(qTbMmbr.mmbrId.eq(qTbMmbrshp.mmbrId))
                .where(qTbMmbr.wtdYn.eq("N"), qTbMmbrshp.useYn.eq("Y"), qTbMmbrshp.mmbrshpStateCd.eq(cd))
                .fetchOne();
        return result;
    }

    @Override
    public PageImpl<UserListRes> selectUserList(Pageable pageable, UserListReq dto) {
        JPQLQuery<UserListRes> result = queryFactory
                .select(Projections.fields(UserListRes.class,
                        qTbMmbr.mmbrId,
                        qTbMmbr.nickname,
                        qTbMmbr.email,
                        qTbMmbr.mainLang,
                        qTbMmbr.regDt,
                        qTbMmbr.oauthTpCd,
                        qTbMmbrshp.mmbrshpId,
                        qTbMmbrshp.mmbrshpStateCd,
                        ExpressionUtils.as(
                            JPAExpressions
                                    .select(qTbLoginHist.loginDttm.max())
                                    .from(qTbLoginHist)
                                    .where(qTbMmbr.mmbrId.eq(qTbLoginHist.mmbrId)),
                            "loginDttm")
                        ))
                .from(qTbMmbr)
                .leftJoin(qTbMmbrshp)
                .on(qTbMmbr.mmbrId.eq(qTbMmbrshp.mmbrId), qTbMmbrshp.useYn.eq("Y"))
                .where(qTbMmbr.wtdYn.eq("N"), eqMmbrShStatCd(dto.getType()))
                .orderBy(qTbMmbr.regDt.desc());
        return jpaPageUtil.getPageImpl(pageable, result, UserListRes.class);
    }

    @Override
    public UserProfileRes selectUserProfile(Long mmbrId) {
        UserProfileRes result = queryFactory
                .select(Projections.fields(UserProfileRes.class,
                        qTbMmbr.mmbrId,
                        qTbMmbr.nickname,
                        qTbMmbr.email,
                        qTbMmbr.mainLang,
                        qTbMmbr.profileImg,
                        qTbMmbr.regDt,
                        qTbMmbrshp.mmbrshpId,
                        qTbMmbrshp.gradCd,
                        qTbMmbrshp.mmbrshpNum,
                        qTbMmbrshp.stateMsg,
                        ExpressionUtils.as(
                                JPAExpressions
                                        .select(qTbRprt.rprtId.count())
                                        .from(qTbRprt)
                                        .where(qTbRprt.mmbrId.eq(qTbMmbr.mmbrId)),
                                "report"),
                        ExpressionUtils.as(
                                JPAExpressions
                                        .select(qTbSubscrt.mmbrId.count())
                                        .from(qTbSubscrt)
                                        .where(qTbSubscrt.mmbrId.eq(qTbMmbr.mmbrId)),
                                "subscribe"),
                        ExpressionUtils.as(
                                JPAExpressions
                                        .select(qTbSubscrt.mmbrId.count())
                                        .from(qTbSubscrt)
                                        .where(qTbSubscrt.mmbrshpId.eq(qTbMmbrshp.mmbrshpId)),
                                "subscriber"),
                        ExpressionUtils.as(
                                JPAExpressions
                                        .select(qTbStory.viewCnt.sum().coalesce(0L))
                                        .from(qTbStory)
                                        .where(qTbStory.mmbrId.eq(qTbMmbr.mmbrId)),
                                "view"),
                        ExpressionUtils.as(
                                JPAExpressions
                                        .select(qTbStory.storyId.count())
                                        .from(qTbStory)
                                        .where(qTbStory.mmbrId.eq(qTbMmbr.mmbrId),
                                                (qTbStory.storyTpCd.eq(StoryTpCd.ACTION).or(qTbStory.storyTpCd.eq(StoryTpCd.CHALLENGE)))),
                                "certify")
                ))
                .from(qTbMmbr)
                .leftJoin(qTbMmbrshp)
                .on(qTbMmbr.mmbrId.eq(qTbMmbrshp.mmbrId), qTbMmbrshp.useYn.eq("Y"))
                .where(qTbMmbr.wtdYn.eq("N"), qTbMmbr.mmbrId.eq(mmbrId))
                .fetchOne();
        return result;
    }

    private BooleanExpression eqMmbrShStatCd(MmbrShpStatCd mmbrShpStatCd) {
        if (mmbrShpStatCd == MmbrShpStatCd.ALL) {
            return null;
        }
        return qTbMmbrshp.mmbrshpStateCd.eq(mmbrShpStatCd);
    }
}
