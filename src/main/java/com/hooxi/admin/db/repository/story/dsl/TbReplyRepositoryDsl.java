package com.hooxi.admin.db.repository.story.dsl;

import com.hooxi.admin.biz.v1.reply.model.ReplyReq;
import com.hooxi.admin.biz.v1.reply.model.ReplyRes;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface TbReplyRepositoryDsl {
    List<ReplyRes> selectReplies(Long StoryId);
    List<ReplyRes> selectReReplies(Long replyId);

    Long selectReReplyCount(Long replyId);

    @Transactional
    Long updateState(ReplyReq replyReq);

}
