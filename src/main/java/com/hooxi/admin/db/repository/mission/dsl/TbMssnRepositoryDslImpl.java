package com.hooxi.admin.db.repository.mission.dsl;

import com.hooxi.admin.biz.v1.action.model.res.*;
import com.hooxi.admin.biz.v1.action.model.ActionMissionListReq;
import com.hooxi.admin.biz.v1.certify.model.CertifyRes;
import com.hooxi.admin.biz.v1.challenge.model.res.ChallengeDtlRes;
import com.hooxi.admin.biz.v1.challenge.model.res.ChallengeListRes;
import com.hooxi.admin.biz.v1.user.model.UserProfileRes;
import com.hooxi.admin.constants.code.*;
import com.hooxi.admin.db.entity.admin.QAdmin;
import com.hooxi.admin.db.entity.company.QTbCompany;
import com.hooxi.admin.util.JpaPageUtil;
import com.hooxi.admin.db.entity.mission.*;
import com.querydsl.core.QueryResults;
import com.querydsl.core.Tuple;
import com.querydsl.core.types.ExpressionUtils;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.JPAExpressions;
import com.querydsl.jpa.JPQLQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.List;

@Aspect
@RequiredArgsConstructor
public class TbMssnRepositoryDslImpl implements TbMssnRepositoryDsl {

    private final JpaPageUtil jpaPageUtil;
    private final JPAQueryFactory queryFactory;

    // mission
    private final QTbMssn qTbMssn = QTbMssn.tbMssn;
    private final QTbMssnContents qTbMssnContents = QTbMssnContents.tbMssnContents;

    //category
    private final QTbMssnCate qTbMssnCate = QTbMssnCate.tbMssnCate;
    private final QTbMssnCateMeta qTbMssnCateMeta = QTbMssnCateMeta.tbMssnCateMeta;
    private final QTbMssnCatePlace qTbMssnCatePlace = QTbMssnCatePlace.tbMssnCatePlace;
    private final QTbMssnCateContents qTbMssnCateContents = QTbMssnCateContents.tbMssnCateContents;

    private final QTbCompany qTbCompany = QTbCompany.tbCompany;

    private final QTbMssnChallenge qTbMssnChallenge = QTbMssnChallenge.tbMssnChallenge;

    //member
    private final QAdmin qAdmin = QAdmin.admin;

    private final QTbActTimeline qTbActTimeline = QTbActTimeline.tbActTimeline;

    /**
     * @apiNote 액션 리스트 조회
     * @param placeCd
     * @param prpsCd
     * @param langTpCd
     * @param pageable
     * @return
     */
    @Override
    public PageImpl<ActionListRes> findActionList(ActionPlaceTpCd placeCd, ActionPurposeTpCd prpsCd, LangTpCd langTpCd, Pageable pageable) {
        JPQLQuery<ActionListRes> query =  queryFactory
                .select(Projections.fields(ActionListRes.class,
                        qTbMssn.mssnId,
                        qTbMssn.cateId,
                        qTbMssnContents.mssnTitle,
                        qTbMssnCateContents.cateTitle,
                        qTbMssnCateMeta.prpsCd,
                        qTbMssnCatePlace.placeCd,
                        qTbMssn.regDt,
                        qTbMssn.modDt,
                        qTbMssn.point,
                        qTbMssn.exp,
                        qTbMssn.imgUrl,
                        qTbMssn.regId,
                        ExpressionUtils.as(qAdmin.adminName, "nickname"),
                        qTbMssn.useYn
                ))
                .from(qTbMssn)
                .innerJoin(qTbMssnContents).on(qTbMssn.mssnId.eq(qTbMssnContents.mssnId))
                .innerJoin(qTbMssnCateContents).on(qTbMssn.cateId.eq(qTbMssnCateContents.cateId))
                .innerJoin(qTbMssnCateMeta).on(qTbMssnCateMeta.cateId.eq(qTbMssn.cateId))
                .innerJoin(qTbMssnCatePlace).on(qTbMssnCatePlace.cateId.eq(qTbMssn.cateId))
                .leftJoin(qAdmin).on(qAdmin.adminId.eq(qTbMssn.regId))
                .where(
                        qTbMssnContents.langCd.eq(LangTpCd.KO),
                        qTbMssnCateContents.langCd.eq(LangTpCd.KO),
                        qTbMssn.tpcTpCd.eq(TpcTpCd.ACTION),
                        eqPrpsCd(prpsCd),
                        eqPlaceCd(placeCd)
                )
                .orderBy(qTbMssn.mssnId.desc());
        return jpaPageUtil.getPageImpl(pageable, query, ActionListRes.class);
    }

    private BooleanExpression eqPrpsCd(ActionPurposeTpCd prpsCd) {
        if (prpsCd == null) return null;
        return qTbMssnCateMeta.prpsCd.eq(prpsCd);
    }

    private BooleanExpression eqPlaceCd(ActionPlaceTpCd placeCd) {
        if (placeCd == null) return null;
        return qTbMssnCatePlace.placeCd.eq(placeCd);
    }

    /**
     *
     * @param mssnId
     * @param langTpCd
     * @return
     */
    @Override
    public ActionDtlRes findActionDtlByMssnId(Long mssnId, LangTpCd langTpCd) {
        ActionDtlRes res = queryFactory
                .select(Projections.fields(ActionDtlRes.class,
                        qTbMssn.mssnId,
                        qTbMssn.cateId,
                        qTbMssn.imgUrl,
                        qTbMssn.exp,
                        qTbMssn.point,
                        qTbMssn.useYn,
                        qTbMssnContents.mssnTitle,
                        qTbMssnContents.mssnTitleDtl,
                        qTbMssnContents.mssnEffect,
                        qTbMssnContents.mssnEtc,
                        qTbMssnContents.langCd,
                        qTbMssnCateContents.cateTitle,
                        qTbMssnCateMeta.prpsCd,
                        qTbMssnCatePlace.placeCd
                ))
                .from(qTbMssn)
                .innerJoin(qTbMssnContents).on(qTbMssnContents.mssnId.eq(qTbMssn.mssnId))
                .innerJoin(qTbMssnCateContents).on(qTbMssnCateContents.cateId.eq(qTbMssn.cateId))
                .innerJoin(qTbMssnCateMeta).on(qTbMssnCateMeta.cateId.eq(qTbMssn.cateId))
                .innerJoin(qTbMssnCatePlace).on(qTbMssnCatePlace.cateId.eq(qTbMssn.cateId))
                .where(
                        qTbMssn.mssnId.eq(mssnId),
                        qTbMssnContents.langCd.eq(langTpCd),
                        qTbMssnCateContents.langCd.eq(langTpCd)
                )
                .fetchOne();
        return res;
    }


    //###################################################################################################
    //###################################################################################################
    //###########################                CHALLENGE                ###############################
    //###################################################################################################
    //###################################################################################################


    @Override
    public PageImpl<ChallengeListRes> findChallengeList(Pageable pageable, LangTpCd langTpCd) {
        JPQLQuery<ChallengeListRes> query = queryFactory
                .select(Projections.fields(ChallengeListRes.class,
                        qTbMssn.mssnId,
                        qTbMssn.cateId,
                        qTbMssn.imgUrl,
                        qTbCompany.companyId,
                        qTbCompany.companyName,
                        qTbMssnContents.mssnTitle,
                        qTbMssnChallenge.challengeId,
                        qTbMssnChallenge.startDt,
                        qTbMssnChallenge.endDt,
                        qTbMssn.regId,
                        qTbMssn.regDt,
                        qTbMssn.modDt,
                        qTbMssnCatePlace.placeCd,
                        qTbMssnCateMeta.prpsCd,
                        ExpressionUtils.as(
                                JPAExpressions.select(qAdmin.adminName).from(qAdmin).where(qAdmin.adminId.eq(qTbMssn.regId)),
                                "nickname"
                        ),
                        qTbMssn.useYn
                ))
                .from(qTbMssn)
                .innerJoin(qTbMssnContents).on(qTbMssn.mssnId.eq(qTbMssnContents.mssnId))
                .innerJoin(qTbMssnCate).on(qTbMssn.cateId.eq(qTbMssnCate.cateId))
                .leftJoin(qTbCompany).on(qTbMssnCate.companyId.eq(qTbCompany.companyId))
                .leftJoin(qTbMssnChallenge).on(qTbMssn.mssnId.eq(qTbMssnChallenge.mssnId))
                .innerJoin(qTbMssnCatePlace).on(qTbMssn.cateId.eq(qTbMssnCatePlace.cateId))
                .innerJoin(qTbMssnCateMeta).on(qTbMssn.cateId.eq(qTbMssnCateMeta.cateId))
                .where(
                        qTbMssnContents.langCd.eq(langTpCd),
                        qTbMssn.tpcTpCd.eq(TpcTpCd.CHALLENGE)
                )
                .orderBy(qTbMssn.mssnId.desc());
        return jpaPageUtil.getPageImpl(pageable, query, ChallengeListRes.class);
    }

    @Override
    public ChallengeDtlRes findChallengeDtlByMssnId(Long mssnId) {
        ChallengeDtlRes res = queryFactory
                .select(Projections.fields(ChallengeDtlRes.class,
                        qTbMssn.mssnId,
                        qTbMssn.cateId,
                        qTbMssnContents.mssnTitle,
                        qTbMssnContents.mssnEtc,
                        qTbMssnContents.mssnTitleDtl,
                        qTbMssnContents.mssnEffect,
                        qTbMssnCateContents.cateTitle,
                        qTbCompany.companyId,
                        qTbCompany.companyName,
                        qTbMssn.imgUrl,
                        qTbMssn.point,
                        qTbMssn.exp,
                        qTbMssn.useYn,
                        qTbMssn.linkUrl,
                        qTbMssnCatePlace.placeCd,
                        qTbMssnCateMeta.prpsCd,
                        qTbMssnChallenge.rangeYn,
                        qTbMssnChallenge.endDt,
                        qTbMssnChallenge.startDt,
                        qTbMssnChallenge.particptnTpCd
                ))
                .from(qTbMssn)
                .innerJoin(qTbMssnContents).on(qTbMssnContents.mssnId.eq(qTbMssn.mssnId))
                .innerJoin(qTbMssnCate).on(qTbMssnCate.cateId.eq(qTbMssn.cateId))
                .innerJoin(qTbMssnCateContents).on(qTbMssnCateContents.cateId.eq(qTbMssnCate.cateId))
                .innerJoin(qTbCompany).on(qTbCompany.companyId.eq(qTbMssnCate.companyId))
                .innerJoin(qTbMssnCatePlace).on(qTbMssn.cateId.eq(qTbMssnCatePlace.cateId))
                .innerJoin(qTbMssnCateMeta).on(qTbMssn.cateId.eq(qTbMssnCateMeta.cateId))
                .leftJoin(qTbMssnChallenge).on(qTbMssn.mssnId.eq(qTbMssnChallenge.mssnId))
                .where(
                        qTbMssn.mssnId.eq(mssnId),
                        qTbMssnContents.langCd.eq(LangTpCd.KO),
                        qTbMssnCateContents.langCd.eq(LangTpCd.KO)
                )
                .fetchOne();
        return res;
    }












    @Override
    public List<TbMssn> selectListTbMssn(ActionMissionListReq dto) {
        if(dto.getSize() == 0){
            dto.setSize(10);
        }
        List<TbMssn> result = queryFactory.selectFrom(qTbMssn)
                .where(
//                        eqSearchsearchSelect(dto.getSearchSelect(), dto.getSearchString()),
                        qTbMssn.useYn.eq("Y"),
                        qTbMssn.tpcTpCd.eq(TpcTpCd.ACTION))
                .orderBy(qTbMssn.regId.desc())
                .offset(dto.getPage()*dto.getSize())
                .limit(dto.getSize())
                .fetch();
        return result;
    }

    @Override
    public QueryResults<TbMssn> selectPageInfoTbMssn(ActionMissionListReq dto) {
        if(dto.getSize() == 0){
            dto.setSize(10);
        }
        QueryResults<TbMssn> result = queryFactory.selectFrom(qTbMssn)
                .where(
//                        eqSearchsearchSelect(dto.getSearchSelect(), dto.getSearchString()),
                        qTbMssn.useYn.eq("Y"),
                        qTbMssn.tpcTpCd.eq(TpcTpCd.ACTION))
                .orderBy(qTbMssn.regId.desc())
                .offset(dto.getPage()*dto.getSize())
                .limit(dto.getSize())
                .fetchResults();
        return result;

    }

    @Override
    public CertifyRes selectCertify(Long actTlId) {
        CertifyRes result = queryFactory
                .select(Projections.fields(CertifyRes.class,
                        qTbActTimeline.actStatusCd,
                        ExpressionUtils.as(
                                JPAExpressions
                                        .select(qTbMssnCatePlace.placeCd)
                                        .from(qTbMssnCatePlace)
                                        .where(qTbMssnCatePlace.cateId.eq(qTbMssn.cateId)),
                                "placeCd"),
                        ExpressionUtils.as(
                                JPAExpressions
                                        .select(qTbMssnCateMeta.prpsCd)
                                        .from(qTbMssnCateMeta)
                                        .where(qTbMssnCateMeta.cateId.eq(qTbMssn.cateId)),
                                "prpsCd"),
                        ExpressionUtils.as(
                                JPAExpressions
                                        .select(qTbMssnCateContents.cateTitle)
                                        .from(qTbMssnCateContents)
                                        .where(qTbMssnCateContents.cateId.eq(qTbMssn.cateId)),
                                "cateTitle"),
                        ExpressionUtils.as(
                                JPAExpressions
                                        .select(qTbMssnCateContents.cateContents)
                                        .from(qTbMssnCateContents)
                                        .where(qTbMssnCateContents.cateId.eq(qTbMssn.cateId)),
                                "cateContents"),
                        ExpressionUtils.as(
                                JPAExpressions
                                        .select(qTbCompany.companyName)
                                        .from(qTbMssnCate)
                                        .join(qTbCompany)
                                        .on(qTbMssnCate.companyId.eq(qTbCompany.companyId))
                                        .where(qTbMssnCate.cateId.eq(qTbMssn.cateId)),
                                "companyName")
                ))
                .from(qTbActTimeline)
                .join(qTbMssn)
                .on(qTbActTimeline.mssnId.eq(qTbMssn.mssnId))
                .where(qTbActTimeline.actTlId.eq(actTlId))
                .fetchOne();
        return result;
    }

}
