package com.hooxi.admin.db.repository.member;

import com.hooxi.admin.constants.code.GradTpCd;
import com.hooxi.admin.constants.code.MmbrShpStatCd;
import com.hooxi.admin.db.entity.member.TbMmbrshp;
import com.hooxi.admin.db.repository.member.dsl.TbMmbrshpRepositoryDsl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

public interface TbMmbrshpRepository extends JpaRepository<TbMmbrshp, Long>, TbMmbrshpRepositoryDsl {

    @Transactional
    @Modifying
    @Query(
            value = "UPDATE tb_mmbrshp set " +
                    "mmbrshp_state_cd = :mmbrshpStateCd " +
                    ", grad_cd = :gradCd " +
                    ", confirm_dt = :confirmDt " +
                    "where " +
                    "mmbrshp_id = :mmbrshpId "
            , nativeQuery = true
    )
    int confirmMemberShip(
            @Param("mmbrshpId") Long mmbrshpId,
            @Param("mmbrshpStateCd") MmbrShpStatCd mmbrshpStateCd,
            @Param("gradCd") GradTpCd gradCd,
            @Param("confirmDt") LocalDateTime confirmDt
    );
}