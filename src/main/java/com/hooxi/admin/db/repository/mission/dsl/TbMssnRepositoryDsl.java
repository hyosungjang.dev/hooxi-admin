package com.hooxi.admin.db.repository.mission.dsl;

import com.hooxi.admin.biz.v1.action.model.res.*;
import com.hooxi.admin.biz.v1.action.model.ActionMissionListReq;
import com.hooxi.admin.biz.v1.certify.model.CertifyRes;
import com.hooxi.admin.biz.v1.challenge.model.res.ChallengeDtlRes;
import com.hooxi.admin.biz.v1.challenge.model.res.ChallengeListRes;
import com.hooxi.admin.constants.code.ActionPlaceTpCd;
import com.hooxi.admin.constants.code.ActionPurposeTpCd;
import com.hooxi.admin.constants.code.LangTpCd;
import com.hooxi.admin.db.entity.mission.TbMssn;
import com.querydsl.core.QueryResults;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface TbMssnRepositoryDsl {

    List<TbMssn> selectListTbMssn(ActionMissionListReq dto);
    QueryResults<TbMssn> selectPageInfoTbMssn(ActionMissionListReq dto);

    PageImpl<ActionListRes> findActionList(ActionPlaceTpCd placeCd, ActionPurposeTpCd prpsCd, LangTpCd langTpCd, Pageable pageable);

    ActionDtlRes findActionDtlByMssnId(Long mssnId, LangTpCd langTpCd);

    PageImpl<ChallengeListRes> findChallengeList(Pageable pageable, LangTpCd langTpCd);

    ChallengeDtlRes findChallengeDtlByMssnId(Long mssnId);

    CertifyRes selectCertify(Long actTlId);

}
