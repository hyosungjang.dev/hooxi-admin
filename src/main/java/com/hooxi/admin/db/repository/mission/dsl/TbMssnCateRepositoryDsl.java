package com.hooxi.admin.db.repository.mission.dsl;

import com.hooxi.admin.biz.v1.action.model.ActionMissionCateListReq;
import com.hooxi.admin.biz.v1.action.model.res.CategoryDtlRes;
import com.hooxi.admin.biz.v1.action.model.res.CategoryListRes;
import com.hooxi.admin.biz.v1.action.model.res.RecommendListRes;
import com.hooxi.admin.biz.v1.challenge.model.res.RecomChallengeListRes;
import com.hooxi.admin.constants.code.ActionPlaceTpCd;
import com.hooxi.admin.constants.code.ActionPurposeTpCd;
import com.hooxi.admin.constants.code.LangTpCd;
import com.hooxi.admin.db.entity.mission.TbMssnCate;
import com.querydsl.core.QueryResults;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface TbMssnCateRepositoryDsl {

    CategoryDtlRes findCategoryByCateIdAndLangTpCd(Long cateId, LangTpCd langTpCd);

    PageImpl<CategoryListRes> findCategoryList(ActionPlaceTpCd placeTpCd, ActionPurposeTpCd prpsCd, LangTpCd langTpCd, Pageable pageable);

    PageImpl<RecomChallengeListRes> findChallengeCateList(Pageable pageable, LangTpCd langTpCd);





    List<TbMssnCate> selectListTbMssnCate(ActionMissionCateListReq dto);
    QueryResults<TbMssnCate> selectPageInfoTbMssnCate(ActionMissionCateListReq dto);
}
