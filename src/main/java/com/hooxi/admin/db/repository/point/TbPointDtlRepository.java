package com.hooxi.admin.db.repository.point;

import com.hooxi.admin.db.entity.point.PointDtl;
import com.hooxi.admin.db.entity.point.id.PointDtlId;
import com.hooxi.admin.db.entity.point.type.PointStatCd;
import com.hooxi.admin.db.entity.point.type.TrstnDtlTpCd;
import com.hooxi.admin.db.entity.point.type.TrstnTpCd;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface TbPointDtlRepository extends JpaRepository<PointDtl, PointDtlId> {


    List<PointDtl> findPointDtlsByMmbrIdAndExpireDtGreaterThanEqualOrderByExpireDtAsc(Long mmbrId, LocalDateTime expireDt);
    List<PointDtl> findPointDtlsByMmbrIdAndPointIdOrderByTrstnDtAscPointDtlIdAsc(Long mmbrId, Long pointId);
    Optional<PointDtl> findByTrstnIdAndPointStateCdAndTrstnTpCdAndTrstnDtlTpCd(Long seq, PointStatCd psc, TrstnTpCd ttc, TrstnDtlTpCd dtd);

    /**
     * get pointDtl Id By point Id
     * @param pointId
     * @param pointStateCd
     * @return
     */
    @Query(nativeQuery = true,
            value = "select min(point_dtl_id) from tb_point_dtl\n" +
                    "where point_id = :pointId \n" +
                    "and mmbr_id = :mmbrId \n" +
                    "and point_state_cd = :pointStateCd "
    )
    Long getPointDtlId(@Param(value = "mmbrId") Long mmbrId, @Param(value = "pointId") Long pointId, @Param(value = "pointStateCd") String pointStateCd);


    @Query(value = "SELECT \n" +
            "    coalesce(sum(point),0) \n" +
            "FROM tb_point_dtl\n" +
            "WHERE mmbr_id = :mmbrId \n" +
            "AND trstn_dt between :startDt and :endDt ",
            nativeQuery = true)
    Long sumPointByMmbrIdAndStartDateAndEndDate(@Param("mmbrId") Long mmbrId, @Param("startDt") LocalDateTime startDt,@Param("endDt") LocalDateTime endDt);

    @Query(value = "SELECT \n" +
            "    coalesce(sum(point),0) \n" +
            "FROM tb_point_dtl\n" +
            "WHERE mmbr_id = :mmbrId \n" +
            "AND expire_dt >= now() ",
            nativeQuery = true)
    Long sumPointByMmbrId(@Param("mmbrId") Long mmbrId);
}

