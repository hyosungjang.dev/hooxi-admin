package com.hooxi.admin.db.repository.member;

import com.hooxi.admin.db.entity.member.TbMmbrshpSnsLink;
import com.hooxi.admin.db.repository.member.dsl.TbMmbrshpSnsLinkRepositoryDsl;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TbMmbrshpSnsLinkRepository extends JpaRepository<TbMmbrshpSnsLink, Long>, TbMmbrshpSnsLinkRepositoryDsl {
}