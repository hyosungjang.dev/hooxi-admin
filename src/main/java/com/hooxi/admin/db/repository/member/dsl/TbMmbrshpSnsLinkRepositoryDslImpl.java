package com.hooxi.admin.db.repository.member.dsl;

import com.hooxi.admin.db.entity.member.QTbMmbrshpSnsLink;
import com.hooxi.admin.db.entity.member.TbMmbrshpSnsLink;
import com.hooxi.admin.util.JpaPageUtil;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.annotation.Aspect;

import java.util.List;


@Aspect
@RequiredArgsConstructor
public class TbMmbrshpSnsLinkRepositoryDslImpl implements TbMmbrshpSnsLinkRepositoryDsl {

    private final JPAQueryFactory queryFactory;

    private final JpaPageUtil jpaPageUtil;

    private QTbMmbrshpSnsLink qTbMmbrshpSnsLink = QTbMmbrshpSnsLink.tbMmbrshpSnsLink;


    @Override
    public List<TbMmbrshpSnsLink> selectInfo(Long mmbrshpId) {
        List<TbMmbrshpSnsLink> result = queryFactory
                .selectFrom(qTbMmbrshpSnsLink)
                .where(qTbMmbrshpSnsLink.useYn.eq("Y"), qTbMmbrshpSnsLink.mmbrshpId.eq(mmbrshpId))
                .fetch();
        return result;
    }
}
