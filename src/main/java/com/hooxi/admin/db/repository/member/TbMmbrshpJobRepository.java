package com.hooxi.admin.db.repository.member;

import com.hooxi.admin.db.entity.member.TbMmbrshp;
import com.hooxi.admin.db.entity.member.TbMmbrshpJob;
import com.hooxi.admin.db.entity.member.id.TbMmbrshpJobId;
import com.hooxi.admin.db.repository.member.dsl.TbMmbrshpJobRepositoryDsl;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TbMmbrshpJobRepository extends JpaRepository<TbMmbrshpJob, TbMmbrshpJobId>, TbMmbrshpJobRepositoryDsl {
}