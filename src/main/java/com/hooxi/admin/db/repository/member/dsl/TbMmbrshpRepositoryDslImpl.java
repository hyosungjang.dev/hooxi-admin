package com.hooxi.admin.db.repository.member.dsl;

import com.hooxi.admin.biz.v1.user.model.MmbrshpConfirmReq;
import com.hooxi.admin.biz.v1.user.model.MmbrshpRes;
import com.hooxi.admin.db.entity.member.QTbMmbrshp;
import com.querydsl.core.types.Projections;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.annotation.Aspect;

@Aspect
@RequiredArgsConstructor
public class TbMmbrshpRepositoryDslImpl implements TbMmbrshpRepositoryDsl {

    private final JPAQueryFactory queryFactory;

    private QTbMmbrshp qTbMmbrshp = QTbMmbrshp.tbMmbrshp;


    @Override
    public MmbrshpRes selectInfo(Long mmbrshpId) {
        MmbrshpRes result = queryFactory
                .select(Projections.fields(MmbrshpRes.class,
                        qTbMmbrshp.mmbrshpId,
                        qTbMmbrshp.nickname,
                        qTbMmbrshp.afltn,
                        qTbMmbrshp.stateMsg,
                        qTbMmbrshp.mmbrshpStateCd,
                        qTbMmbrshp.rjctRsn,
                        qTbMmbrshp.frstRgstDttm,
                        qTbMmbrshp.fnlModDttm,
                        qTbMmbrshp.confirmDt
                ))
                .from(qTbMmbrshp)
                .where(qTbMmbrshp.useYn.eq("Y"), qTbMmbrshp.mmbrshpId.eq(mmbrshpId))
                .fetchOne();
        return result;
    }

    @Override
    public Long confirmMmbrshp(MmbrshpConfirmReq mmbrshpConfirmReq) {
        Long result = queryFactory
                .update(qTbMmbrshp)
                .set(qTbMmbrshp.mmbrshpStateCd, mmbrshpConfirmReq.getMmbrshpStateCd())
                .set(qTbMmbrshp.gradCd, mmbrshpConfirmReq.getGradCd())
                .set(qTbMmbrshp.confirmDt, mmbrshpConfirmReq.getConfirmDt())
                .where(qTbMmbrshp.mmbrshpId.eq(mmbrshpConfirmReq.getMmbrshpId()))
                .execute();
        return result;
    }

}
