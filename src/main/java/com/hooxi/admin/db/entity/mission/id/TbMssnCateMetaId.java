package com.hooxi.admin.db.entity.mission.id;

import com.hooxi.admin.constants.code.ActionPurposeTpCd;
import lombok.Data;

import java.io.Serializable;

@Data
public class TbMssnCateMetaId implements Serializable {
    private Long cateId;
    private ActionPurposeTpCd prpsCd;

}
