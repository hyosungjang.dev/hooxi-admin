package com.hooxi.admin.db.entity.mission;

import com.hooxi.admin.biz.v1.action.model.ActionMissionVO;
import com.hooxi.admin.biz.v1.action.model.req.ModActionReq;
import com.hooxi.admin.constants.code.LangTpCd;
import com.hooxi.admin.constants.codeConverter.LangTpCdConvert;
import com.hooxi.admin.db.entity.mission.id.TbMssnContentsId;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Table(name = "tb_mssn_contents")
@Entity
@Getter
@Setter
@NoArgsConstructor
@IdClass(TbMssnContentsId.class)
@DynamicUpdate
public class TbMssnContents {

    @Id
    @Column(name = "mssn_id", nullable = true)
    private Long mssnId;
    @Column(name = "lang_cd", nullable = true)
    @Convert(converter = LangTpCdConvert.class)
    private LangTpCd langCd;
    @Column(name = "mssn_title", nullable = true)
    private String mssnTitle;
    @Column(name = "mssn_title_dtl", nullable = true)
    private String mssnTitleDtl;
    @Column(name = "mssn_effect", nullable = true)
    private String mssnEffect;
    @Column(name = "mssn_etc", nullable = true)
    private String mssnEtc;

    @Builder
    public TbMssnContents(
            Long mssnId,
            LangTpCd langCd,
            String mssnTitle,
            String mssnTitleDtl,
            String mssnEffect,
            String mssnEtc
    ) {
        this.mssnId = mssnId;
        this.langCd = langCd;
        this.mssnTitle = mssnTitle;
        this.mssnTitleDtl = mssnTitleDtl;
        this.mssnEffect = mssnEffect;
        this.mssnEtc = mssnEtc;
    }

    public void update(ActionMissionVO request) {
        if(request.getMssnId() != null) {
            this.mssnId = request.getMssnId();
        }

        this.langCd = request.getLangCd();
        this.mssnTitle = request.getMssnTitle();
        this.mssnTitleDtl = request.getMssnTitleDtl();
        this.mssnEffect = request.getMssnEffect();
        this.mssnEtc = request.getMssnEtc();
    }

    public void updateContents(ModActionReq req) {

        this.mssnEffect = req.getMssnEffect();
        this.mssnEtc = req.getMssnEtc();
        this.mssnTitle = req.getMssnTitle();
        this.mssnTitleDtl = req.getMssnTitleDtl();
    }
}
