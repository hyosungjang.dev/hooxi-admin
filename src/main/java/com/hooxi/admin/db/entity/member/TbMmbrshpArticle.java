package com.hooxi.admin.db.entity.member;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "tb_mmbrshp_article")
@Entity
@Getter
@Setter
@NoArgsConstructor
public class TbMmbrshpArticle {
    @Id
    @Column(name = "mmbrshp_id", nullable = false)
    private Long mmbrshpId;

    @Column(name = "sort")
    private Integer sort;

    @Column(name = "link", length = 200)
    private String link;

    @Column(name = "use_yn", length = 1)
    private String useYn;

    @Builder
    public TbMmbrshpArticle(Long mmbrshpId, Integer sort, String link, String useYn) {
        this.mmbrshpId = mmbrshpId;
        this.sort = sort;
        this.link = link;
        this.useYn = useYn;
    }
}