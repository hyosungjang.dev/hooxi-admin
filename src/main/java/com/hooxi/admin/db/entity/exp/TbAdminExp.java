package com.hooxi.admin.db.entity.exp;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "tb_admin_exp")
@Entity
@Getter
@Setter
@NoArgsConstructor
public class TbAdminExp {

    @Id
    @Column(name = "admin_exp_id", nullable = true)
    private Long adminExpId;
    @Column(name = "lvl_cd", nullable = true)
    private String lvlCd;
    @Column(name = "ncssry_exp", nullable = true)
    private Long ncssryExp;
    @Column(name = "cntn", nullable = true)
    private String cntn;
    @Column(name = "commnt", nullable = true)
    private String commnt;
    @Column(name = "rwd", nullable = true)
    private String rwd;
    @Column(name = "title", nullable = true)
    private String title;

    @Builder
    public TbAdminExp(Long adminExpId, String lvlCd, Long ncssryExp, String cntn, String commnt, String rwd, String title) {
        this.adminExpId = adminExpId;
        this.lvlCd = lvlCd;
        this.ncssryExp = ncssryExp;
        this.cntn = cntn;
        this.commnt = commnt;
        this.rwd = rwd;
        this.title = title;
    }


}
