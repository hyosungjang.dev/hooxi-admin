package com.hooxi.admin.db.entity.story;


import com.hooxi.admin.db.entity.story.id.TbLikeId;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Table(name = "tb_like")
@Entity
@Getter
@Setter
@NoArgsConstructor
@IdClass(TbLikeId.class)
public class TbLike {

    @Id
    @Column(name = "story_id", nullable = true)
    private Long storyId;
    @Id
    @Column(name = "mmbr_id", nullable = true)
    private Long mmbrId;
    @Column(name = "use_yn", nullable = true)
    private String useYn;
    @Column(name = "reg_id", nullable = true)
    private Long regId;
    @Column(name = "reg_dt", nullable = true)
    private LocalDateTime regDt;
    @Column(name = "mod_id", nullable = true)
    private Long modId;
    @Column(name = "mod_dt", nullable = true)
    private LocalDateTime modDt;

    @Builder
    public TbLike(Long storyId, Long mmbrId, String useYn, Long regId, LocalDateTime regDt, Long modId, LocalDateTime modDt) {
        this.storyId = storyId;
        this.mmbrId = mmbrId;
        this.useYn = useYn;
        this.regId = regId;
        this.regDt = regDt;
        this.modId = modId;
        this.modDt = modDt;
    }
}
