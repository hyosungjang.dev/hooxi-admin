package com.hooxi.admin.db.entity.story;

import com.hooxi.admin.constants.code.FeedTpCd;
import com.hooxi.admin.constants.code.StoryStatCd;
import com.hooxi.admin.constants.code.StoryTpCd;
import com.hooxi.admin.constants.codeConverter.FeedTpCdConvert;
import com.hooxi.admin.constants.codeConverter.StoryStatCdConvert;
import com.hooxi.admin.constants.codeConverter.StoryTpCdConvert;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Table(name = "tb_story")
@Entity
@Getter
@Setter
@NoArgsConstructor
public class TbStory {
    @Id
    @Column(name = "story_id", nullable = true)
    private Long storyId;

    @Column(name = "mmbr_id", nullable = true)
    private Long mmbrId;

    @Column(name = "act_tl_id", nullable = true)
    private Long actTlId;

    @Column(name = "story_stat_cd", nullable = true)
    @Convert(converter = StoryStatCdConvert.class)
    private StoryStatCd storyStatCd;

    @Column(name = "story_tp_cd", nullable = true)
    @Convert(converter = StoryTpCdConvert.class)
    private StoryTpCd storyTpCd;

    @Column(name = "feed_tp_cd", nullable = true)
    @Convert(converter = FeedTpCdConvert.class)
    private FeedTpCd feedTpCd;

    @Column(name = "story_cntn", nullable = true)
    private String storyCntn;

    @Column(name = "link_url", nullable = true)
    private String linkUrl;

    @Column(name = "main_img_url", nullable = true)
    private String mainImgUrl;

    @Column(name = "reply_yn", nullable = true)
    private String replyYn;

    @Column(name = "views_yn", nullable = true)
    private String viewsYn;

    @Column(name = "like_yn", nullable = true)
    private String likeYn;

    @Column(name = "private_tp_cd", nullable = true)
    private String privateTpCd;

    @Column(name = "locat", nullable = true)
    private String locat;

    @Column(name = "view_cnt", nullable = true)
    private Long viewCnt;

    @Column(name = "share_cnt", nullable = true)
    private Long shareCnt;
    
    @Column(name = "reg_id", nullable = true)
    private Long regId;

    @Column(name = "reg_dt", nullable = true)
    private LocalDateTime regDt;

    @Column(name = "mod_id", nullable = true)
    private Long modId;

    @Column(name = "mod_dt", nullable = true)
    private LocalDateTime modDt;

}
