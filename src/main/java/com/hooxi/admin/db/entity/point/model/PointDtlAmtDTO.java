package com.hooxi.admin.db.entity.point.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
public class PointDtlAmtDTO {

    private Long dtlOrgPointId;
    private Long pointAmt;
    private Long orgPointAmt;
    private LocalDateTime expireDt;


}
