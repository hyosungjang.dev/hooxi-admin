package com.hooxi.admin.db.entity.admin.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class AdminPmsPmnId implements Serializable {
    private Long adminPmsPmnId;
    private Long adminPmsId;
}
