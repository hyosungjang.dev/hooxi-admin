package com.hooxi.admin.db.entity.mission;

import com.hooxi.admin.db.entity.mission.id.TbMssnRecomMappingId;
import com.hooxi.admin.db.entity.mission.id.TbMssnRecomMetaId;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Table(name = "tb_mssn_recom_mapping")
@Entity
@Getter
@Setter
@NoArgsConstructor
@IdClass(TbMssnRecomMappingId.class)
public class TbMssnRecomMapping {

    @Id
    @Column(name = "recom_id", nullable = true)
    private Long recomId;
    @Id
    @Column(name = "mssn_id", nullable = true)
    private Long mssnId;
    @Column(name = "use_yn", nullable = true)
    private String useYn;
    @Column(name = "reg_id", nullable = true)
    private Long regId;
    @Column(name = "reg_dttm", nullable = true)
    private LocalDateTime regDt;
    @Column(name = "mod_id", nullable = true)
    private Long modId;
    @Column(name = "mod_dttm", nullable = true)
    private LocalDateTime modDt;



    @Builder
    public TbMssnRecomMapping(Long recomId,
                              Long mssnId,
                              String useYn,
                              Long regId,
                              LocalDateTime regDt,
                              Long modId,
                              LocalDateTime modDt
    ) {
        this.recomId = recomId;
        this.mssnId = mssnId;
        this.useYn = useYn;
        this.regId = regId;
        this.regDt = regDt;
        this.modId = modId;
        this.modDt = modDt;
    }


}
