package com.hooxi.admin.db.entity.user.id;

import org.hibernate.Hibernate;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class TbClausAgrId implements Serializable {
    private static final long serialVersionUID = -7081860948529583891L;
    @Column(name = "mmbr_id", nullable = false)
    private Long mmbrId;
    @Column(name = "claus_id", nullable = false)
    private Long clausId;

    public Long getClausId() {
        return clausId;
    }

    public void setClausId(Long clausId) {
        this.clausId = clausId;
    }

    public Long getMmbrId() {
        return mmbrId;
    }

    public void setMmbrId(Long mmbrId) {
        this.mmbrId = mmbrId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(clausId, mmbrId);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        TbClausAgrId entity = (TbClausAgrId) o;
        return Objects.equals(this.clausId, entity.clausId) &&
                Objects.equals(this.mmbrId, entity.mmbrId);
    }
}