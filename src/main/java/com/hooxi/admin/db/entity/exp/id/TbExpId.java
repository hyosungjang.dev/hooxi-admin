package com.hooxi.admin.db.entity.exp.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class TbExpId implements Serializable {

    private Long expId;
    private Long mmbrId;

}
