package com.hooxi.admin.db.entity.mission;


import com.hooxi.admin.constants.code.ActionPlaceTpCd;
import com.hooxi.admin.constants.codeConverter.ActionPlaceTpCdConvert;
import com.hooxi.admin.db.entity.mission.id.TbMssnCatePlaceId;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Table(name = "tb_mssn_cate_place")
@Entity
@Getter
@Setter
@NoArgsConstructor
@IdClass(TbMssnCatePlaceId.class)
public class TbMssnCatePlace {
    @Id
    @Column(name = "cate_id", nullable = true)
    private Long cateId;

    @Column(name = "place_cd", nullable = true)
    @Convert(converter = ActionPlaceTpCdConvert.class)
    private ActionPlaceTpCd placeCd;

    @Builder
    public TbMssnCatePlace(Long cateId, ActionPlaceTpCd placeCd) {
        this.cateId = cateId;
        this.placeCd = placeCd;
    }


}
