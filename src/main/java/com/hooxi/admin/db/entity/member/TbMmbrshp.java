package com.hooxi.admin.db.entity.member;

import com.hooxi.admin.constants.code.GradTpCd;
import com.hooxi.admin.constants.code.MmbrShpStatCd;
import com.hooxi.admin.constants.codeConverter.GradTpCdConvert;
import com.hooxi.admin.constants.codeConverter.MmbrShpStatCdConvert;
import com.hooxi.admin.db.entity.member.id.TbMmbrshpId;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Table(name = "tb_mmbrshp")
@Entity
@Getter
@Setter
@NoArgsConstructor
@IdClass(TbMmbrshpId.class)
public class TbMmbrshp {
    @Id
    @Column(name = "mmbrshp_id", nullable = false)
    private Long mmbrshpId;

    @Id
    @Column(name = "mmbr_id", nullable = false)
    private Long mmbrId;

    @Column(name = "afltn", length = 50)
    private String afltn;

    @Column(name = "state_msg", length = 200)
    private String stateMsg;
    @Convert(converter = MmbrShpStatCdConvert.class)
    @Column(name = "mmbrshp_state_cd", length = 2)
    private MmbrShpStatCd mmbrshpStateCd;

    @Column(name = "grad_cd", length = 6)
    @Convert(converter = GradTpCdConvert.class)
    private GradTpCd gradCd;

    @Column(name = "frst_rgst_dttm")
    private LocalDateTime frstRgstDttm;

    @Column(name = "bg_img", length = 200)
    private String bgImg;

    @Column(name = "nickname", length = 60)
    private String nickname;

    @Column(name = "profile_img", length = 200)
    private String profileImg;

    @Column(name = "mmbrshp_num")
    private String mmbrshpNum;

    @Column(name = "rjct_rsn")
    private String rjctRsn;

    @Column(name = "fnl_mod_dttm")
    private LocalDateTime fnlModDttm;

    @Column(name = "use_yn")
    private String useYn;

    @Column(name = "confirm_dt")
    private LocalDateTime confirmDt;

    @Builder
    public TbMmbrshp(
            Long mmbrshpId,
            Long mmbrId,
            String afltn,
            String stateMsg,
            MmbrShpStatCd mmbrshpStateCd,
            GradTpCd gradCd,
            LocalDateTime frstRgstDttm,
            String bgImg,
            String nickname,
            String profileImg,
            String mmbrshpNum,
            String rjctRsn,
            LocalDateTime fnlModDttm
            ) {
        this.mmbrshpId = mmbrshpId;
        this.mmbrId = mmbrId;
        this.afltn = afltn;
        this.stateMsg = stateMsg;
        this.mmbrshpStateCd = mmbrshpStateCd;
        this.gradCd = gradCd;
        this.frstRgstDttm = frstRgstDttm;
        this.bgImg = bgImg;
        this.nickname = nickname;
        this.profileImg = profileImg;
        this.mmbrshpNum = mmbrshpNum;
        this.rjctRsn = rjctRsn;
        this.fnlModDttm = fnlModDttm;
    }
}