package com.hooxi.admin.db.entity.report;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Table(name = "tb_rprt")
public class TbRprt {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "rprt_id", nullable = false)
    private Long rprtId;

    @Column(name = "rprt_tp_cd", nullable = false, length = 6)
    private String rprtTpCd;

    @Column(name = "rprt_stat_cd", nullable = false, length = 6)
    private String rprtStatCd;

    @Column(name = "mmbr_id", nullable = false)
    private Long mmbrId;

    @Column(name = "rprt_trgt_mmbr_id", nullable = false)
    private Long rprtTrgtMmbrId;

    @Column(name = "rprt_tp_id", nullable = false)
    private Long rprtTpId;

    @Column(name = "rprt_rsn", length = 2000)
    private String rprtRsn;

    @Column(name = "reject_rsn", length = 2000)
    private String rejectRsn;

    @Column(name = "reg_id")
    private Long regId;

    @Column(name = "reg_dt")
    private Instant regDt;

    @Column(name = "mod_id")
    private Long modId;

    @Column(name = "mod_dt")
    private Instant modDt;

    @Column(name = "rprt_ctgr_cd", length = 6)
    private String rprtCtgrCd;
}