package com.hooxi.admin.db.entity.point.convert;

import com.hooxi.admin.util.AbstractLegacyEnumAttributeConverter;
import com.hooxi.admin.db.entity.point.type.TrstnTpCd;

import javax.persistence.Converter;

@Converter
public class TrstnTpCdConvert extends AbstractLegacyEnumAttributeConverter<TrstnTpCd> {
	public static final String ENUM_NAME = "거래유형코드";

	public TrstnTpCdConvert() {
		super(TrstnTpCd.class,true, ENUM_NAME);
	}
}
