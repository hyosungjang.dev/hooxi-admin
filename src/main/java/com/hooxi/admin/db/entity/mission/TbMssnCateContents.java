package com.hooxi.admin.db.entity.mission;

import com.hooxi.admin.biz.v1.action.model.req.ModCateReq;
import com.hooxi.admin.constants.code.LangTpCd;
import com.hooxi.admin.constants.codeConverter.LangTpCdConvert;
import com.hooxi.admin.db.entity.mission.id.TbMssnCateContentsId;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Table(name = "tb_mssn_cate_contents")
@Entity
@Getter
@Setter
@NoArgsConstructor
@IdClass(TbMssnCateContentsId.class)
public class TbMssnCateContents {
    @Id
    @Column(name = "cate_id", nullable = true)
    private Long cateId;
    @Column(name = "lang_cd", nullable = true)
    @Convert(converter = LangTpCdConvert.class)
    private LangTpCd langCd;
    @Column(name = "cate_title", nullable = true)
    private String cateTitle;
//    @Column(name = "cate_title_dtl", nullable = true)
//    private String cateTitleDtl;
    @Column(name = "cate_contents", nullable = true)
    private String cateContents;

    @Builder
    public TbMssnCateContents(
            Long cateId,
            LangTpCd langCd,
            String cateTitle,
            String cateContents
    ) {
        this.cateId = cateId;
        this.langCd = langCd;
        this.cateTitle = cateTitle;
        this.cateContents = cateContents;
    }

    public void update(ModCateReq req) {
        this.cateTitle = req.getCateTitle();
        this.cateContents = req.getCateContents();
    }
}
