package com.hooxi.admin.db.entity.mission;

import com.hooxi.admin.constants.code.ActStatusCd;
import com.hooxi.admin.constants.codeConverter.ActStatusCdConvert;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@Table(name = "tb_act_timeline")
public class TbActTimeline {
    @Id
    @Column(name = "act_tl_id", nullable = false)
    private Long actTlId;

    @JoinColumn(name = "act_id")
    private Long actId;

    @Column(name = "mssn_id")
    private Long mssnId;

    @Column(name = "mmbr_id")
    private Long mmbrId;

    @Column(name = "review_yn")
    private String reviewYn;

    @Column(name = "start_dt")
    private LocalDateTime startDt;

    @Column(name = "confirm_dt")
    private LocalDateTime confirmDt;

    @Column(name = "act_status_cd")
    @Convert(converter = ActStatusCdConvert.class)
    private ActStatusCd actStatusCd;
}