package com.hooxi.admin.db.entity.point.type;

import com.hooxi.admin.util.LegacyCommonType;
import lombok.Getter;

@Getter
public enum TrstnTpCd implements LegacyCommonType {
    STORY("스토리","TTC001"),
    ACTION("액션","TTC002"),
    CHALLENGE("챌린지","TTC003"),
    COMMUNITY("커뮤니티","TTC004"),
    EVENT("이벤트","TTC008"),

    HOOXIMALL("후시몰","TTC009");

    private String desc;
    private String legacyCode;

    TrstnTpCd(String desc
            , String legacyCode) {
        this.desc = desc;
        this.legacyCode = legacyCode;
    }

    public static TrstnTpCd lookup(String code) {
        if( code == null ) {
            throw new IllegalArgumentException("Code Is Null");
        }
        for (TrstnTpCd element : TrstnTpCd.values()) {
            if (element.legacyCode.equalsIgnoreCase(code)) {
                return element;
            }
        }
        return null;
    }
}
