package com.hooxi.admin.db.entity.point.model;

import com.hooxi.admin.db.entity.point.type.PointStatCd;
import com.hooxi.admin.db.entity.point.type.TrstnTpCd;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
public class PointListResponse {

//    @ApiModelProperty(name = "pointId")
    private Long pointId;

//    @ApiModelProperty(name = "point")
    private Long point;

//    @ApiModelProperty(name = "pointStatCd")
    private PointStatCd pointStateCd;
//    @ApiModelProperty(name = "pointStatCdDesc")
    private String pointStateCdDesc;

    public void setPointStateCd(PointStatCd pointStateCd) {
        this.pointStateCd = pointStateCd;
        this.pointStateCdDesc = pointStateCd.getDesc();
    }

//    @ApiModelProperty(name = "trstnDt")
    private LocalDateTime trstnDt;
//    @ApiModelProperty(name = "trstnTpCd")
    private TrstnTpCd trstnTpCd;
//    @ApiModelProperty(name = "trstnTpCdDesc")
    private String trstnTpCdDesc;
    public void setTrstnTpCd(TrstnTpCd trstnTpCd) {
        this.trstnTpCd = trstnTpCd;
        this.trstnTpCdDesc = trstnTpCd.getDesc();
    }


}
