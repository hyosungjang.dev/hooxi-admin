package com.hooxi.admin.db.entity.mission;

import com.hooxi.admin.constants.code.ParticptnTpCd;
import com.hooxi.admin.constants.codeConverter.ParticptnTpCdConvert;
import com.hooxi.admin.db.entity.mission.id.TbMssnChallengeId;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Table(name = "tb_mssn_challenge")
@Entity
@Getter
@Setter
@NoArgsConstructor
@IdClass(TbMssnChallengeId.class)
public class TbMssnChallenge {

    @Id
    @Column(name = "challenge_id", nullable = true)
    @GeneratedValue(generator = "tb_mssn_challenge_id_seq")
    private Long challengeId;
    @Id
    @Column(name = "mssn_id", nullable = true)
    private Long mssnId;
    @Column(name = "range_yn", nullable = true)
    private String rangeYn;
    @Column(name = "particptn_tp_cd", nullable = true)
    @Convert(converter = ParticptnTpCdConvert.class)
    private ParticptnTpCd particptnTpCd;
    @Column(name = "use_yn", nullable = true)
    private String useYn;
    @Column(name = "start_dt", nullable = true)
    private LocalDateTime startDt;
    @Column(name = "end_dt", nullable = true)
    private LocalDateTime endDt;
    @Column(name = "act_dt", nullable = true)
    private LocalDateTime actDt;
    @Column(name = "act_area", nullable = true)
    private String actArea;
    @Column(name = "method", nullable = true)
    private String method;

    @Builder
    public TbMssnChallenge(
            Long challengeId,
            Long mssnId, String rangeYn,
            ParticptnTpCd particptnTpCd,
            String useYn,
            LocalDateTime startDt,
            LocalDateTime endDt,
            LocalDateTime actDt,
            String actArea,
            String method
    ) {
        this.challengeId = challengeId;
        this.mssnId = mssnId;
        this.rangeYn = rangeYn;
        this.particptnTpCd = particptnTpCd;
        this.useYn = useYn;
        this.startDt = startDt;
        this.endDt = endDt;
        this.actDt = actDt;
        this.actArea = actArea;
        this.method = method;
    }


}
