package com.hooxi.admin.db.entity.member;

import com.hooxi.admin.constants.code.SnsTpCd;
import com.hooxi.admin.constants.codeConverter.GradTpCdConvert;
import com.hooxi.admin.constants.codeConverter.SnsTpCdConvert;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Table(name = "tb_mmbrshp_sns_link")
@Entity
@Getter
@Setter
@NoArgsConstructor
public class TbMmbrshpSnsLink {
    @Id
    @Column(name = "mmbrshp_id", nullable = false)
    private Long mmbrshpId;

    @Column(name = "sort")
    private Integer sort;

    @Column(name = "sns_link", length = 200)
    private String snsLink;

    @Column(name = "use_yn", length = 1)
    private String useYn;

    @Column(name = "sns_cd", length = 10)
    @Convert(converter = SnsTpCdConvert.class)
    private SnsTpCd snsCd;

    @Column(name = "sns_link_id", length = 10)
    private String snsLinkId;

    @Builder
    public TbMmbrshpSnsLink(Long mmbrshpId, Integer sort, String snsLink, String useYn, SnsTpCd snsCd, String snsLinkId) {
        this.mmbrshpId = mmbrshpId;
        this.sort = sort;
        this.snsLink = snsLink;
        this.useYn = useYn;
        this.snsCd = snsCd;
        this.snsLinkId = snsLinkId;
    }
}