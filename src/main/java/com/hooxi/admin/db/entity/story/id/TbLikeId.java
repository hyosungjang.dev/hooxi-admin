package com.hooxi.admin.db.entity.story.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class TbLikeId implements Serializable {
    private Long storyId;
    private Long mmbrId;
}
