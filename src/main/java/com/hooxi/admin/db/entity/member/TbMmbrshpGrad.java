package com.hooxi.admin.db.entity.member;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "tb_mmbrshp_grad")
@Entity
@NoArgsConstructor
@Getter
@Setter
public class TbMmbrshpGrad {
    @Id
    @Column(name = "grad_id", nullable = true)
    private Long gradId;
    @Column(name = "grad_cd", nullable = true)
    private String gradCd;
    @Column(name = "min_subscrt", nullable = true)
    private Long minSubscrt;
    @Column(name = "grad_nm", nullable = true)
    private String gradNm;
    @Column(name = "grad_img", nullable = true)
    private String gradImg;
    @Column(name = "grad", nullable = true)
    private Long grad;

    @Builder
    public TbMmbrshpGrad(Long gradId, String gradCd, Long minSubscrt, String gradNm, String gradImg, Long grad) {
        this.gradId = gradId;
        this.gradCd = gradCd;
        this.minSubscrt = minSubscrt;
        this.gradNm = gradNm;
        this.gradImg = gradImg;
        this.grad = grad;
    }
}