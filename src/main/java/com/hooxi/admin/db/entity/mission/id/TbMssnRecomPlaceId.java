package com.hooxi.admin.db.entity.mission.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class TbMssnRecomPlaceId implements Serializable {
    private Long recomId;
    private String placeCd;
}
