package com.hooxi.admin.db.entity.member.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class TbMmbrshpId implements Serializable {
    private Long mmbrshpId;
    private Long mmbrId;
}
