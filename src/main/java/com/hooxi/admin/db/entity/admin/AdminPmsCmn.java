package com.hooxi.admin.db.entity.admin;

import com.hooxi.admin.db.entity.admin.id.AdminPmsCmnId;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Table(name = "tb_admin_pms_cmn")
@Entity
@Getter
@Setter
@NoArgsConstructor
@IdClass(AdminPmsCmnId.class)
public class AdminPmsCmn {

    @Id
    @Column(name = "admin_pms_cmn_id", nullable = true)
    private Long adminPmsCmnId;
    @Id
    @Column(name = "admin_pms_pmn_id", nullable = true)
    private Long adminPmsPmnId;
    @Id
    @Column(name = "admin_pms_id", nullable = true)
    private Long adminPmsId;
    @Column(name = "admin_cmn_nm", nullable = true)
    private String adminCmnNm;
    @Column(name = "admin_cmn_c", nullable = true)
    private String adminCmnC;
    @Column(name = "admin_cmn_r", nullable = true)
    private String adminCmnR;
    @Column(name = "admin_cmn_u", nullable = true)
    private String adminCmnU;
    @Column(name = "admin_cmn_d", nullable = true)
    private String adminCmnD;
    @Column(name = "reg_id", nullable = true)
    private Long regId;
    @Column(name = "reg_dttm", nullable = true)
    private LocalDateTime regDttm;
    @Column(name = "mod_id", nullable = true)
    private Long modId;
    @Column(name = "mod_dttm", nullable = true)
    private LocalDateTime modDttm;

    @Builder
    public AdminPmsCmn(Long adminPmsCmnId, Long adminPmsPmnId, Long adminPmsId, String adminCmnNm, String adminCmnC, String adminCmnR, String adminCmnU, String adminCmnD, Long regId, LocalDateTime regDttm, Long modId, LocalDateTime modDttm) {
        this.adminPmsCmnId = adminPmsCmnId;
        this.adminPmsPmnId = adminPmsPmnId;
        this.adminPmsId = adminPmsId;
        this.adminCmnNm = adminCmnNm;
        this.adminCmnC = adminCmnC;
        this.adminCmnR = adminCmnR;
        this.adminCmnU = adminCmnU;
        this.adminCmnD = adminCmnD;
        this.regId = regId;
        this.regDttm = regDttm;
        this.modId = modId;
        this.modDttm = modDttm;
    }
}
