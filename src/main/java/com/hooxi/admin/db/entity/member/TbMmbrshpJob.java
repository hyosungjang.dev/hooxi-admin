package com.hooxi.admin.db.entity.member;

import com.hooxi.admin.constants.code.JobKywdTpCd;
import com.hooxi.admin.constants.codeConverter.GradTpCdConvert;
import com.hooxi.admin.constants.codeConverter.JobKywdTpCdConvert;
import com.hooxi.admin.db.entity.member.id.TbMmbrshpJobId;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Table(name = "tb_mmbrshp_job")
@Entity
@Getter
@Setter
@NoArgsConstructor
@IdClass(TbMmbrshpJobId.class)
public class TbMmbrshpJob {
    @Id
    @Column(name = "mmbrshp_job_id", nullable = false)
    private Long mmbrshpJobId;

    @Id
    @Column(name = "mmbrshp_id", nullable = false)
    private Long mmbrshpId;

    @Id
    @Column(name = "job_kywd_tp_cd", nullable = false)
    private String jobKywdTpCd;

//    @Id
//    @Column(name = "job_kywd_tp_cd", nullable = false)
//    @Convert(converter = JobKywdTpCdConvert.class)
//    private JobKywdTpCd jobKywdTpCd;

    @Column(name = "use_tp_cd", nullable = false)
    private String useTpCd;
}