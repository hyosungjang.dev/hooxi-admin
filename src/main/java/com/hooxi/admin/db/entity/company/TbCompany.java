package com.hooxi.admin.db.entity.company;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Table(name = "tb_company")
@Entity
@Getter
@Setter
@NoArgsConstructor
public class TbCompany {
    @Id
    @GeneratedValue(generator = "tb_mmbr_id_seq")
    @Column(name = "company_id", nullable = false)
    private Long companyId;

    @Column(name = "company_name", length = 200)
    private String companyName;

    @Column(name = "contents", length = 2000)
    private String contents;

    @Column(name = "img_url", length = 200)
    private String imgUrl;

    @Column(name = "reg_id")
    private Long regId;

    @Column(name = "reg_dttm")
    private LocalDateTime regDttm;

    @Column(name = "mod_id")
    private Long modId;

    @Column(name = "mod_dttm")
    private LocalDateTime modDttm;

    @Builder
    public TbCompany(Long companyId, String companyName, String contents, String imgUrl, Long regId, LocalDateTime regDttm, Long modId, LocalDateTime modDttm) {
        this.companyId = companyId;
        this.companyName = companyName;
        this.contents = contents;
        this.imgUrl = imgUrl;
        this.regId = regId;
        this.regDttm = regDttm;
        this.modId = modId;
        this.modDttm = modDttm;
    }


}