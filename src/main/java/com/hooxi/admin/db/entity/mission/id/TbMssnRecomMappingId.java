package com.hooxi.admin.db.entity.mission.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class TbMssnRecomMappingId implements Serializable {

    private Long recomId;
    private Long mssnId;

}
