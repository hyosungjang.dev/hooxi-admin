package com.hooxi.admin.db.entity.mission.id;

import com.hooxi.admin.constants.code.ActionPlaceTpCd;
import lombok.Data;

import java.io.Serializable;

@Data
public class TbMssnCatePlaceId implements Serializable {
    private Long cateId;
    private ActionPlaceTpCd placeCd;
}
