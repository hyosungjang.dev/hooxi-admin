package com.hooxi.admin.db.entity.exp.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class TbMmbrExpHistoryId implements Serializable {

    private Long historyId;
    private Long mmbrId;

}
