package com.hooxi.admin.db.entity.mission.id;

import com.hooxi.admin.constants.code.LangTpCd;
import lombok.Data;

import java.io.Serializable;

@Data
public class TbMssnCateContentsId implements Serializable {

    private Long cateId;
    private LangTpCd langCd;

}
