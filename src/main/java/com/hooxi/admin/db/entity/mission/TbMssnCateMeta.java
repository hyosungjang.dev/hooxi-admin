package com.hooxi.admin.db.entity.mission;

import com.hooxi.admin.constants.code.ActionPurposeTpCd;
import com.hooxi.admin.constants.codeConverter.ActionPurposeTpCdConvert;
import com.hooxi.admin.db.entity.mission.id.TbMssnCateMetaId;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Table(name = "tb_mssn_cate_meta")
@Entity
@Getter
@Setter
@NoArgsConstructor
@IdClass(TbMssnCateMetaId.class)
public class TbMssnCateMeta {

    @Id
    @Column(name = "cate_id", nullable = true)
    private Long cateId;

    @Column(name = "prps_cd", nullable = true)
    @Convert(converter = ActionPurposeTpCdConvert.class)
    private ActionPurposeTpCd prpsCd;

    @Builder
    public TbMssnCateMeta(Long cateId, ActionPurposeTpCd prpsCd) {
        this.cateId = cateId;
        this.prpsCd = prpsCd;
    }

}
