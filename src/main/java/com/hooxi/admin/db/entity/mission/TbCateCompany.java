package com.hooxi.admin.db.entity.mission;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Table(name = "tb_cate_company")
@Entity
@Getter
@Setter
@NoArgsConstructor
public class TbCateCompany {

    @Id
    @Column(name = "company_id", nullable = true)
    private Long companyId;
    @Column(name = "title", nullable = true)
    private String title;
    @Column(name = "contents", nullable = true)
    private String contents;
    @Column(name = "img_url", nullable = true)
    private String imgUrl;
    @Column(name = "reg_id", nullable = true)
    private Long regId;
    @Column(name = "reg_dttm", nullable = true)
    private LocalDateTime regDttm;
    @Column(name = "mod_id", nullable = true)
    private Long modId;
    @Column(name = "mod_dttm", nullable = true)
    private LocalDateTime modDttm;

    @Builder
    public TbCateCompany(Long companyId, String title, String contents, String imgUrl, Long regId, LocalDateTime regDttm, Long modId, LocalDateTime modDttm) {
        this.companyId = companyId;
        this.title = title;
        this.contents = contents;
        this.imgUrl = imgUrl;
        this.regId = regId;
        this.regDttm = regDttm;
        this.modId = modId;
        this.modDttm = modDttm;
    }
}
