package com.hooxi.admin.db.entity.code.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class CmnCodeId implements Serializable {
    private Long id;
    private String cd;
}
