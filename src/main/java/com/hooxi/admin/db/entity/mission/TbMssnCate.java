package com.hooxi.admin.db.entity.mission;


import com.hooxi.admin.biz.v1.action.model.ActionMissionCateVO;
import com.hooxi.admin.biz.v1.action.model.ActionMissionVO;
import com.hooxi.admin.biz.v1.action.model.req.ModCateReq;
import com.hooxi.admin.constants.code.TpcTpCd;
import com.hooxi.admin.constants.codeConverter.TpcTpCdConvert;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Table(name = "tb_mssn_cate")
@Entity
@Getter
@Setter
@NoArgsConstructor
public class TbMssnCate {

    @Id
    @GeneratedValue(generator = "tb_mssn_id_seq")
    @Column(name = "cate_id", nullable = true)
    private Long cateId;
    @Column(name = "tpc_tp_cd", nullable = true)
    @Convert(converter = TpcTpCdConvert.class)
    private TpcTpCd tpcTpCd;
    @Column(name = "use_yn", nullable = true)
    private String useYn;
    @Column(name = "img_url", nullable = true)
    private String imgUrl;
    @Column(name = "reg_id", nullable = true)
    private Long regId;
    @Column(name = "reg_dt", nullable = true)
    private LocalDateTime regDt;
    @Column(name = "mod_id", nullable = true)
    private Long modId;
    @Column(name = "mod_dt", nullable = true)
    private LocalDateTime modDt;
    @Column(name = "company_id", nullable = true)
    private Long companyId;
    @Column(name = "del_yn", nullable = true)
    private String delYn;

    @Builder
    public TbMssnCate(
            TpcTpCd tpcTpCd,
            String useYn,
            String imgUrl,
            Long regId,
            LocalDateTime regDt,
            Long modId,
            LocalDateTime modDt,
            Long companyId
    ) {
        this.tpcTpCd = tpcTpCd;
        this.useYn = useYn;
        this.imgUrl = imgUrl;
        this.regId = regId;
        this.regDt = regDt;
        this.modId = modId;
        this.modDt = modDt;
        this.companyId = companyId;
    }

    public void update(ActionMissionCateVO request) {
        if(request.getCateId() != null) {
            this.cateId = request.getCateId();
        }

        this.tpcTpCd = request.getTpcTpCd();
        this.imgUrl = request.getImgUrl();
        this.companyId = request.getCompanyId();
        this.useYn = request.getUesYn();
        this.regId = request.getRegId();
        this.regDt = request.getRegDttm();
        this.modId = request.getModId();
        this.modDt = LocalDateTime.now();
    }

    public void updateCate(ModCateReq req, String imgUrl) {
        this.imgUrl = imgUrl;
        this.useYn = req.getUseYn();
        this.modDt = LocalDateTime.now();
        this.modId = 3L;
    }

}
