package com.hooxi.admin.db.entity.user;

import javax.persistence.*;
import java.time.Instant;

@Table(name = "tb_claus_hist")
@Entity
public class TbClausHist {
    @Id
    @Column(name = "claus_agr_id", nullable = false)
    private Long id;

    @Column(name = "claus_agr_yn", length = 1)
    private String clausAgrYn;

    @Column(name = "frst_rgst_dttm")
    private Instant frstRgstDttm;

    @Column(name = "fnl_mod_dttm")
    private Instant fnlModDttm;

    @JoinColumns({
            @JoinColumn(name = "mmbr_id", referencedColumnName = "mmbr_id"),
            @JoinColumn(name = "claus_id", referencedColumnName = "claus_id")
    })
    @ManyToOne
    private TbClausAgr tbClausAgr;

    public TbClausAgr getTbClausAgr() {
        return tbClausAgr;
    }

    public void setTbClausAgr(TbClausAgr tbClausAgr) {
        this.tbClausAgr = tbClausAgr;
    }

    public Instant getFnlModDttm() {
        return fnlModDttm;
    }

    public void setFnlModDttm(Instant fnlModDttm) {
        this.fnlModDttm = fnlModDttm;
    }

    public Instant getFrstRgstDttm() {
        return frstRgstDttm;
    }

    public void setFrstRgstDttm(Instant frstRgstDttm) {
        this.frstRgstDttm = frstRgstDttm;
    }

    public String getClausAgrYn() {
        return clausAgrYn;
    }

    public void setClausAgrYn(String clausAgrYn) {
        this.clausAgrYn = clausAgrYn;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}