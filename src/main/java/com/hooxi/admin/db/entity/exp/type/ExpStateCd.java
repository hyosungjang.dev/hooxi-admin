package com.hooxi.admin.db.entity.exp.type;

import com.hooxi.admin.util.LegacyCommonType;
import lombok.Getter;

@Getter
public enum ExpStateCd implements LegacyCommonType {
    GET("경험치획득","ESC001"),
    CANCEL("경험치취소","ESC002"),
    RETRIEVE("경험치회수","ESC003");

    private String desc;
    private String legacyCode;

    ExpStateCd(String desc
            , String legacyCode) {
        this.desc = desc;
        this.legacyCode = legacyCode;
    }

}
