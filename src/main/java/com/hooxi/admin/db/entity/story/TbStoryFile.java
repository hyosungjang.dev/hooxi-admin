package com.hooxi.admin.db.entity.story;

import com.hooxi.admin.db.entity.story.id.TbStoryFileId;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Getter
@Setter
@IdClass(TbStoryFileId.class)
@Table(name = "tb_story_file")
public class TbStoryFile {

    @Id
    @Column(name = "file_seq")
    private Long fileSeq;

    @Id
    @Column(name = "story_id")
    private Long storyId;

    @Column(name = "file_name", nullable = false, length = 200)
    private String fileName;

    @Column(name = "file_path", nullable = false, length = 200)
    private String filePath;

    @Column(name = "file_url", length = 500)
    private String fileUrl;

    @Column(name = "file_size")
    private Long fileSize;

    @Column(name = "reg_id")
    private Long regId;

    @Column(name = "reg_dt")
    private Instant regDt;

    @Column(name = "mod_id")
    private Long modId;

    @Column(name = "mod_dt")
    private Instant modDt;

}