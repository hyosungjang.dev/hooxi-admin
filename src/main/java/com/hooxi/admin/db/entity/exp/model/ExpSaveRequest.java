package com.hooxi.admin.db.entity.exp.model;

import com.hooxi.admin.db.entity.exp.TbMmbrExp;
import com.hooxi.admin.db.entity.point.type.TrstnDtlTpCd;
import com.hooxi.admin.db.entity.point.type.TrstnTpCd;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor
public class ExpSaveRequest {

//    @ApiModelProperty(name = "mmbrId", example = "1", required = true)
    @NotNull(message = "User Id Is Not Null")
    private Long mmbrId;

//    @ApiModelProperty(name = "expAcmltId", example = "1", required = true, notes = "경험치 적립 유형 id")
    private Long expAcmltId;

//    @ApiModelProperty(name = "expAcmltTpCd", example = "STORY,ACTION...", required = true, notes = "경험치 획등 방법")
    private TrstnTpCd expAcmltTpCd;

//    @ApiModelProperty(name = "expAcmltDtlTpCd", example = "STORY,ACTION...", required = true, notes = "경험치 획등 방법")
    private TrstnDtlTpCd expAcmltDtlTpCd;

//    @ApiModelProperty(name = "exp", example = "100", required = true, notes = "경험치")
    private Long exp;


    @Builder
    ExpSaveRequest(Long mmbrId, Long expAcmltId, TrstnTpCd expAcmltTpCd, TrstnDtlTpCd expAcmltDtlTpCd, Long exp) {
        this.mmbrId = mmbrId;
        this.expAcmltId = expAcmltId;
        this.expAcmltTpCd = expAcmltTpCd;
        this.expAcmltDtlTpCd = expAcmltDtlTpCd;
        this.exp = exp;
    }


    // lvlCd, title 코드로 관리가 되어야 한다.
    public TbMmbrExp toSaveEntityMmbrExp(String lvlCd, String title, Long nextNcssryExp ) {
        return TbMmbrExp.builder()
                .mmbrId(this.getMmbrId())
                .lvlCd(lvlCd)
                .title(title)
                .nextNcssryExp(nextNcssryExp)
                .exp(this.getExp())
                .expAcmltDt(LocalDateTime.now())
                .build();
    }

//
//    public TbExp toSaveEntityExp() {
//        // 경험치 획득 엔티티 생성
//        return TbExp.builder()
//                .mmbrId(this.getMmbrId())
//                .expStateCd(ExpStateCd.GET)
//                .expAcmltId(this.getExpAcmltId())
//                .expAcmltTpCd(this.expAcmltTpCd)
//                .expAcmltDtlTpCd(this.expAcmltDtlTpCd)
//                .exp(this.getExp())
//                .expAcmltDt(LocalDateTime.now())
//                .build();
//    }


    public void updateExp(Long exp) {
        this.exp = exp;
    }


}
