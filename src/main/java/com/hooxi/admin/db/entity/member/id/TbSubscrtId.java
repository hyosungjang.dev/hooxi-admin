package com.hooxi.admin.db.entity.member.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class TbSubscrtId implements Serializable {
    private Long mmbrId;
    private Long mmbrshpId;
}
