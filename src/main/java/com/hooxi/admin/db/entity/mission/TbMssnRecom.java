package com.hooxi.admin.db.entity.mission;

import com.hooxi.admin.biz.v1.action.model.req.ModRecomReq;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Table(name = "tb_mssn_recom")
@Entity
@Getter
@Setter
@NoArgsConstructor
public class TbMssnRecom {

    @Id
    @GeneratedValue(generator = "tb_mssn_recom_id_seq")
    @Column(name = "recom_id", nullable = true)
    private Long recomId;
    @Column(name = "use_yn", nullable = true)
    private String useYn;
    @Column(name = "img_url", nullable = true)
    private String imgUrl;
    @Column(name = "reg_id", nullable = true)
    private Long regId;
    @Column(name = "reg_dt", nullable = true)
    private LocalDateTime regDt;
    @Column(name = "mod_id", nullable = true)
    private Long modId;
    @Column(name = "mod_dt", nullable = true)
    private LocalDateTime modDt;
    @Column(name = "del_yn", nullable = true)
    private String delYn;

    @Builder
    public TbMssnRecom(
            Long recomId,
            String useYn,
            String imgUrl,
            Long regId,
            LocalDateTime regDt,
            Long modId,
            LocalDateTime modDt,
            String delYn
    ) {
        this.recomId = recomId;
        this.useYn = useYn;
        this.imgUrl = imgUrl;
        this.regId = regId;
        this.regDt = regDt;
        this.modId = modId;
        this.modDt = modDt;
        this.delYn = delYn;
    }

    public void updateRecom(ModRecomReq req, String imgUrl) {
        this.imgUrl = imgUrl;
        this.useYn = req.getUseYn();
        this.modDt = LocalDateTime.now();
        this.modId = 3L;
    }

}
