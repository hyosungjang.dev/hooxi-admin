package com.hooxi.admin.db.entity.story;

import com.hooxi.admin.db.entity.story.id.TbReplyId;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Table(name = "tb_reply")
@Entity
@Getter
@Setter
@NoArgsConstructor
@IdClass(TbReplyId.class)
public class TbReply {

    @Id
    @Column(name = "reply_id ", nullable = true)
    private Long replyId;
    @Column(name = "reply_prt_id ", nullable = true)
    private Long replyPrtId;
    @Id
    @Column(name = "story_id", nullable = true)
    private Long storyId;
    @Column(name = "mmbr_id ", nullable = true)
    private Long mmbrId;
    @Column(name = "reply_ctnt", nullable = true)
    private String replyCtnt;
    @Column(name = "use_yn ", nullable = true)
    private String useYn;
    @Column(name = "rprt_yn ", nullable = true)
    private String rprtYn;
    @Column(name = "reg_id ", nullable = true)
    private Long regId;
    @Column(name = "reg_dt ", nullable = true)
    private LocalDateTime regDt;
    @Column(name = "mod_id ", nullable = true)
    private Long modId;
    @Column(name = "mod_dt", nullable = true)
    private LocalDateTime modDt;
    @Column(name = "dlt_tp_cd", nullable = true)
    private String dltTpCd;


    @Builder
    public TbReply(Long replyId, Long replyPrtId, Long storyId, Long mmbrId, String replyCtnt, String useYn, String rprtYn, Long regId, LocalDateTime regDt, Long modId, LocalDateTime modDt, String dltTpCd) {
        this.replyId = replyId;
        this.replyPrtId = replyPrtId;
        this.storyId = storyId;
        this.mmbrId = mmbrId;
        this.replyCtnt = replyCtnt;
        this.useYn = useYn;
        this.rprtYn = rprtYn;
        this.regId = regId;
        this.regDt = regDt;
        this.modId = modId;
        this.modDt = modDt;
        this.dltTpCd = dltTpCd;
    }



}
