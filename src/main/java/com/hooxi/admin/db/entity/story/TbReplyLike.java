package com.hooxi.admin.db.entity.story;

import com.hooxi.admin.db.entity.story.id.TbReplyLikeId;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Table(name = "tb_reply_like")
@Entity
@Getter
@Setter
@NoArgsConstructor
@IdClass(TbReplyLikeId.class)
public class TbReplyLike {

    @Id
    @Column(name = "reply_id")
    private Long replyId;

    @Id
    @Column(name = "mmbr_id")
    private Long mmbrId;

    @Column(name = "use_yn")
    private String useYn;

}