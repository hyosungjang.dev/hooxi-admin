package com.hooxi.admin.db.entity.mission.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class TbMssnPlaceId implements Serializable {

    private Long mssnId;
    private String placeCd;

}
