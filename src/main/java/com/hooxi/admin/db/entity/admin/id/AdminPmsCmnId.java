package com.hooxi.admin.db.entity.admin.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class AdminPmsCmnId implements Serializable {

    private Long adminPmsCmnId;
    private Long adminPmsPmnId;

}
