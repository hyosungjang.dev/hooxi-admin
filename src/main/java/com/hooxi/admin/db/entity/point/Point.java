package com.hooxi.admin.db.entity.point;

import com.hooxi.admin.db.entity.point.convert.PointStatCdConvert;
import com.hooxi.admin.db.entity.point.convert.TrstnDtlTpCdConvert;
import com.hooxi.admin.db.entity.point.convert.TrstnTpCdConvert;
import com.hooxi.admin.db.entity.point.id.PointId;
import com.hooxi.admin.db.entity.point.type.PointStatCd;
import com.hooxi.admin.db.entity.point.type.TrstnDtlTpCd;
import com.hooxi.admin.db.entity.point.type.TrstnTpCd;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Entity
@Getter
@NoArgsConstructor
@IdClass(PointId.class)
@Table(name = "tb_point")
public class Point {

    @Id
    @GeneratedValue(generator="tb_point_point_id_seq")
    @Column(name = "point_id", nullable = false)
    private Long pointId;
    @Id
    @Column(name = "mmbr_id", nullable = false)
    private Long mmbrId;

    @Column(name = "point_state_cd", nullable = false, length = 6)
    @Convert(converter = PointStatCdConvert.class)
    private PointStatCd pointStateCd;

    @Column(name = "trstn_id", nullable = false)
    private Long trstnId;

    @Column(name = "trstn_tp_cd", nullable = false, length = 6)
    @Convert(converter = TrstnTpCdConvert.class)
    private TrstnTpCd trstnTpCd;

    @Column(name = "trstn_dtl_tp_cd", nullable = false, length = 6)
    @Convert(converter = TrstnDtlTpCdConvert.class)
    private TrstnDtlTpCd trstnDtlTpCd;

    @Column(name = "point", nullable = false)
    private Long point;

    @Column(name = "org_point_id")
    private Long orgPointId;


    @Column(name = "trstn_dt", nullable = false)
    @CreatedDate
    private LocalDateTime trstnDt;

    @Column(name = "reg_id", nullable = true)
    private Long regId;
    @Column(name = "reg_dt", nullable = true)
    private LocalDateTime regDt;
    @Column(name = "mod_id", nullable = true)
    private Long modId;
    @Column(name = "mod_dt", nullable = true)
    private LocalDateTime modDt;

    @Builder
    public Point(Long pointId, Long mmbrId, PointStatCd pointStateCd, Long trstnId, TrstnTpCd trstnTpCd, TrstnDtlTpCd trstnDtlTpCd, Long point, Long orgPointId, LocalDateTime trstnDt, Long regId, LocalDateTime regDt, Long modId, LocalDateTime modDt) {
        this.pointId = pointId;
        this.mmbrId = mmbrId;
        this.pointStateCd = pointStateCd;
        this.trstnId = trstnId;
        this.trstnTpCd = trstnTpCd;
        this.trstnDtlTpCd = trstnDtlTpCd;
        this.point = point;
        this.orgPointId = orgPointId;
        this.trstnDt = trstnDt;
        this.regId = regId;
        this.regDt = regDt;
        this.modId = modId;
        this.modDt = modDt;
    }



    // 포인트 저장할 때 point_id 시퀀스가 생성 되면서 저장
    // point_id: 포인트 기본키
    // org_point_id: 포인트의 이력을 찾기 위한 값
    //  - 적립취소, 사용취소를 하게 되면 명세에서 원본이 되는 값을 저장
    public void saveOrgPointId() {
        this.orgPointId = this.pointId;
    }



    public Point toCancelPointEntity(PointStatCd pointStateCd, Long reg) {
        return Point.builder()
                .mmbrId(this.getMmbrId())
                .pointStateCd(pointStateCd)
                .trstnId(this.getTrstnId())
                .trstnTpCd(this.getTrstnTpCd())
                .trstnDtlTpCd(this.trstnDtlTpCd)
                .point(this.getPoint() * -1)
                .trstnDt(LocalDateTime.now())
                .orgPointId(this.pointId)
                .regId(reg)
                .regDt(LocalDateTime.now())
                .modId(reg)
                .modDt(LocalDateTime.now())
                .build();
    }

    /**
     * 포인트 상세 저장을 위한 데이터 값 셋팅
     * TODO: 만료 일자는 정책을 따라가기 때문에 DB에서 관리하여야 함. 현재는 30일로 고정
     * @return
     */
    public PointDtl toSavePointDtlEntity(Long reg) {
        LocalDateTime expire = LocalDate.now().plusDays(30).atTime(LocalTime.MIDNIGHT);
        return PointDtl.builder()
                .pointId(this.getPointId())
                .mmbrId(this.getMmbrId())
                .pointStateCd(this.getPointStateCd())
                .trstnId(this.getTrstnId())
                .trstnTpCd(this.getTrstnTpCd())
                .trstnDtlTpCd(this.getTrstnDtlTpCd())
                .point(this.getPoint())
                .dtlCnclPointId(null)
                .trstnDt(this.trstnDt)
                .expireDt(expire)
                .regId(reg)
                .regDt(LocalDateTime.now())
                .modId(reg)
                .modDt(LocalDateTime.now())
                .build();
    }


    /**
     * 포인트 사용 상세
     * @param dtlOrgPointId: 포인트 명세에서 생성 된 pointId
     * @param usePoint: 사용 포인트
     * @param orgExpireDt: 적립된 포인트의 만료일자
     * @return
     */
    public PointDtl toUsePointDtlEntity(Long dtlOrgPointId, Long usePoint, LocalDateTime orgExpireDt) {
        return PointDtl.builder()
                .pointId(this.getPointId())
                .dtlOrgPointId(dtlOrgPointId)
                .point(usePoint)
                .mmbrId(this.getMmbrId())
                .pointStateCd(this.getPointStateCd())
                .trstnId(this.getTrstnId())
                .trstnTpCd(this.getTrstnTpCd())
                .trstnDtlTpCd(this.getTrstnDtlTpCd())
                .dtlCnclPointId(null)
                .trstnDt(this.trstnDt)
                .expireDt(orgExpireDt)
                .build();
    }


    public PointDtl toCancelSavePointDtlEntity(Long dtlOrgPointId,LocalDateTime expireDt) {
        return PointDtl.builder()
                .pointId(this.getPointId())
                .mmbrId(this.getMmbrId())
                .pointStateCd(this.getPointStateCd())
                .trstnId(this.getTrstnId())
                .trstnTpCd(this.getTrstnTpCd())
                .trstnDtlTpCd(this.getTrstnDtlTpCd())
                .point(this.getPoint())
                .dtlOrgPointId(dtlOrgPointId)
                .dtlCnclPointId(null)
                .trstnDt(this.trstnDt)
                .expireDt(expireDt)
                .build();
    }

    public PointDtl toCancelUsePointDtlEntity(Long dtlOrgPointId, Long usePoint, LocalDateTime expireDt) {
        return PointDtl.builder()
                .pointId(this.getPointId())
                .mmbrId(this.getMmbrId())
                .pointStateCd(this.getPointStateCd())
                .trstnId(this.getTrstnId())
                .trstnTpCd(this.getTrstnTpCd())
                .trstnDtlTpCd(this.getTrstnDtlTpCd())
                .point(usePoint * -1)
                .dtlOrgPointId(dtlOrgPointId)
                .dtlCnclPointId(null)
                .trstnDt(this.trstnDt)
                .expireDt(expireDt)
                .build();
    }

    // 적립
    public void updateStoryPointState() {
        this.pointStateCd = PointStatCd.ACCUMULATE;
        this.trstnDt = LocalDateTime.now();
    }
}
