package com.hooxi.admin.db.entity.exp.convert;

import com.hooxi.admin.util.AbstractLegacyEnumAttributeConverter;
import com.hooxi.admin.db.entity.exp.type.ExpAcmltTpCd;

import javax.persistence.Converter;

@Converter
public class ExpAclmltTpCdConvert extends AbstractLegacyEnumAttributeConverter<ExpAcmltTpCd> {
	public static final String ENUM_NAME = "경험치적립유형코드";

	public ExpAclmltTpCdConvert() {
		super(ExpAcmltTpCd.class,true, ENUM_NAME);
	}
}
