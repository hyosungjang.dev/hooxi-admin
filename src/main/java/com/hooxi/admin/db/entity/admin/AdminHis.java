package com.hooxi.admin.db.entity.admin;

import com.hooxi.admin.db.entity.admin.id.AdminHisId;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Table(name = "tb_admin_his")
@Entity
@Getter
@Setter
@NoArgsConstructor
@IdClass(AdminHisId.class)
public class AdminHis {

    @Id
    @Column(name = "admin_id", nullable = true)
    private Long adminId;
    @Id
    @Column(name = "admin_login_dttm", nullable = true)
    private LocalDateTime adminLoginDttm;
    @Column(name = "admin_login_rslt_cd", nullable = true)
    private String adminLoginRsltCd;

    @Builder
    public AdminHis(Long adminId, LocalDateTime adminLoginDttm, String adminLoginRsltCd) {
        this.adminId = adminId;
        this.adminLoginDttm = adminLoginDttm;
        this.adminLoginRsltCd = adminLoginRsltCd;
    }

}
