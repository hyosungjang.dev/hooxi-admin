package com.hooxi.admin.db.entity.mission.id;

import com.hooxi.admin.constants.code.LangTpCd;
import lombok.Data;

import java.io.Serializable;

@Data
public class TbMssnContentsId implements Serializable {

    private Long mssnId;
    private LangTpCd langCd;

}
