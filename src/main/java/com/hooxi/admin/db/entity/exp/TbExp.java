package com.hooxi.admin.db.entity.exp;

import com.hooxi.admin.db.entity.exp.id.TbExpId;
import com.hooxi.admin.db.entity.point.type.TrstnDtlTpCd;
import com.hooxi.admin.db.entity.point.type.TrstnTpCd;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Table(name = "tb_exp")
@Entity
@Getter
@Setter
@NoArgsConstructor
@IdClass(TbExpId.class)
public class TbExp {

    @Id
    @GeneratedValue(generator = "tb_exp_exp_id_seq")
    @Column(name = "exp_id", nullable = true)
    private Long expId;
    @Id
    @Column(name = "mmbr_id", nullable = true)
    private Long mmbrId;
    @Column(name = "exp_state_cd", nullable = true)
    private String expStateCd;
    @Column(name = "exp_acmlt_id", nullable = true)
    private Long expAcmltId;
    @Column(name = "exp_acmlt_tp_cd", nullable = true)
    private String expAcmltTpCd;

    @Column(name = "exp_acmlt_dtl_tp_cd", nullable = true)
    private String expAcmltDtlTpCd;

    @Column(name = "exp", nullable = true)
    private Long exp;
    @Column(name = "exp_acmlt_dt", nullable = true)
    private LocalDateTime expAcmltDt;
    @Column(name = "reg_dt", nullable = true)
    private LocalDateTime regDt;
    @Column(name = "mod_id", nullable = true)
    private Long modId;
    @Column(name = "mod_dt", nullable = true)
    private LocalDateTime modDt;
    @Column(name = "reg_id", nullable = true)
    private Long regId;

    @Builder
    public TbExp(
            Long expId,
            Long mmbrId,
            String expStateCd,
            Long expAcmltId,
            String expAcmltTpCd,
            String expAcmltDtlTpCd,
            Long exp,
            LocalDateTime expAcmltDt,
            LocalDateTime regDt,
            Long modId,
            LocalDateTime modDt,
            Long regId
    ) {
        this.expId = expId;
        this.mmbrId = mmbrId;
        this.expStateCd = expStateCd;
        this.expAcmltId = expAcmltId;
        this.expAcmltTpCd = expAcmltTpCd;
        this.expAcmltDtlTpCd = expAcmltDtlTpCd;
        this.exp = exp;
        this.expAcmltDt = expAcmltDt;
        this.regDt = regDt;
        this.modId = modId;
        this.modDt = modDt;
        this.regId = regId;
    }

    public TbExp toSave(Long mmbrId, Long storyId, Long exp, Long regId) {
        TbExp expSava = new TbExp();
        expSava.setMmbrId(mmbrId);
        expSava.setExpStateCd("ESC001");
        expSava.setExpAcmltId(storyId);
        expSava.setExpAcmltTpCd(TrstnTpCd.STORY.getLegacyCode());
        expSava.setExpAcmltDtlTpCd(TrstnDtlTpCd.STORY_CREATE.getLegacyCode());
        expSava.setExp(exp);
        expSava.setExpAcmltDt(LocalDateTime.now());
        expSava.setRegDt(LocalDateTime.now());
        expSava.setModId(regId);
        expSava.setModDt(LocalDateTime.now());
        expSava.setRegId(regId);
        return expSava;
    }


}
