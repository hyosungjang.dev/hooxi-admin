package com.hooxi.admin.db.entity.mission;

import com.hooxi.admin.biz.v1.action.model.req.ModRecomReq;
import com.hooxi.admin.constants.code.LangTpCd;
import com.hooxi.admin.constants.codeConverter.LangTpCdConvert;
import com.hooxi.admin.db.entity.mission.id.TbMssnRecomContentsId;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Table(name = "tb_mssn_recom_contents")
@Entity
@Getter
@Setter
@NoArgsConstructor
@IdClass(TbMssnRecomContentsId.class)
public class TbMssnRecomContents {

    @Id
    @Column(name = "recom_id", nullable = true)
    private Long recomId;
    @Column(name = "lang_cd", nullable = true)
    @Convert(converter = LangTpCdConvert.class)
    private LangTpCd langCd;
    @Column(name = "recom_title", nullable = true)
    private String recomTitle;
    @Column(name = "recom_contents", nullable = true)
    private String recomContents;

    @Builder
    public TbMssnRecomContents(
            Long recomId,
            LangTpCd langCd,
            String recomTitle,
            String recomContents
    ) {
        this.recomId = recomId;
        this.langCd = langCd;
        this.recomTitle = recomTitle;
        this.recomContents = recomContents;
    }

    public void updateContents(ModRecomReq req) {
        this.recomContents = req.getRecomContents();
        this.recomTitle = req.getRecomTitle();
    }
}
