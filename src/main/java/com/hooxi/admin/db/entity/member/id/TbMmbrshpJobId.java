package com.hooxi.admin.db.entity.member.id;

import com.hooxi.admin.constants.code.JobKywdTpCd;
import com.hooxi.admin.constants.codeConverter.JobKywdTpCdConvert;
import lombok.Data;

import javax.persistence.Convert;
import java.io.Serializable;

@Data
public class TbMmbrshpJobId implements Serializable {
    private Long mmbrshpJobId;
    private Long mmbrshpId;
//    private JobKywdTpCd jobKywdTpCd;
    private String jobKywdTpCd;
}
