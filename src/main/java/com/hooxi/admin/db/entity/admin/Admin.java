package com.hooxi.admin.db.entity.admin;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Table(name = "tb_admin")
@Entity
@Getter
@Setter
@NoArgsConstructor
public class Admin {

    @Id
    @GeneratedValue(generator = "tb_admin_id_seq")
    @Column(name = "admin_id", nullable = true)
    private Long adminId;
    @Column(name = "admin_email", nullable = true)
    private String adminEmail;
    @Column(name = "admin_name", nullable = true)
    private String adminName;
    @Column(name = "admin_pwd", nullable = true)
    private String adminPwd;
    @Column(name = "admin_pms_id", nullable = true)
    private Long adminPmsId;
    @Column(name = "use_yn", nullable = true)
    private String useYn;
    @Column(name = "reg_id", nullable = true)
    private Long regId;
    @Column(name = "reg_dttm", nullable = true)
    private LocalDateTime regDttm;

    @Builder
    public Admin(Long adminId, String adminEmail, String adminName, String adminPwd, Long adminPmsId, String useYn, Long regId, LocalDateTime regDttm) {
        this.adminId = adminId;
        this.adminEmail = adminEmail;
        this.adminName = adminName;
        this.adminPwd = adminPwd;
        this.adminPmsId = adminPmsId;
        this.useYn = useYn;
        this.regId = regId;
        this.regDttm = regDttm;
    }
}
