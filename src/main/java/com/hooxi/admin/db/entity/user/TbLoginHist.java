package com.hooxi.admin.db.entity.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.Instant;
import java.time.LocalDateTime;

@Table(name = "tb_login_hist")
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TbLoginHist {
    @Id
    @Column(name = "mmbr_id", nullable = false)
    private Long mmbrId;

    @Column(name = "login_dttm")
    private LocalDateTime loginDttm;

    @Column(name = "login_rslt_cd", length = 10)
    private String loginRsltCd;

    @Column(name = "login_tp", length = 10)
    private String loginTp;

    @Column(name = "device_os", length = 10)
    private String deviceOs;

    @Column(name = "device_name", length = 10)
    private String deviceName;
}