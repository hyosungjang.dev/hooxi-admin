package com.hooxi.admin.db.entity.point.model;

import com.hooxi.admin.db.entity.point.PointDtl;
import com.hooxi.admin.db.entity.point.type.PointStatCd;
import com.hooxi.admin.db.entity.point.type.TrstnTpCd;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
public class PointDtlDTO {

    private Long pointDtlId;
    private Long pointId;
    private Long mmbrId;
    private PointStatCd pointStateCd;
    private Long trstnId;
    private TrstnTpCd trstnTpCd;
    private Long point;
    private Long orgPointId;
    private Long dtlCnclPointId;
    private LocalDateTime trstnDt;
    private LocalDateTime expireDt;

    public PointDtl toSaveEntity(Long orgPointId, LocalDateTime expireDt) {
        return PointDtl.builder()
                .pointId(this.getPointId())
                .mmbrId(this.getMmbrId())
                .pointStateCd(this.getPointStateCd())
                .trstnId(this.getTrstnId())
                .trstnTpCd(this.getTrstnTpCd())
                .point(this.getPoint())
                .dtlOrgPointId(orgPointId)
                .dtlCnclPointId(null)
                .trstnDt(this.trstnDt)
                .expireDt(expireDt)
                .build();
    }
}
