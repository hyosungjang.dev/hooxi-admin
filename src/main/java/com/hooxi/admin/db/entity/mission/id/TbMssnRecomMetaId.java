package com.hooxi.admin.db.entity.mission.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class TbMssnRecomMetaId implements Serializable {

    private Long recomId;
    private String prpsCd;

}
