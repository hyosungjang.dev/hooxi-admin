package com.hooxi.admin.db.entity.mission;

import com.hooxi.admin.biz.v1.action.model.ActionMissionVO;
import com.hooxi.admin.biz.v1.action.model.req.ModActionReq;
import com.hooxi.admin.constants.code.TpcTpCd;
import com.hooxi.admin.constants.codeConverter.TpcTpCdConvert;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Table(name = "tb_mssn")
@Entity
@Getter
@Setter
@NoArgsConstructor
@DynamicUpdate
//@IdClass(TbMssnId.class)
public class TbMssn {

    /*
    select nextval('시퀀스');
    select setval('시퀀스',(select max(admin_id) from 타겟_테이블));
     */

    @Id
    @GeneratedValue(generator = "tb_mssn_id_seq")
    @Column(name = "mssn_id", nullable = true)
    private Long mssnId;
    @Column(name = "cate_id", nullable = true)
    private Long cateId;
    @Column(name = "tpc_tp_cd", nullable = true)
    @Convert(converter = TpcTpCdConvert.class)
    private TpcTpCd tpcTpCd;
    @Column(name = "img_url", nullable = true)
    private String imgUrl;
    @Column(name = "exp", nullable = true)
    private Integer exp;
    @Column(name = "point", nullable = true)
    private Integer point;
    @Column(name = "hit", nullable = true)
    private Long hit;
    @Column(name = "use_yn", nullable = true)
    private String useYn;
    @Column(name = "reg_id", nullable = true)
    private Long regId;
    @Column(name = "reg_dt", nullable = true)
    private LocalDateTime regDt;
    @Column(name = "mod_id", nullable = true)
    private Long modId;
    @Column(name = "mod_dt", nullable = true)
    private LocalDateTime modDt;
    @Column(name = "link", nullable = true)
    private String linkUrl;

    @Builder
    public TbMssn(Long mssnId,
                  Long cateId,
                  TpcTpCd tpcTpCd,
                  String imgUrl,
                  Integer exp,
                  Integer point,
                  Long hit,
                  String useYn,
                  Long regId,
                  LocalDateTime regDt,
                  Long modId,
                  LocalDateTime modDt,
                  String linkUrl) {
        this.mssnId = mssnId;
        this.cateId = cateId;
        this.tpcTpCd = tpcTpCd;
        this.imgUrl = imgUrl;
        this.exp = exp;
        this.point = point;
        this.hit = hit;
        this.useYn = useYn;
        this.regId = regId;
        this.regDt = regDt;
        this.modId = modId;
        this.modDt = modDt;
        this.linkUrl = linkUrl;
    }

    public void update(ActionMissionVO request) {
        if(request.getMssnId() != null) {
            this.mssnId = request.getMssnId();
        }

        this.cateId = request.getMssnId();
        this.tpcTpCd = request.getTpcTpCd();
        this.imgUrl = request.getImgUrl();
//        this.exp = request.getExp();
//        this.point = request.getPoint();
        this.hit = request.getHit();
        this.useYn = request.getUesYn();
        this.regId = request.getRegId();
        this.regDt = request.getRegDt();
        this.modId = request.getModId();
        this.modDt = LocalDateTime.now();
        this.linkUrl = request.getImgUrl();
    }

    public void updateMssn(ModActionReq req, String imgUrl) {
        this.cateId = req.getCateId();
        this.exp = req.getExp();
        this.imgUrl = imgUrl;
        this.useYn =  req.getUseYn();
        this.linkUrl = req.getLink();
        this.point = req.getPoint();
        this.modId = 3L;
        this.modDt = LocalDateTime.now();
    }
}
