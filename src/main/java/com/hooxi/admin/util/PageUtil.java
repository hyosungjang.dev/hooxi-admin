package com.hooxi.admin.util;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.JsonSerializer;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PageUtil<T> extends PageImpl<T> {
//page, size, totalPage, totalSize

    private long page;
    private long size;
    private long totalPage;
    private long totalSize;

    @JsonCreator
    public PageUtil(List content, Pageable pageable, long totalCount) {
//        super(
//                content,
//                pageable,
//                totalCount
//        );
        super(content);
    }

    @JsonGetter(value = "result")
    @Override
    public List getContent() {
        return super.getContent();
    }

    @JsonGetter(value = "paging")
    public Map getPaging() {
        Map<String, Object> paging = new HashMap<>();
        paging.put("totalPages", super.getTotalPages());
        paging.put("totalElements", super.getTotalElements());
        paging.put("pageSize", super.getSize());
        paging.put("pageNumber", super.getNumber() + 1);
//        paging.put("isFirst", super.isFirst());
//        paging.put("isLast", super.isLast());
//        paging.put("sort", super.getSort());
        paging.put("isEmpty", super.isEmpty());
        return paging;
    }



//    @JsonGetter(value = "indexes")
//    public List getPaging() {
//        return indexes;
//    }
}
