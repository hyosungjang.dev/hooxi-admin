package com.hooxi.admin.util;


import com.querydsl.core.QueryResults;

import java.util.List;

public class DslResult<T> {

    private List<T> listResults;
    private QueryResults<T> queryResults;
    private T dslResult;

    public DslResult(){}
    public DslResult(List<T> listResults, QueryResults<T> queryResults) {
        this.listResults = listResults;
        this.queryResults = queryResults;
    }

    public List<T> getListResults() {
        return listResults;
    }

    public void setListResults(List<T> listResults) {
        this.listResults = listResults;
    }

    public QueryResults<T> getQueryResults() {
        return queryResults;
    }

    public void setQueryResults(QueryResults<T> queryResults) {
        this.queryResults = queryResults;
    }

    public T getDslResult() {
        return dslResult;
    }

    public void setDslResult(T dslResult) {
        this.dslResult = dslResult;
    }
}
