package com.hooxi.admin.exception;

import org.springframework.http.HttpStatus;

public enum ErrorCode {

    // sample
    INVALID_PARAMETER(HttpStatus.BAD_REQUEST.value(), null, "Invalid Request Data"),


    COUPON_EXPIRATION(410, "C001", "Coupon Was Expired"),
    COUPON_NOT_FOUND(404, "C002", "Coupon Not Found"),

    // 회원
    DUPLICATION_EMAIL(HttpStatus.OK.value(), "M101", "이미 존재하는 이메일 계정입니다."),
    DUPLICATION_NICKNAME(HttpStatus.OK.value(), "M102", "이미 존재하는 닉네임입니다."),
    NOT_FOUND_MEMBER(HttpStatus.OK.value(), "M103", "존재하지 않는 회원입니다."),
    WRONG_PASSWORD(HttpStatus.OK.value(), "M104", "잘못된 비밀번호 입니다."),
    OTHER_LOGIN_TYPE(HttpStatus.OK.value(), "M105", "다른 수단으로 가입된 계정입니다."),
    DUPLICATION_MEMBERSHIP(HttpStatus.OK.value(), "M108", "이미 작가신청이 완료된 계정입니다."),
    NOT_FOUND_MEMBERSHIP(HttpStatus.OK.value(), "M109", "작가로 등록되지 않은 계정입니다. 작가 신청 먼저 진행해 주세요."),
    PROFILE_IMG_UPDATE_ERROR(HttpStatus.INTERNAL_SERVER_ERROR.value(), "M111", "프로필 이미지 업로드에 실패했습니다. 잠시 후 다시 시도해주세요."),
    NICKNAME_UPDATE_ERROR(HttpStatus.INTERNAL_SERVER_ERROR.value(), "M112", "닉네임 변경에 실패했습니다. 잠시 후 다시 시도해주세요."),
    PWD_UPDATE_ERROR(HttpStatus.INTERNAL_SERVER_ERROR.value(), "M113", "비밀번호 변경에 실패했습니다. 잠시 후 다시 시도해주세요."),
    WITHDRAWAL_MEMBER(HttpStatus.OK.value(), "M114", "탈퇴한 계정입니다."),
    FAIL_JOIN(HttpStatus.INTERNAL_SERVER_ERROR.value(), "M115", "회원가입에 실패했습니다. 잠시 후 다시 시도해주세요."),
    NULL_LANG_CD(HttpStatus.OK.value(), "M116", "언어를 설정해주세요."),
    NOT_MEMBERSHIP(HttpStatus.OK.value(), "M117", "작가 신청을 진행중인 계정입니다. 신청이 완료된 후 진행해주세요."),

    //스토리
    STORY_SAVE_ERROR(HttpStatus.INTERNAL_SERVER_ERROR.value(), "S100", "스토리 등록에 실패했습니다. 잠시 후 다시 시도해주세요."),
    NOT_FOUND_STORY(HttpStatus.OK.value(), "S101", "스토리 정보를 찾을 수 없습니다."),
    NOT_FOUND_MMBR_ID(HttpStatus.BAD_REQUEST.value(), "S102", "회원 정보를 찾을 수 없습니다."),
    NOT_FOUND_STORY_ID(HttpStatus.BAD_REQUEST.value(), "S103", "스토리 정보를 찾을 수 없습니다."),

    NOT_FOUND_REPLY(HttpStatus.OK.value(), "S111", "댓글 정보를 찾을 수 없습니다."),
    NOT_REPORT_YOURSELF(HttpStatus.OK.value(), "S112", "본인이 작성한 글은 신고할 수 없습니다."),
    NOT_REPORT_ALREADY(HttpStatus.OK.value(), "S113", "이미 신고 접수가 되었습니다."),

    // 토큰
    TOKEN_FORBIDDEN(HttpStatus.FORBIDDEN.value(), null, "로그아웃 되었습니다. 다시 로그인해주세요."),
    TOKEN_EXPIRED(HttpStatus.UNAUTHORIZED.value(), null, "token expired"),

    // 구독
    FAIL_SUBSCRIBE_ERROR(HttpStatus.INTERNAL_SERVER_ERROR.value(), "F101", "구독 신청에 실패했습니다. 잠시 후 다시 시도해주세요."),

    //인증
    EXPIRED_VERIFY_NUMBER(HttpStatus.OK.value(), "V101", "인증 시간을 초과하였습니다. 인증번호를 다시 요청해주세요."),
    INVALID_VERIFY_NUMBER(HttpStatus.OK.value(), "V102", "인증번호가 틀렸습니다. 다시 시도해주세요."),

    //이메일 전송
    FAIL_SEND_EMAIL(HttpStatus.INTERNAL_SERVER_ERROR.value(), "E101", "이메일 전송에 실패했습니다. 잠시 후 다시 시도해주세요."),
    FAIL_SAVE_REDIS(HttpStatus.INTERNAL_SERVER_ERROR.value(), "E102", "인증 번호 발행에 실패했습니다. 잠시 후 다시 시도해주세요."),

    //액션, 챌린지
    NOT_FOUND_ACTION(HttpStatus.OK.value(), "A101", "행동 정보를 찾을 수 없습니다."),
    INVALID_TPC_TP_CD_TO_ACTION(HttpStatus.OK.value(), "A102", "잘못된 접근입니다."),
    INVALID_TPC_TP_CD_TO_CHALLENGE(HttpStatus.OK.value(), "A103", "잘못된 접근입니다."),
    ALREADY_ONGOING_ACTION(HttpStatus.OK.value(), "A104", "이미 진행중인 행동입니다."),
    NOT_FOUND_CATE(HttpStatus.OK.value(), "A105", "카테고리 정보를 찾을 수 없습니다."),
    NOT_FOUND_RECOM(HttpStatus.OK.value(), "A106", "추천 정보를 찾을 수 없습니다."),

    NOT_FOUND_INQUIRY(HttpStatus.OK.value(), "I101", "존재하지 않는 문의입니다."),

    FAIL_ERROR(HttpStatus.INTERNAL_SERVER_ERROR.value(), "FE000", "Fail SAVE TO DB"),

    FAIL_SAVE_ERROR(HttpStatus.INTERNAL_SERVER_ERROR.value(), "FE001", "저장에 실패했습니다. 잠시 후 다시 시도해주세요."),

    SYSTEM_ERROR(HttpStatus.INTERNAL_SERVER_ERROR.value(), "F999", "서버에러입니다. 잠시 후 다시 시도해주세요."),

    //common
    INVALID_LANG_TP_CD(HttpStatus.OK.value(), "C001", "언어 코드가 존재하지 않습니다."),
    CONTENTS_IS_NULL(HttpStatus.OK.value(), "C002", "컨텐츠가 비었습니다."),
    COMMON_EXCEPTION_ERROR(HttpStatus.INTERNAL_SERVER_ERROR.value(), "C999", null)


    ;

    private final String code;
    private final String message;
    private final int status;

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public int getStatus() {
        return status;
    }

    ErrorCode(final int status, final String code, final String message) {
        this.status = status;
        this.message = message;
        this.code = code;
    }
}
