package com.hooxi.admin.exception.common;

import com.hooxi.admin.exception.CustomException;
import com.hooxi.admin.exception.ErrorCode;

public class CommonCodeException extends CustomException {

    private static final long serialVersionUID = -2116671122895194101L;

    public CommonCodeException(ErrorCode e) {
        super(e);
    }

    public CommonCodeException(String errorMessage) {
        super(errorMessage);
    }

}
