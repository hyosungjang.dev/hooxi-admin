package com.hooxi.admin.db.entity.user.id;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;


/**
 * QTbLikeId is a Querydsl query type for TbLikeId
 */
@Generated("com.querydsl.codegen.DefaultEmbeddableSerializer")
public class QTbLikeId extends BeanPath<TbLikeId> {

    private static final long serialVersionUID = 1443903340L;

    public static final QTbLikeId tbLikeId = new QTbLikeId("tbLikeId");

    public final NumberPath<Long> mmbrId = createNumber("mmbrId", Long.class);

    public final NumberPath<Long> storyId = createNumber("storyId", Long.class);

    public QTbLikeId(String variable) {
        super(TbLikeId.class, forVariable(variable));
    }

    public QTbLikeId(Path<? extends TbLikeId> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTbLikeId(PathMetadata metadata) {
        super(TbLikeId.class, metadata);
    }

}

