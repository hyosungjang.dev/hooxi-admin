package com.hooxi.admin.db.entity.member;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;


/**
 * QTbMmbrshpGrad is a Querydsl query type for TbMmbrshpGrad
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QTbMmbrshpGrad extends EntityPathBase<TbMmbrshpGrad> {

    private static final long serialVersionUID = 1884640851L;

    public static final QTbMmbrshpGrad tbMmbrshpGrad = new QTbMmbrshpGrad("tbMmbrshpGrad");

    public final NumberPath<Long> grad = createNumber("grad", Long.class);

    public final StringPath gradCd = createString("gradCd");

    public final NumberPath<Long> gradId = createNumber("gradId", Long.class);

    public final StringPath gradImg = createString("gradImg");

    public final StringPath gradNm = createString("gradNm");

    public final NumberPath<Long> minSubscrt = createNumber("minSubscrt", Long.class);

    public QTbMmbrshpGrad(String variable) {
        super(TbMmbrshpGrad.class, forVariable(variable));
    }

    public QTbMmbrshpGrad(Path<? extends TbMmbrshpGrad> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTbMmbrshpGrad(PathMetadata metadata) {
        super(TbMmbrshpGrad.class, metadata);
    }

}

