package com.hooxi.admin.db.entity.mission;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;


/**
 * QTbMssnCateMeta is a Querydsl query type for TbMssnCateMeta
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QTbMssnCateMeta extends EntityPathBase<TbMssnCateMeta> {

    private static final long serialVersionUID = -2003440831L;

    public static final QTbMssnCateMeta tbMssnCateMeta = new QTbMssnCateMeta("tbMssnCateMeta");

    public final NumberPath<Long> cateId = createNumber("cateId", Long.class);

    public final EnumPath<com.hooxi.admin.constants.code.ActionPurposeTpCd> prpsCd = createEnum("prpsCd", com.hooxi.admin.constants.code.ActionPurposeTpCd.class);

    public QTbMssnCateMeta(String variable) {
        super(TbMssnCateMeta.class, forVariable(variable));
    }

    public QTbMssnCateMeta(Path<? extends TbMssnCateMeta> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTbMssnCateMeta(PathMetadata metadata) {
        super(TbMssnCateMeta.class, metadata);
    }

}

