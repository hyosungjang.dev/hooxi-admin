package com.hooxi.admin.db.entity.user;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QTbClausAgr is a Querydsl query type for TbClausAgr
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QTbClausAgr extends EntityPathBase<TbClausAgr> {

    private static final long serialVersionUID = 1410906187L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QTbClausAgr tbClausAgr = new QTbClausAgr("tbClausAgr");

    public final StringPath clausAgrYn = createString("clausAgrYn");

    public final DateTimePath<java.time.Instant> fnlModDttm = createDateTime("fnlModDttm", java.time.Instant.class);

    public final DateTimePath<java.time.Instant> frstRgstDttm = createDateTime("frstRgstDttm", java.time.Instant.class);

    public final com.hooxi.admin.db.entity.user.id.QTbClausAgrId id;

    public QTbClausAgr(String variable) {
        this(TbClausAgr.class, forVariable(variable), INITS);
    }

    public QTbClausAgr(Path<? extends TbClausAgr> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QTbClausAgr(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QTbClausAgr(PathMetadata metadata, PathInits inits) {
        this(TbClausAgr.class, metadata, inits);
    }

    public QTbClausAgr(Class<? extends TbClausAgr> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.id = inits.isInitialized("id") ? new com.hooxi.admin.db.entity.user.id.QTbClausAgrId(forProperty("id")) : null;
    }

}

