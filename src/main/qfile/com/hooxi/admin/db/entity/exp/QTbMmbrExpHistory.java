package com.hooxi.admin.db.entity.exp;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;


/**
 * QTbMmbrExpHistory is a Querydsl query type for TbMmbrExpHistory
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QTbMmbrExpHistory extends EntityPathBase<TbMmbrExpHistory> {

    private static final long serialVersionUID = -715386878L;

    public static final QTbMmbrExpHistory tbMmbrExpHistory = new QTbMmbrExpHistory("tbMmbrExpHistory");

    public final NumberPath<Integer> exp = createNumber("exp", Integer.class);

    public final DateTimePath<java.time.LocalDateTime> expAcmltDt = createDateTime("expAcmltDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> historyId = createNumber("historyId", Long.class);

    public final StringPath lvlCd = createString("lvlCd");

    public final NumberPath<Long> mmbrId = createNumber("mmbrId", Long.class);

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modId = createNumber("modId", Long.class);

    public final NumberPath<Integer> nextNcssryExp = createNumber("nextNcssryExp", Integer.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regId = createNumber("regId", Long.class);

    public final StringPath title = createString("title");

    public QTbMmbrExpHistory(String variable) {
        super(TbMmbrExpHistory.class, forVariable(variable));
    }

    public QTbMmbrExpHistory(Path<? extends TbMmbrExpHistory> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTbMmbrExpHistory(PathMetadata metadata) {
        super(TbMmbrExpHistory.class, metadata);
    }

}

