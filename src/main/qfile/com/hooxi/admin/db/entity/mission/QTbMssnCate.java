package com.hooxi.admin.db.entity.mission;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;


/**
 * QTbMssnCate is a Querydsl query type for TbMssnCate
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QTbMssnCate extends EntityPathBase<TbMssnCate> {

    private static final long serialVersionUID = -257536228L;

    public static final QTbMssnCate tbMssnCate = new QTbMssnCate("tbMssnCate");

    public final NumberPath<Long> cateId = createNumber("cateId", Long.class);

    public final NumberPath<Long> companyId = createNumber("companyId", Long.class);

    public final StringPath delYn = createString("delYn");

    public final StringPath imgUrl = createString("imgUrl");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modId = createNumber("modId", Long.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regId = createNumber("regId", Long.class);

    public final EnumPath<com.hooxi.admin.constants.code.TpcTpCd> tpcTpCd = createEnum("tpcTpCd", com.hooxi.admin.constants.code.TpcTpCd.class);

    public final StringPath useYn = createString("useYn");

    public QTbMssnCate(String variable) {
        super(TbMssnCate.class, forVariable(variable));
    }

    public QTbMssnCate(Path<? extends TbMssnCate> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTbMssnCate(PathMetadata metadata) {
        super(TbMssnCate.class, metadata);
    }

}

