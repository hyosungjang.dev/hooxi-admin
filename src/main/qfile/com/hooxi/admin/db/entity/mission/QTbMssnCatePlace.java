package com.hooxi.admin.db.entity.mission;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;


/**
 * QTbMssnCatePlace is a Querydsl query type for TbMssnCatePlace
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QTbMssnCatePlace extends EntityPathBase<TbMssnCatePlace> {

    private static final long serialVersionUID = -1974162613L;

    public static final QTbMssnCatePlace tbMssnCatePlace = new QTbMssnCatePlace("tbMssnCatePlace");

    public final NumberPath<Long> cateId = createNumber("cateId", Long.class);

    public final EnumPath<com.hooxi.admin.constants.code.ActionPlaceTpCd> placeCd = createEnum("placeCd", com.hooxi.admin.constants.code.ActionPlaceTpCd.class);

    public QTbMssnCatePlace(String variable) {
        super(TbMssnCatePlace.class, forVariable(variable));
    }

    public QTbMssnCatePlace(Path<? extends TbMssnCatePlace> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTbMssnCatePlace(PathMetadata metadata) {
        super(TbMssnCatePlace.class, metadata);
    }

}

