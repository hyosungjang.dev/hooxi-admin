package com.hooxi.admin.db.entity.admin;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;


/**
 * QTbAdminJobKywd is a Querydsl query type for TbAdminJobKywd
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QTbAdminJobKywd extends EntityPathBase<TbAdminJobKywd> {

    private static final long serialVersionUID = 16252498L;

    public static final QTbAdminJobKywd tbAdminJobKywd = new QTbAdminJobKywd("tbAdminJobKywd");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public QTbAdminJobKywd(String variable) {
        super(TbAdminJobKywd.class, forVariable(variable));
    }

    public QTbAdminJobKywd(Path<? extends TbAdminJobKywd> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTbAdminJobKywd(PathMetadata metadata) {
        super(TbAdminJobKywd.class, metadata);
    }

}

