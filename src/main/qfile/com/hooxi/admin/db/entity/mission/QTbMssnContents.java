package com.hooxi.admin.db.entity.mission;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;


/**
 * QTbMssnContents is a Querydsl query type for TbMssnContents
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QTbMssnContents extends EntityPathBase<TbMssnContents> {

    private static final long serialVersionUID = 1674477671L;

    public static final QTbMssnContents tbMssnContents = new QTbMssnContents("tbMssnContents");

    public final EnumPath<com.hooxi.admin.constants.code.LangTpCd> langCd = createEnum("langCd", com.hooxi.admin.constants.code.LangTpCd.class);

    public final StringPath mssnEffect = createString("mssnEffect");

    public final StringPath mssnEtc = createString("mssnEtc");

    public final NumberPath<Long> mssnId = createNumber("mssnId", Long.class);

    public final StringPath mssnTitle = createString("mssnTitle");

    public final StringPath mssnTitleDtl = createString("mssnTitleDtl");

    public QTbMssnContents(String variable) {
        super(TbMssnContents.class, forVariable(variable));
    }

    public QTbMssnContents(Path<? extends TbMssnContents> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTbMssnContents(PathMetadata metadata) {
        super(TbMssnContents.class, metadata);
    }

}

