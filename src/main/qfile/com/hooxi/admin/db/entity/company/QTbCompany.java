package com.hooxi.admin.db.entity.company;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;


/**
 * QTbCompany is a Querydsl query type for TbCompany
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QTbCompany extends EntityPathBase<TbCompany> {

    private static final long serialVersionUID = 1417222914L;

    public static final QTbCompany tbCompany = new QTbCompany("tbCompany");

    public final NumberPath<Long> companyId = createNumber("companyId", Long.class);

    public final StringPath companyName = createString("companyName");

    public final StringPath contents = createString("contents");

    public final StringPath imgUrl = createString("imgUrl");

    public final DateTimePath<java.time.LocalDateTime> modDttm = createDateTime("modDttm", java.time.LocalDateTime.class);

    public final NumberPath<Long> modId = createNumber("modId", Long.class);

    public final DateTimePath<java.time.LocalDateTime> regDttm = createDateTime("regDttm", java.time.LocalDateTime.class);

    public final NumberPath<Long> regId = createNumber("regId", Long.class);

    public QTbCompany(String variable) {
        super(TbCompany.class, forVariable(variable));
    }

    public QTbCompany(Path<? extends TbCompany> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTbCompany(PathMetadata metadata) {
        super(TbCompany.class, metadata);
    }

}

