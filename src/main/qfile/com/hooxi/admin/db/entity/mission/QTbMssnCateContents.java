package com.hooxi.admin.db.entity.mission;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;


/**
 * QTbMssnCateContents is a Querydsl query type for TbMssnCateContents
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QTbMssnCateContents extends EntityPathBase<TbMssnCateContents> {

    private static final long serialVersionUID = 577581654L;

    public static final QTbMssnCateContents tbMssnCateContents = new QTbMssnCateContents("tbMssnCateContents");

    public final StringPath cateContents = createString("cateContents");

    public final NumberPath<Long> cateId = createNumber("cateId", Long.class);

    public final StringPath cateTitle = createString("cateTitle");

    public final EnumPath<com.hooxi.admin.constants.code.LangTpCd> langCd = createEnum("langCd", com.hooxi.admin.constants.code.LangTpCd.class);

    public QTbMssnCateContents(String variable) {
        super(TbMssnCateContents.class, forVariable(variable));
    }

    public QTbMssnCateContents(Path<? extends TbMssnCateContents> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTbMssnCateContents(PathMetadata metadata) {
        super(TbMssnCateContents.class, metadata);
    }

}

