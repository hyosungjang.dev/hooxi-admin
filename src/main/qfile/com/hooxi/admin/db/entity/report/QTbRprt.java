package com.hooxi.admin.db.entity.report;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;


/**
 * QTbRprt is a Querydsl query type for TbRprt
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QTbRprt extends EntityPathBase<TbRprt> {

    private static final long serialVersionUID = -188685876L;

    public static final QTbRprt tbRprt = new QTbRprt("tbRprt");

    public final NumberPath<Long> mmbrId = createNumber("mmbrId", Long.class);

    public final DateTimePath<java.time.Instant> modDt = createDateTime("modDt", java.time.Instant.class);

    public final NumberPath<Long> modId = createNumber("modId", Long.class);

    public final DateTimePath<java.time.Instant> regDt = createDateTime("regDt", java.time.Instant.class);

    public final NumberPath<Long> regId = createNumber("regId", Long.class);

    public final StringPath rejectRsn = createString("rejectRsn");

    public final StringPath rprtCtgrCd = createString("rprtCtgrCd");

    public final NumberPath<Long> rprtId = createNumber("rprtId", Long.class);

    public final StringPath rprtRsn = createString("rprtRsn");

    public final StringPath rprtStatCd = createString("rprtStatCd");

    public final StringPath rprtTpCd = createString("rprtTpCd");

    public final NumberPath<Long> rprtTpId = createNumber("rprtTpId", Long.class);

    public final NumberPath<Long> rprtTrgtMmbrId = createNumber("rprtTrgtMmbrId", Long.class);

    public QTbRprt(String variable) {
        super(TbRprt.class, forVariable(variable));
    }

    public QTbRprt(Path<? extends TbRprt> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTbRprt(PathMetadata metadata) {
        super(TbRprt.class, metadata);
    }

}

