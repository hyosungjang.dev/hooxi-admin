package com.hooxi.admin.db.entity.mission;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;


/**
 * QTbMssn is a Querydsl query type for TbMssn
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QTbMssn extends EntityPathBase<TbMssn> {

    private static final long serialVersionUID = -371507667L;

    public static final QTbMssn tbMssn = new QTbMssn("tbMssn");

    public final NumberPath<Long> cateId = createNumber("cateId", Long.class);

    public final NumberPath<Integer> exp = createNumber("exp", Integer.class);

    public final NumberPath<Long> hit = createNumber("hit", Long.class);

    public final StringPath imgUrl = createString("imgUrl");

    public final StringPath linkUrl = createString("linkUrl");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modId = createNumber("modId", Long.class);

    public final NumberPath<Long> mssnId = createNumber("mssnId", Long.class);

    public final NumberPath<Integer> point = createNumber("point", Integer.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regId = createNumber("regId", Long.class);

    public final EnumPath<com.hooxi.admin.constants.code.TpcTpCd> tpcTpCd = createEnum("tpcTpCd", com.hooxi.admin.constants.code.TpcTpCd.class);

    public final StringPath useYn = createString("useYn");

    public QTbMssn(String variable) {
        super(TbMssn.class, forVariable(variable));
    }

    public QTbMssn(Path<? extends TbMssn> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTbMssn(PathMetadata metadata) {
        super(TbMssn.class, metadata);
    }

}

