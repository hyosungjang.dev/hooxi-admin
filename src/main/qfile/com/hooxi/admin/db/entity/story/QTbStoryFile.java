package com.hooxi.admin.db.entity.story;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;


/**
 * QTbStoryFile is a Querydsl query type for TbStoryFile
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QTbStoryFile extends EntityPathBase<TbStoryFile> {

    private static final long serialVersionUID = -1627999538L;

    public static final QTbStoryFile tbStoryFile = new QTbStoryFile("tbStoryFile");

    public final StringPath fileName = createString("fileName");

    public final StringPath filePath = createString("filePath");

    public final NumberPath<Long> fileSeq = createNumber("fileSeq", Long.class);

    public final NumberPath<Long> fileSize = createNumber("fileSize", Long.class);

    public final StringPath fileUrl = createString("fileUrl");

    public final DateTimePath<java.time.Instant> modDt = createDateTime("modDt", java.time.Instant.class);

    public final NumberPath<Long> modId = createNumber("modId", Long.class);

    public final DateTimePath<java.time.Instant> regDt = createDateTime("regDt", java.time.Instant.class);

    public final NumberPath<Long> regId = createNumber("regId", Long.class);

    public final NumberPath<Long> storyId = createNumber("storyId", Long.class);

    public QTbStoryFile(String variable) {
        super(TbStoryFile.class, forVariable(variable));
    }

    public QTbStoryFile(Path<? extends TbStoryFile> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTbStoryFile(PathMetadata metadata) {
        super(TbStoryFile.class, metadata);
    }

}

