package com.hooxi.admin.db.entity.mission;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;


/**
 * QTbActTimeline is a Querydsl query type for TbActTimeline
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QTbActTimeline extends EntityPathBase<TbActTimeline> {

    private static final long serialVersionUID = 1602146759L;

    public static final QTbActTimeline tbActTimeline = new QTbActTimeline("tbActTimeline");

    public final NumberPath<Long> actId = createNumber("actId", Long.class);

    public final EnumPath<com.hooxi.admin.constants.code.ActStatusCd> actStatusCd = createEnum("actStatusCd", com.hooxi.admin.constants.code.ActStatusCd.class);

    public final NumberPath<Long> actTlId = createNumber("actTlId", Long.class);

    public final DateTimePath<java.time.LocalDateTime> confirmDt = createDateTime("confirmDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> mmbrId = createNumber("mmbrId", Long.class);

    public final NumberPath<Long> mssnId = createNumber("mssnId", Long.class);

    public final StringPath reviewYn = createString("reviewYn");

    public final DateTimePath<java.time.LocalDateTime> startDt = createDateTime("startDt", java.time.LocalDateTime.class);

    public QTbActTimeline(String variable) {
        super(TbActTimeline.class, forVariable(variable));
    }

    public QTbActTimeline(Path<? extends TbActTimeline> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTbActTimeline(PathMetadata metadata) {
        super(TbActTimeline.class, metadata);
    }

}

