package com.hooxi.admin.db.entity.story;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;


/**
 * QTbReplyLike is a Querydsl query type for TbReplyLike
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QTbReplyLike extends EntityPathBase<TbReplyLike> {

    private static final long serialVersionUID = 523011166L;

    public static final QTbReplyLike tbReplyLike = new QTbReplyLike("tbReplyLike");

    public final NumberPath<Long> mmbrId = createNumber("mmbrId", Long.class);

    public final NumberPath<Long> replyId = createNumber("replyId", Long.class);

    public final StringPath useYn = createString("useYn");

    public QTbReplyLike(String variable) {
        super(TbReplyLike.class, forVariable(variable));
    }

    public QTbReplyLike(Path<? extends TbReplyLike> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTbReplyLike(PathMetadata metadata) {
        super(TbReplyLike.class, metadata);
    }

}

