package com.hooxi.admin.db.entity.member;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;


/**
 * QTbMmbrshpJob is a Querydsl query type for TbMmbrshpJob
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QTbMmbrshpJob extends EntityPathBase<TbMmbrshpJob> {

    private static final long serialVersionUID = -2017412328L;

    public static final QTbMmbrshpJob tbMmbrshpJob = new QTbMmbrshpJob("tbMmbrshpJob");

    public final StringPath jobKywdTpCd = createString("jobKywdTpCd");

    public final NumberPath<Long> mmbrshpId = createNumber("mmbrshpId", Long.class);

    public final NumberPath<Long> mmbrshpJobId = createNumber("mmbrshpJobId", Long.class);

    public final StringPath useTpCd = createString("useTpCd");

    public QTbMmbrshpJob(String variable) {
        super(TbMmbrshpJob.class, forVariable(variable));
    }

    public QTbMmbrshpJob(Path<? extends TbMmbrshpJob> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTbMmbrshpJob(PathMetadata metadata) {
        super(TbMmbrshpJob.class, metadata);
    }

}

