package com.hooxi.admin.db.entity.member;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;


/**
 * QTbMmbrshp is a Querydsl query type for TbMmbrshp
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QTbMmbrshp extends EntityPathBase<TbMmbrshp> {

    private static final long serialVersionUID = 1937576549L;

    public static final QTbMmbrshp tbMmbrshp = new QTbMmbrshp("tbMmbrshp");

    public final StringPath afltn = createString("afltn");

    public final StringPath bgImg = createString("bgImg");

    public final DateTimePath<java.time.LocalDateTime> confirmDt = createDateTime("confirmDt", java.time.LocalDateTime.class);

    public final DateTimePath<java.time.LocalDateTime> fnlModDttm = createDateTime("fnlModDttm", java.time.LocalDateTime.class);

    public final DateTimePath<java.time.LocalDateTime> frstRgstDttm = createDateTime("frstRgstDttm", java.time.LocalDateTime.class);

    public final EnumPath<com.hooxi.admin.constants.code.GradTpCd> gradCd = createEnum("gradCd", com.hooxi.admin.constants.code.GradTpCd.class);

    public final NumberPath<Long> mmbrId = createNumber("mmbrId", Long.class);

    public final NumberPath<Long> mmbrshpId = createNumber("mmbrshpId", Long.class);

    public final StringPath mmbrshpNum = createString("mmbrshpNum");

    public final EnumPath<com.hooxi.admin.constants.code.MmbrShpStatCd> mmbrshpStateCd = createEnum("mmbrshpStateCd", com.hooxi.admin.constants.code.MmbrShpStatCd.class);

    public final StringPath nickname = createString("nickname");

    public final StringPath profileImg = createString("profileImg");

    public final StringPath rjctRsn = createString("rjctRsn");

    public final StringPath stateMsg = createString("stateMsg");

    public final StringPath useYn = createString("useYn");

    public QTbMmbrshp(String variable) {
        super(TbMmbrshp.class, forVariable(variable));
    }

    public QTbMmbrshp(Path<? extends TbMmbrshp> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTbMmbrshp(PathMetadata metadata) {
        super(TbMmbrshp.class, metadata);
    }

}

