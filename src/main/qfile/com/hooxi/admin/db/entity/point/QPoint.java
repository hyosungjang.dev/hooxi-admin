package com.hooxi.admin.db.entity.point;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;


/**
 * QPoint is a Querydsl query type for Point
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QPoint extends EntityPathBase<Point> {

    private static final long serialVersionUID = -928138314L;

    public static final QPoint point1 = new QPoint("point1");

    public final NumberPath<Long> mmbrId = createNumber("mmbrId", Long.class);

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modId = createNumber("modId", Long.class);

    public final NumberPath<Long> orgPointId = createNumber("orgPointId", Long.class);

    public final NumberPath<Long> point = createNumber("point", Long.class);

    public final NumberPath<Long> pointId = createNumber("pointId", Long.class);

    public final EnumPath<com.hooxi.admin.db.entity.point.type.PointStatCd> pointStateCd = createEnum("pointStateCd", com.hooxi.admin.db.entity.point.type.PointStatCd.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regId = createNumber("regId", Long.class);

    public final DateTimePath<java.time.LocalDateTime> trstnDt = createDateTime("trstnDt", java.time.LocalDateTime.class);

    public final EnumPath<com.hooxi.admin.db.entity.point.type.TrstnDtlTpCd> trstnDtlTpCd = createEnum("trstnDtlTpCd", com.hooxi.admin.db.entity.point.type.TrstnDtlTpCd.class);

    public final NumberPath<Long> trstnId = createNumber("trstnId", Long.class);

    public final EnumPath<com.hooxi.admin.db.entity.point.type.TrstnTpCd> trstnTpCd = createEnum("trstnTpCd", com.hooxi.admin.db.entity.point.type.TrstnTpCd.class);

    public QPoint(String variable) {
        super(Point.class, forVariable(variable));
    }

    public QPoint(Path<? extends Point> path) {
        super(path.getType(), path.getMetadata());
    }

    public QPoint(PathMetadata metadata) {
        super(Point.class, metadata);
    }

}

