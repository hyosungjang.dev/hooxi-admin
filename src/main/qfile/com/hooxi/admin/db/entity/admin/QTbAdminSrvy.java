package com.hooxi.admin.db.entity.admin;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;


/**
 * QTbAdminSrvy is a Querydsl query type for TbAdminSrvy
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QTbAdminSrvy extends EntityPathBase<TbAdminSrvy> {

    private static final long serialVersionUID = -1859952792L;

    public static final QTbAdminSrvy tbAdminSrvy = new QTbAdminSrvy("tbAdminSrvy");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public QTbAdminSrvy(String variable) {
        super(TbAdminSrvy.class, forVariable(variable));
    }

    public QTbAdminSrvy(Path<? extends TbAdminSrvy> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTbAdminSrvy(PathMetadata metadata) {
        super(TbAdminSrvy.class, metadata);
    }

}

