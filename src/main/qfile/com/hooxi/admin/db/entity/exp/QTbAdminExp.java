package com.hooxi.admin.db.entity.exp;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;


/**
 * QTbAdminExp is a Querydsl query type for TbAdminExp
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QTbAdminExp extends EntityPathBase<TbAdminExp> {

    private static final long serialVersionUID = 460785353L;

    public static final QTbAdminExp tbAdminExp = new QTbAdminExp("tbAdminExp");

    public final NumberPath<Long> adminExpId = createNumber("adminExpId", Long.class);

    public final StringPath cntn = createString("cntn");

    public final StringPath commnt = createString("commnt");

    public final StringPath lvlCd = createString("lvlCd");

    public final NumberPath<Long> ncssryExp = createNumber("ncssryExp", Long.class);

    public final StringPath rwd = createString("rwd");

    public final StringPath title = createString("title");

    public QTbAdminExp(String variable) {
        super(TbAdminExp.class, forVariable(variable));
    }

    public QTbAdminExp(Path<? extends TbAdminExp> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTbAdminExp(PathMetadata metadata) {
        super(TbAdminExp.class, metadata);
    }

}

