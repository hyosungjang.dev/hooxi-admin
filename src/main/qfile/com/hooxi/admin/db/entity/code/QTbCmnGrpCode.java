package com.hooxi.admin.db.entity.code;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;


/**
 * QTbCmnGrpCode is a Querydsl query type for TbCmnGrpCode
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QTbCmnGrpCode extends EntityPathBase<TbCmnGrpCode> {

    private static final long serialVersionUID = 110773057L;

    public static final QTbCmnGrpCode tbCmnGrpCode = new QTbCmnGrpCode("tbCmnGrpCode");

    public final StringPath grpCdDesc = createString("grpCdDesc");

    public final StringPath grpCdNm = createString("grpCdNm");

    public final StringPath grpCdVal = createString("grpCdVal");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modId = createNumber("modId", Long.class);

    public final NumberPath<Integer> ordr = createNumber("ordr", Integer.class);

    public final StringPath parentGrpCd = createString("parentGrpCd");

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regId = createNumber("regId", Long.class);

    public final StringPath useYn = createString("useYn");

    public QTbCmnGrpCode(String variable) {
        super(TbCmnGrpCode.class, forVariable(variable));
    }

    public QTbCmnGrpCode(Path<? extends TbCmnGrpCode> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTbCmnGrpCode(PathMetadata metadata) {
        super(TbCmnGrpCode.class, metadata);
    }

}

