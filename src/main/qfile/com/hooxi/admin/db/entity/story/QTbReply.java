package com.hooxi.admin.db.entity.story;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;


/**
 * QTbReply is a Querydsl query type for TbReply
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QTbReply extends EntityPathBase<TbReply> {

    private static final long serialVersionUID = 908643367L;

    public static final QTbReply tbReply = new QTbReply("tbReply");

    public final StringPath dltTpCd = createString("dltTpCd");

    public final NumberPath<Long> mmbrId = createNumber("mmbrId", Long.class);

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modId = createNumber("modId", Long.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regId = createNumber("regId", Long.class);

    public final StringPath replyCtnt = createString("replyCtnt");

    public final NumberPath<Long> replyId = createNumber("replyId", Long.class);

    public final NumberPath<Long> replyPrtId = createNumber("replyPrtId", Long.class);

    public final StringPath rprtYn = createString("rprtYn");

    public final NumberPath<Long> storyId = createNumber("storyId", Long.class);

    public final StringPath useYn = createString("useYn");

    public QTbReply(String variable) {
        super(TbReply.class, forVariable(variable));
    }

    public QTbReply(Path<? extends TbReply> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTbReply(PathMetadata metadata) {
        super(TbReply.class, metadata);
    }

}

