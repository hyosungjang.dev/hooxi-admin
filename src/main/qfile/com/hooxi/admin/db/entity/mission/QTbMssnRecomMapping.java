package com.hooxi.admin.db.entity.mission;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;


/**
 * QTbMssnRecomMapping is a Querydsl query type for TbMssnRecomMapping
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QTbMssnRecomMapping extends EntityPathBase<TbMssnRecomMapping> {

    private static final long serialVersionUID = 1764724045L;

    public static final QTbMssnRecomMapping tbMssnRecomMapping = new QTbMssnRecomMapping("tbMssnRecomMapping");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modId = createNumber("modId", Long.class);

    public final NumberPath<Long> mssnId = createNumber("mssnId", Long.class);

    public final NumberPath<Long> recomId = createNumber("recomId", Long.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regId = createNumber("regId", Long.class);

    public final StringPath useYn = createString("useYn");

    public QTbMssnRecomMapping(String variable) {
        super(TbMssnRecomMapping.class, forVariable(variable));
    }

    public QTbMssnRecomMapping(Path<? extends TbMssnRecomMapping> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTbMssnRecomMapping(PathMetadata metadata) {
        super(TbMssnRecomMapping.class, metadata);
    }

}

