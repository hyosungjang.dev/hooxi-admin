package com.hooxi.admin.db.entity.user;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;


/**
 * QTbLoginHist is a Querydsl query type for TbLoginHist
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QTbLoginHist extends EntityPathBase<TbLoginHist> {

    private static final long serialVersionUID = -978207754L;

    public static final QTbLoginHist tbLoginHist = new QTbLoginHist("tbLoginHist");

    public final StringPath deviceName = createString("deviceName");

    public final StringPath deviceOs = createString("deviceOs");

    public final DateTimePath<java.time.LocalDateTime> loginDttm = createDateTime("loginDttm", java.time.LocalDateTime.class);

    public final StringPath loginRsltCd = createString("loginRsltCd");

    public final StringPath loginTp = createString("loginTp");

    public final NumberPath<Long> mmbrId = createNumber("mmbrId", Long.class);

    public QTbLoginHist(String variable) {
        super(TbLoginHist.class, forVariable(variable));
    }

    public QTbLoginHist(Path<? extends TbLoginHist> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTbLoginHist(PathMetadata metadata) {
        super(TbLoginHist.class, metadata);
    }

}

