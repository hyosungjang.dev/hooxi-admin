package com.hooxi.admin.db.entity.story;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;


/**
 * QTbStory is a Querydsl query type for TbStory
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QTbStory extends EntityPathBase<TbStory> {

    private static final long serialVersionUID = 910012978L;

    public static final QTbStory tbStory = new QTbStory("tbStory");

    public final NumberPath<Long> actTlId = createNumber("actTlId", Long.class);

    public final EnumPath<com.hooxi.admin.constants.code.FeedTpCd> feedTpCd = createEnum("feedTpCd", com.hooxi.admin.constants.code.FeedTpCd.class);

    public final StringPath likeYn = createString("likeYn");

    public final StringPath linkUrl = createString("linkUrl");

    public final StringPath locat = createString("locat");

    public final StringPath mainImgUrl = createString("mainImgUrl");

    public final NumberPath<Long> mmbrId = createNumber("mmbrId", Long.class);

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modId = createNumber("modId", Long.class);

    public final StringPath privateTpCd = createString("privateTpCd");

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regId = createNumber("regId", Long.class);

    public final StringPath replyYn = createString("replyYn");

    public final NumberPath<Long> shareCnt = createNumber("shareCnt", Long.class);

    public final StringPath storyCntn = createString("storyCntn");

    public final NumberPath<Long> storyId = createNumber("storyId", Long.class);

    public final EnumPath<com.hooxi.admin.constants.code.StoryStatCd> storyStatCd = createEnum("storyStatCd", com.hooxi.admin.constants.code.StoryStatCd.class);

    public final EnumPath<com.hooxi.admin.constants.code.StoryTpCd> storyTpCd = createEnum("storyTpCd", com.hooxi.admin.constants.code.StoryTpCd.class);

    public final NumberPath<Long> viewCnt = createNumber("viewCnt", Long.class);

    public final StringPath viewsYn = createString("viewsYn");

    public QTbStory(String variable) {
        super(TbStory.class, forVariable(variable));
    }

    public QTbStory(Path<? extends TbStory> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTbStory(PathMetadata metadata) {
        super(TbStory.class, metadata);
    }

}

