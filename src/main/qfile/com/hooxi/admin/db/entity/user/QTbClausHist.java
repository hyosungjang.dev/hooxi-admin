package com.hooxi.admin.db.entity.user;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QTbClausHist is a Querydsl query type for TbClausHist
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QTbClausHist extends EntityPathBase<TbClausHist> {

    private static final long serialVersionUID = 788629443L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QTbClausHist tbClausHist = new QTbClausHist("tbClausHist");

    public final StringPath clausAgrYn = createString("clausAgrYn");

    public final DateTimePath<java.time.Instant> fnlModDttm = createDateTime("fnlModDttm", java.time.Instant.class);

    public final DateTimePath<java.time.Instant> frstRgstDttm = createDateTime("frstRgstDttm", java.time.Instant.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final QTbClausAgr tbClausAgr;

    public QTbClausHist(String variable) {
        this(TbClausHist.class, forVariable(variable), INITS);
    }

    public QTbClausHist(Path<? extends TbClausHist> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QTbClausHist(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QTbClausHist(PathMetadata metadata, PathInits inits) {
        this(TbClausHist.class, metadata, inits);
    }

    public QTbClausHist(Class<? extends TbClausHist> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.tbClausAgr = inits.isInitialized("tbClausAgr") ? new QTbClausAgr(forProperty("tbClausAgr"), inits.get("tbClausAgr")) : null;
    }

}

