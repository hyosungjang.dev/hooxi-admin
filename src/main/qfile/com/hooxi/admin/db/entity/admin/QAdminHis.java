package com.hooxi.admin.db.entity.admin;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;


/**
 * QAdminHis is a Querydsl query type for AdminHis
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QAdminHis extends EntityPathBase<AdminHis> {

    private static final long serialVersionUID = -588080386L;

    public static final QAdminHis adminHis = new QAdminHis("adminHis");

    public final NumberPath<Long> adminId = createNumber("adminId", Long.class);

    public final DateTimePath<java.time.LocalDateTime> adminLoginDttm = createDateTime("adminLoginDttm", java.time.LocalDateTime.class);

    public final StringPath adminLoginRsltCd = createString("adminLoginRsltCd");

    public QAdminHis(String variable) {
        super(AdminHis.class, forVariable(variable));
    }

    public QAdminHis(Path<? extends AdminHis> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAdminHis(PathMetadata metadata) {
        super(AdminHis.class, metadata);
    }

}

